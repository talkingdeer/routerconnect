package ca

// PID is and ID for a process. Each PID has a dedicated virtual address space.
type PID uint32
