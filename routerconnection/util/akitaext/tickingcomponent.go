package akitaext

import (
	"sync"

	"gitlab.com/akita/akita"
)

// A Ticker is an object that updates states with ticks.
type Ticker interface {
	Tick(now akita.VTimeInSec) bool
}

type tickScheduler struct {
	lock    sync.Mutex
	handler akita.Handler
	Freq    akita.Freq
	Engine  akita.Engine

	nextTickTime akita.VTimeInSec
}

func newTicker(
	handler akita.Handler,
	engine akita.Engine,
	freq akita.Freq,
) *tickScheduler {
	ticker := new(tickScheduler)

	ticker.handler = handler
	ticker.Engine = engine
	ticker.Freq = freq

	ticker.nextTickTime = -1
	return ticker
}

func (t *tickScheduler) tickLater(now akita.VTimeInSec) {
	t.lock.Lock()
	time := t.Freq.NextTick(now)

	if t.nextTickTime >= time {
		t.lock.Unlock()
		return
	}

	t.nextTickTime = time
	tick := *akita.NewTickEvent(time, t.handler)
	t.Engine.Schedule(tick)
	t.lock.Unlock()
}

// TickingComponent is a type of component that update states from cycle to
// cycle. A programmer would only need to program a tick function for a ticking
// component.
type TickingComponent struct {
	*akita.ComponentBase
	*tickScheduler

	ticker Ticker
}

// NotifyPortFree triggers the TickingComponent to start ticking again.
func (c *TickingComponent) NotifyPortFree(
	now akita.VTimeInSec,
	port akita.Port,
) {
	c.tickScheduler.tickLater(now)
}

// NotifyRecv triggers the TickingComponent to start ticking again.
func (c *TickingComponent) NotifyRecv(
	now akita.VTimeInSec,
	port akita.Port,
) {
	c.tickScheduler.tickLater(now)
}

// Handle triggers the tick function of the TickingComponent
func (c *TickingComponent) Handle(e akita.Event) error {
	now := e.Time()
	madeProgress := c.ticker.Tick(now)
	if madeProgress {
		c.tickScheduler.tickLater(now)
	}
	return nil
}

// NewTickingComponent creates a new ticking component
func NewTickingComponent(
	name string,
	engine akita.Engine,
	freq akita.Freq,
	ticker Ticker,
) *TickingComponent {
	tc := new(TickingComponent)
	tc.tickScheduler = newTicker(tc, engine, freq)
	tc.ComponentBase = akita.NewComponentBase(name)
	tc.ticker = ticker
	return tc
}
