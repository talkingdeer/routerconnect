package akitaext_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/akita/util/akitaext"

	"github.com/golang/mock/gomock"
	"gitlab.com/akita/akita"
)

var _ = Describe("BufferedSender", func() {
	var (
		mockCtrl *gomock.Controller
		port     *MockPort
		buffer   *MockBuffer
		sender   BufferedSender
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		port = NewMockPort(mockCtrl)
		buffer = NewMockBuffer(mockCtrl)
		buffer.EXPECT().Capacity().Return(2).AnyTimes()
		sender = NewBufferedSender(port, buffer)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should panic if to send requests more than capacity", func() {
		Expect(func() { sender.CanSend(3) }).To(Panic())
	})

	It("should buffer", func() {
		buffer.EXPECT().Size().Return(0)
		Expect(sender.CanSend(1)).To(BeTrue())
		buffer.EXPECT().Size().Return(0)
		Expect(sender.CanSend(2)).To(BeTrue())

		buffer.EXPECT().Size().Return(1)
		Expect(sender.CanSend(1)).To(BeTrue())
		buffer.EXPECT().Size().Return(1)
		Expect(sender.CanSend(2)).To(BeFalse())

		buffer.EXPECT().Size().Return(2)
		Expect(sender.CanSend(1)).To(BeFalse())
		buffer.EXPECT().Size().Return(2)
		Expect(sender.CanSend(2)).To(BeFalse())
	})

	It("should send", func() {
		req1 := akita.NewReqBase()
		buffer.EXPECT().Push(req1)
		sender.Send(req1)

		req2 := akita.NewReqBase()
		buffer.EXPECT().Push(req2)
		sender.Send(req2)

		port.EXPECT().Send(req1)
		buffer.EXPECT().Peek().Return(req1)
		buffer.EXPECT().Pop()
		sent := sender.Tick(10)
		Expect(req1.SendTime()).To(Equal(akita.VTimeInSec(10)))
		Expect(sent).To(BeTrue())

		port.EXPECT().Send(req2)
		buffer.EXPECT().Peek().Return(req2)
		buffer.EXPECT().Pop()
		sent = sender.Tick(11)
		Expect(sent).To(BeTrue())
		Expect(req2.SendTime()).To(Equal(akita.VTimeInSec(11)))
	})

	It("should do nothing if buffer is empty", func() {
		buffer.EXPECT().Peek().Return(nil)
		sent := sender.Tick(10)
		Expect(sent).To(BeFalse())
	})

	It("should do nothing if send failed", func() {
		req1 := akita.NewReqBase()
		buffer.EXPECT().Peek().Return(req1)
		port.EXPECT().Send(req1).Return(akita.NewSendError())

		sent := sender.Tick(10)

		Expect(sent).To(BeFalse())
	})

})
