module gitlab.com/akita/util

require (
	github.com/golang/mock v1.2.0
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	github.com/rs/xid v1.2.1
	gitlab.com/akita/akita v1.3.2
)

go 1.12
