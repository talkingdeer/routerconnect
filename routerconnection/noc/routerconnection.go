package noc

import (
	"fmt"
	"log"
	"math/rand"
	"reflect"
	"strings"
	"sync"
	"time"

	"gitlab.com/akita/akita"
)

type RouterConnection struct {
	*akita.HookableBase
	ticker      *akita.Ticker
	Freq        akita.Freq
	NeedTicking bool
	sync.Mutex

	endPoints map[akita.Port]bool
	engine    akita.Engine

	//currently fixing..
	routers           []*SimpleRouter
	Argv1             int
	Argv2             int
	NumRouters        int
	NumGPUs           int
	RouterFreq        akita.Freq
	SrcBufferCapacity int

	PortMapping map[akita.Port]int

	Req2FlitMapping map[akita.Req]*Flit
	HopCount        int

	VCcounts         int
	FlitSize         int
	RouterBufferSize int
	IsWarmUpFinished bool

	WarmUpCycle akita.VTimeInSec
	DrainCycle  akita.VTimeInSec

	MAX_NUM_ROUTER int
	MAX_NUM_PORT   int
	MAX_NUM_VC     int

	// FlitStartBuffer [RouterIndex][PortIndex][VCIndex]
	// we can make a packet into flits but cannot inject all flits at the same Time
	// so.. RouterConnection should keep these flits and inject a flits per 1 clock cycle
	// similar to fixedbandwidthconnection
	FlitStartBuffer [][][][]Flit

	// FlitEndBuffer [DstPort][TailFlit..]
	// this is for returning tail flit's credit.
	FlitEndBuffer [][]akita.Req
	counter       int
	DirectCon     *akita.DirectConnection

	// for topology setup
	IsDGX1      bool
	IsDGX2      bool
	IsButterFly bool
	IsMesh      bool

	RDMAcounter int
	MMUcounter  int
	ETCcounter  int
	GPUcounter  []int

	Counter4Debug int
}

func (c *RouterConnection) ReqWrapping(req akita.Req) *Flit {
	//	reqBase := akita.NewReqBase()
	packet := new(Flit)
	packet.SetConnectionModule(c)
	// packet copy ..
	packet.Req = req
	packet.RoutingAddress = make([]int, 0)
	packet.LastPortCreditPointer = nil
	packet.LastRouterIndex = 0
	packet.CurrentStage = 0
	packet.CurrentVCIndex = 0
	rand.Seed(time.Now().UnixNano())
	packet.NextVCIndex = rand.Intn(c.VCcounts) // vc nums ..

	return packet
}

func (c *RouterConnection) ReqToFlits(req akita.Req) []Flit {
	numFlits := req.ByteSize() / c.FlitSize

	if numFlits*c.FlitSize < req.ByteSize() {
		numFlits++
	}

	if req.ByteSize() == 0 {
		numFlits = 1
	}

	Flits := make([]Flit, numFlits)

	rand.Seed(time.Now().UnixNano())

	for i := 0; i < numFlits; i++ {
		Flits[i].SetConnectionModule(c)
		Flits[i].Req = req
		Flits[i].RoutingAddress = make([]int, 0)
		Flits[i].LastPortCreditPointer = nil
		Flits[i].LastRouterIndex = 0
		Flits[i].CurrentStage = 0
		Flits[i].CurrentVCIndex = 0
		Flits[i].NextVCIndex = rand.Intn(c.VCcounts)
	}

	Flits[0].IsHead = true
	Flits[numFlits-1].IsTail = true

	return Flits
}

func (c *RouterConnection) PlugIn(port akita.Port) {
	//fmt.Printf("%s is connected.. ! \n", port.Component().Name())
	c.endPoints[port] = true

	// we can decide the mapping of source and destination
	port.SetConnection(c)
	c.PortMapping[port] = c.counter
	c.counter++
}

func (c *RouterConnection) Unplug(port akita.Port) {

	if _, ok := c.endPoints[port]; !ok {
		log.Panicf("connectable if not attached")
	}

	delete(c.endPoints, port)
	port.SetConnection(nil)
}

func (c *RouterConnection) SetDirectCon(conn *akita.DirectConnection) {
	c.DirectCon = conn
}

func (c *RouterConnection) CreditReturn(now akita.VTimeInSec, port akita.Port, req akita.Req) {

	IsEnd := false

	for p := range c.endPoints {
		if port == p {
			IsEnd = true
		}
	}

	if IsEnd == true {

		Flit := c.Req2FlitMapping[req]
		LastRouter := (*Flit).LastRouterPointer
		LastRouter.NewCreditCounts[Flit.LastPortIndex][Flit.LastVCIndex]++
		LastRouter.OutputVCStatus[Flit.LastPortIndex][Flit.LastVCIndex] = true
		LastRouter.ticker.TickLater(now)

	}

}

func (c *RouterConnection) NotifyAvailable(now akita.VTimeInSec, port akita.Port) {

	for p := range c.endPoints {
		p.NotifyAvailable(now)
	}

	for i := 0; i < c.NumRouters; i++ {
		for j := 0; j < c.routers[i].numInputUnit; j++ {
			c.routers[i].InputPorts[j].NotifyAvailable(now)
		}
	}

}

func (c *RouterConnection) AddressToRouterIndexDGX2(numGPUs int, req akita.Req) (int, int) {
	Src := req.Src().Component().Name()
	Dst := req.Dst().Component().Name()

	numSwitchs := c.Argv1

	srcindex := 0
	dstindex := 0

	// Find By Name
	for i := 1; i <= numGPUs; i++ {
		name := fmt.Sprintf("GPU_%d", i)

		if strings.Contains(Src, name) == true {
			srcindex = 3 * (i - 1)

			if strings.Contains(Src, "TLB") == true {

			} else if strings.Contains(Src, "RDMA") {
				srcindex = srcindex + 1
			} else {
				srcindex = srcindex + 2
			}
		}

		if strings.Contains(Dst, name) == true {
			dstindex = 3 * (i - 1)

			if strings.Contains(Dst, "TLB") == true {

			} else if strings.Contains(Dst, "RDMA") {
				dstindex = dstindex + 1
			} else {
				dstindex = dstindex + 2
			}
		}

		if Src == "MMU" {
			srcindex = numGPUs*3 + numGPUs*2 + 2*numSwitchs + 2 + 1 - 1
		}

		if Dst == "MMU" {
			dstindex = numGPUs*3 + numGPUs*2 + 2*numSwitchs + 2 + 1 - 1
		}

		if Src == "driver" {
			srcindex = numGPUs*3 + numGPUs*2 + 2*numSwitchs + 2 + 2 - 1
		}

		if Dst == "driver" {
			dstindex = numGPUs*3 + numGPUs*2 + 2*numSwitchs + 2 + 2 - 1
		}
	}

	return srcindex, dstindex
}

func (c *RouterConnection) AddressToRouterIndexDGX1(numGPUs int, req akita.Req) (int, int) {
	Src := req.Src().Component().Name()
	Dst := req.Dst().Component().Name()

	srcindex := 0
	dstindex := 0

	// Find By Name
	for i := 1; i <= numGPUs; i++ {
		name := fmt.Sprintf("GPU_%d", i)

		if strings.Contains(Src, name) == true {
			srcindex = 3 * (i - 1)

			if strings.Contains(Src, "TLB") == true {

			} else if strings.Contains(Src, "RDMA") {
				srcindex = srcindex + 1
			} else {
				srcindex = srcindex + 2
			}
		}

		if strings.Contains(Dst, name) == true {
			dstindex = 3 * (i - 1)

			if strings.Contains(Dst, "TLB") == true {

			} else if strings.Contains(Dst, "RDMA") {
				dstindex = dstindex + 1
			} else {
				dstindex = dstindex + 2
			}
		}

		if Src == "MMU" {
			srcindex = numGPUs*3 + numGPUs*2 + 2 + 1 - 1
		}

		if Dst == "MMU" {
			dstindex = numGPUs*3 + numGPUs*2 + 2 + 1 - 1
		}

		if Src == "driver" {
			srcindex = numGPUs*3 + numGPUs*2 + 2 + 2 - 1
		}

		if Dst == "driver" {
			dstindex = numGPUs*3 + numGPUs*2 + 2 + 2 - 1
		}
	}

	return srcindex, dstindex
}

/// For Traffic Analysis... (inter GPU)
func (c *RouterConnection) TrafficAnalysis(req akita.Req) {
	now := c.engine.CurrentTime()
	src := req.Src().Component().Name()
	dst := req.Dst().Component().Name()

	fmt.Printf("src:%s -> dst:%s \n", src, dst)
	if strings.Contains(src, "RDMA") || strings.Contains(dst, "RDMA") {
		c.RDMAcounter++
	} else if strings.Contains(src, "MMU") || strings.Contains(dst, "MMU") {
		c.MMUcounter++
	} else {
		FlitSize := req.ByteSize()
		fmt.Printf("Flit Size : %d \n", FlitSize)
		c.ETCcounter++
	}

	fmt.Printf("now: %0.12f   RDMA: %d      MMU: %d     ETC: %d   \n", now, c.RDMAcounter, c.MMUcounter, c.ETCcounter)

}

func (c *RouterConnection) StaticPathCalculate(req akita.Req) (int, int, []Flit) {

	// step 1 , make a req(packet) into flits
	QueuingDelay := req.RecvTime()
	req.SetRecvTime(req.SendTime())

	flits := c.ReqToFlits(req)

	// step 2 , Making a topology...
	iniRouterIndex := 0
	iniPortIndex := 0

	for i := 0; i < len(flits); i++ {
		flits[i].StartCycle = QueuingDelay

		if c.IsButterFly == true {
			iniRouterIndex, iniPortIndex = c.CalculatingButterfly(c.Argv1, c.Argv2, c.PortMapping[req.Src()], c.PortMapping[req.Dst()], &flits[i])

			flits[i].HopCount = c.HopCount
		}

		if c.IsMesh == true {
			iniRouterIndex, iniPortIndex = c.CalculatingMesh(c.Argv1, c.Argv2, c.PortMapping[req.Src()], c.PortMapping[req.Dst()], &flits[i])
			flits[i].HopCount = len(flits[i].RoutingAddress)
		}

		if c.IsDGX1 == true {
			SrcIndex, DstIndex := c.AddressToRouterIndexDGX1(c.NumGPUs, req)
			iniRouterIndex, iniPortIndex = c.CalculatingMinimalPathOnDGX1_Scalable(SrcIndex, DstIndex, &flits[i])
			flits[i].HopCount = len(flits[i].RoutingAddress)
		}

		if c.IsDGX2 == true {

			SrcIndex, DstIndex := c.AddressToRouterIndexDGX2(c.NumGPUs, req)
			iniRouterIndex, iniPortIndex = c.CalculatingMinimalPathOnDGX2_Scalable(SrcIndex, DstIndex, &flits[i])
			flits[i].HopCount = len(flits[i].RoutingAddress)
		}

		flits[i].CurrentPortIndex = iniPortIndex
		flits[i].NextVCIndex = flits[i].GetCurrentRoutingAddress() % c.VCcounts
	}

	return iniRouterIndex, iniPortIndex, flits
}

func (c *RouterConnection) Send(req akita.Req) *akita.SendError {
	now := c.engine.CurrentTime()
	//for debugging
	//fmt.Printf("%s   ->   %s \n", req.Src().Component().Name(), req.Dst().Component().Name())

	if req.Dst() == nil {
		log.Panic("destination is null")
	}

	req.Dst().Component().Name()
	if _, found := c.endPoints[req.Dst()]; !found {
		log.Printf("destination %s is not connected, "+
			"req ID %s, "+
			"request from %s",
			req.Dst().Component().Name(),
			req.GetID(),
			req.Src().Component().Name(),
		)
	}

	iniRouterIndex, iniPortIndex, flits := c.StaticPathCalculate(req)

	//whatIwant2See := len(c.FlitStartBuffer[iniRouterIndex][iniPortIndex][flits[0].NextVCIndex])
	//fmt.Printf("size~ : %d \n", whatIwant2See)

	//if c.routers[iniRouterIndex].InputVCStatus[iniPortIndex][flits[0].NextVCIndex] == true {

	if len(c.FlitStartBuffer[iniRouterIndex][iniPortIndex][flits[0].NextVCIndex]) < c.SrcBufferCapacity {
		// Inject flits into start temp buffer
		for i := 0; i < len(flits); i++ {
			c.FlitStartBuffer[iniRouterIndex][iniPortIndex][flits[i].NextVCIndex] = append(c.FlitStartBuffer[iniRouterIndex][iniPortIndex][flits[i].NextVCIndex], flits[i])
		}
		c.ticker.TickLater(now)
	} else {
		return akita.NewSendError()
	}

	return nil
}

func (c *RouterConnection) SendToDst(evt *SendToDstEvent) {

	req := evt.req
	now := c.engine.CurrentTime()

	//	fmt.Printf("%s   ->   %s \n", req.Src().Component().Name(), req.Dst().Component().Name())

	switch req := req.(type) {
	case *Flit:
		req.SetRecvTime(now)

		dstportindex := req.GetCurrentRoutingAddress()
		req.CurrentPortIndex = dstportindex
		req.RoutingAddress = req.RoutingAddress[1:]
		req.CurrentStage++

		NextStageIndex := req.CurrentStage

		DstPort := req.Dst()
		temprouter := req.LastRouterPointer

		if c.engine.CurrentTime()*1e9 > c.WarmUpCycle && c.engine.CurrentTime()*1e9 < c.DrainCycle {
			if c.IsWarmUpFinished == false {
				temp_ctx := akita.HookCtx{
					Domain: c,
					Now:    now,
					Pos:    HookWarmUpFinished,
				}
				c.InvokeHook(&temp_ctx)

				c.IsWarmUpFinished = true
			}

			Info := NewSendResult(temprouter.RouterIndex, dstportindex, c.FlitSize)
			ctx := akita.HookCtx{
				Domain: c,
				Now:    now,
				Pos:    HookRouterSendFlit,
				Item:   Info,
			}
			c.InvokeHook(&ctx)

			ctx = akita.HookCtx{
				Domain: c,
				Now:    now,
				Pos:    HookCheckLastTime,
			}
			c.InvokeHook(&ctx)
		}

		if NextStageIndex < req.HopCount {
			DstPort = *(temprouter.NextPorts[dstportindex])
			//creditcount := *req.LastPortCreditPointer

			if DstPort.Recv(*req) != nil {
				log.Panicf("Wrong Path Error")
			}

		} else {
			c.Counter4Debug++
			RealReq := req.Req
			c.Req2FlitMapping[RealReq] = req

			if req.IsTail != true {
				c.CreditReturn(now, DstPort, RealReq)
			} else {
				RealReq.SetRecvTime(now)
				if DstPort.Recv(RealReq) != nil {
					// need to re transfer
					DstPortIndex := c.PortMapping[DstPort]
					c.FlitEndBuffer[DstPortIndex] = append(c.FlitEndBuffer[DstPortIndex], RealReq)
					c.NeedTicking = true

				} else {
					// Success

					if c.engine.CurrentTime()*1e9 > c.WarmUpCycle && c.engine.CurrentTime()*1e9 < c.DrainCycle {
						ctx := akita.HookCtx{
							Domain: c,
							Now:    now,
							Pos:    HookFlitArrived,
							Item:   req,
						}
						c.InvokeHook(&ctx)

					}
					c.CreditReturn(now, DstPort, RealReq)
				}
			}
		}

	default:
		log.Panicf("cannot handle request of type %s ", reflect.TypeOf(req))
	}
}

func (c *RouterConnection) InjectFlit(now akita.VTimeInSec) {

	// for Flit to Flits
	for i := 0; i < c.MAX_NUM_ROUTER; i++ {
		for j := 0; j < c.MAX_NUM_PORT; j++ {
			for k := 0; k < c.MAX_NUM_VC; k++ {
				if len(c.FlitStartBuffer[i][j][k]) > 0 {

					flit := c.FlitStartBuffer[i][j][k][0]
					flit.SetRecvTime(now)
					inputport := j
					inputVC := flit.NextVCIndex
					Req := flit.Req
					if c.routers[i].InputVCStatus[inputport][inputVC] == true || c.routers[i].InputVCOwner[inputport][inputVC] == Req {
						err := c.routers[i].InputPorts[j].Recv(flit)

						if err != nil {
							//log.Panic("Wrong Flit Injection")
						} else {
							c.FlitStartBuffer[i][j][k] = c.FlitStartBuffer[i][j][k][1:]
						}
					}

					if len(c.FlitStartBuffer[i][j][k]) > 0 {
						c.NeedTicking = true
					}

				}
			}
		}
	}

	// for Flits to Flit
	for i := 0; i < 256; i++ {
		if len(c.FlitEndBuffer[i]) != 0 {
			Req := c.FlitEndBuffer[i][0]
			Req.SetRecvTime(now)

			var DstPort akita.Port

			//find dst port
			for key, val := range c.PortMapping {
				if val == i {
					DstPort = key
				}
			}

			if DstPort.Recv(Req) != nil {
				c.NeedTicking = true
			} else {

				if c.engine.CurrentTime()*1e9 > c.WarmUpCycle && c.engine.CurrentTime()*1e9 < c.DrainCycle {
					ctx := akita.HookCtx{
						Domain: c,
						Now:    now,
						Pos:    HookFlitArrived,
						Item:   Req,
					}
					c.InvokeHook(&ctx)
				}

				c.CreditReturn(now, DstPort, Req)
				c.FlitEndBuffer[i] = c.FlitEndBuffer[i][1:]

				if len(c.FlitEndBuffer[i]) > 0 {
					c.NeedTicking = true
				}
			}

		}
	}

}

func (c *RouterConnection) handleTickEvent(e akita.TickEvent) error {
	now := e.Time()

	c.NeedTicking = false

	c.InjectFlit(now)
	// inject packet

	if c.NeedTicking {
		c.ticker.TickLater(now)
	}

	return nil
}

func (c *RouterConnection) Handle(e akita.Event) error {
	c.Lock()
	defer c.Unlock()

	switch e := e.(type) {
	case *SendToDstEvent:
		c.SendToDst(e)
	case akita.TickEvent:
		c.handleTickEvent(e)
	default:
		log.Panicf("cannot handle event of %s", reflect.TypeOf(e))
	}
	return nil
}

// NewDirectConnection creates a new DirectConnection object
func NewRouterConnection(engine akita.Engine, numGPUs int, VCs int, RouterFreq akita.Freq) *RouterConnection {

	c := new(RouterConnection)
	//Router Connection's Freq
	c.ticker = akita.NewTicker(c, engine, 1*akita.GHz)

	c.HookableBase = akita.NewHookableBase()
	c.endPoints = make(map[akita.Port]bool)
	c.PortMapping = make(map[akita.Port]int)
	c.Req2FlitMapping = make(map[akita.Req]*Flit)

	c.MAX_NUM_PORT = 256
	c.MAX_NUM_ROUTER = 256
	c.MAX_NUM_VC = 256

	c.NumRouters = c.MAX_NUM_ROUTER

	c.FlitStartBuffer = make([][][][]Flit, c.MAX_NUM_ROUTER)
	c.FlitEndBuffer = make([][]akita.Req, c.MAX_NUM_ROUTER)
	c.VCcounts = VCs
	c.RouterFreq = RouterFreq
	c.NumGPUs = numGPUs
	c.IsWarmUpFinished = false

	c.IsDGX1 = false
	c.IsDGX2 = false
	c.IsButterFly = false
	c.IsMesh = false
	c.Argv1 = 0
	c.Argv2 = 0
	c.SrcBufferCapacity = 4

	c.WarmUpCycle = 3000
	c.DrainCycle = 4000

	for i := 0; i < c.MAX_NUM_ROUTER; i++ {
		c.FlitStartBuffer[i] = make([][][]Flit, c.MAX_NUM_PORT)
		for j := 0; j < c.MAX_NUM_PORT; j++ {
			c.FlitStartBuffer[i][j] = make([][]Flit, c.MAX_NUM_VC)
			for k := 0; k < c.MAX_NUM_VC; k++ {
				c.FlitStartBuffer[i][j][k] = make([]Flit, 0)
			}
		}
	}

	c.routers = make([]*SimpleRouter, c.MAX_NUM_ROUTER)
	c.engine = engine
	c.counter = 0
	c.HopCount = 0
	c.FlitSize = 32
	c.RouterBufferSize = 4

	c.ETCcounter = 0
	c.MMUcounter = 0
	c.RDMAcounter = 0
	c.GPUcounter = make([]int, c.MAX_NUM_ROUTER)

	c.Counter4Debug = 0
	return c
}
