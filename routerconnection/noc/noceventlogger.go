package noc

import (
	"fmt"
	"log"

	"gitlab.com/akita/akita"
	//"reflect"
)

const MAX_CHANNEL_NUM int = 1024
const MAX_ROUTER_NUM int = 1024

type SingleRouterRecorder struct {
	StartTime akita.VTimeInSec
	EndTime   akita.VTimeInSec

	// Record in channel units
	TransferredData []int
}

type NocStatLogger struct {
	StartTime akita.VTimeInSec
	EndTime   akita.VTimeInSec
	akita.SimulationEndHandler
	akita.LogHookBase
	Routers       []SingleRouterRecorder
	RouterConnect *RouterConnection
	// exactly, Cycles
	AccumulatedLatency akita.VTimeInSec
	FlitsCounter       int
}

func NewNocStatLogger(logger *log.Logger, c *RouterConnection) *NocStatLogger {
	h := new(NocStatLogger)
	h.Logger = logger
	h.RouterConnect = c
	h.Routers = make([]SingleRouterRecorder, MAX_ROUTER_NUM)
	for i := 0; i < MAX_ROUTER_NUM; i++ {
		h.Routers[i].TransferredData = make([]int, MAX_CHANNEL_NUM)
	}
	h.AccumulatedLatency = 0
	h.FlitsCounter = 0

	return h
}

func (h *NocStatLogger) Handle(now akita.VTimeInSec) {
	// Handle Method is only called engine.Finish()
	//ElapsedTime := h.EndTime - h.StartTime

	//fmt.Printf(" %0.12f %d \n", h.AccumulatedLatency, h.FlitsCounter)
	fmt.Printf("Average Flit Latency : %0.5f \n", float64(h.AccumulatedLatency*1e9)/float64(h.FlitsCounter))
	//log.Printf("Each Router's Channel BW Utilization \n")
	for i := 0; i < h.RouterConnect.NumRouters; i++ {
		//	log.Printf("Router %d 's Utilization", i)
		for j := 0; j < h.RouterConnect.routers[i].numInputUnit; j++ {
			//	TotalBytes := h.Routers[i].TransferredData[j]
			//	TotalGBytes := float64(TotalBytes) / float64(1024*1024*1024)

			//BW := float64(TotalGBytes) / float64(ElapsedTime)
			//	log.Printf("Channel %d : %0.4f GB/s \n", j, BW)
			//log.Printf("Channel %d : %d : %0.12f \n",j,h.Routers[i].TransferredData[j],ElapsedTime)
		}
		//	log.Printf("\n")
	}

}

var HookFlitStartTransfer = &akita.HookPos{Name: "Flit Start Transfer"}

var HookFlitArrived = &akita.HookPos{Name: "Flit Arrived"}

var HookFlitDelivered = &akita.HookPos{Name: "Flit Delivered"}

var HookRouterSendFlit = &akita.HookPos{Name: "Router Send Flit"}

var HookWarmUpFinished = &akita.HookPos{Name: "WarmUp Finished"}

var HookCheckLastTime = &akita.HookPos{Name: "CheckLast Time"}

func (h *NocStatLogger) Func(ctx *akita.HookCtx) {

	now := ctx.Now
	if ctx.Pos == HookFlitStartTransfer {

	} else if ctx.Pos == HookFlitArrived {
		flit, ok := ctx.Item.(*Flit)

		if !ok {
			return
		}
		h.RecordFlitArrived(*flit, now)

	} else if ctx.Pos == HookFlitDelivered {

	} else if ctx.Pos == HookRouterSendFlit {
		result, ok := ctx.Item.(*SendResult)

		if !ok {
			return
		}
		h.RecordRouterSendFlit(*result)

	} else if ctx.Pos == HookWarmUpFinished {
		h.StartTime = now
	} else if ctx.Pos == HookCheckLastTime {
		h.EndTime = now
	} else {
		return
	}

}

func (h *NocStatLogger) RecordFlitStart(p Flit) {

}

func (h *NocStatLogger) RecordFlitArrived(flit Flit, now akita.VTimeInSec) {
	ElapsedTime := flit.RecvTime() - flit.SendTime()
	h.AccumulatedLatency = h.AccumulatedLatency + ElapsedTime
	h.FlitsCounter++
	//fmt.Printf("Avg Latency:%0.12f \n", float64(h.AccumulatedLatency*1e9)/float64(h.FlitsCounter))
}

func (h *NocStatLogger) RecordFlitDelivered() {

}

func (h *NocStatLogger) RecordRouterSendFlit(result SendResult) {
	RouterIndex := result.RouterIndex
	ChannelIndex := result.ChannelIndex
	DataSize := result.FlitSize
	//log.Printf("%d %d %d \n", RouterIndex, ChannelIndex, DataSize)
	h.Routers[RouterIndex].TransferredData[ChannelIndex] = h.Routers[RouterIndex].TransferredData[ChannelIndex] + DataSize
}
