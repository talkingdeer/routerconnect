package noc

import (
	"gitlab.com/akita/akita"
)

type RoutingEvent struct {
	*akita.EventBase
}

func NewRoutingEvent(time akita.VTimeInSec, handler akita.Handler) *RoutingEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(RoutingEvent)
	evt.EventBase = evtBase

	return evt
}

type VCAllocateEvent struct {
	*akita.EventBase
}

func NewVCAllocEvent(time akita.VTimeInSec, handler akita.Handler) *VCAllocateEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(VCAllocateEvent)
	evt.EventBase = evtBase

	return evt
}

type SwitchAllocateEvent struct {
	*akita.EventBase
}


func NewSwitchAllocateEvent(time akita.VTimeInSec, handler akita.Handler) *SwitchAllocateEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(SwitchAllocateEvent)
	evt.EventBase = evtBase

	return evt
}

type SwitchTraversalEvent struct {
	*akita.EventBase
}

func NewSwitchTraversalEvent(time akita.VTimeInSec, handler akita.Handler) *SwitchTraversalEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(SwitchTraversalEvent)
	evt.EventBase = evtBase

	return evt
}

type SendToDstEvent struct {
	*akita.EventBase
	req akita.Req
}

func NewSendToDstEvent(time akita.VTimeInSec, handler akita.Handler, req akita.Req) *SendToDstEvent {
	evtBase := akita.NewEventBase(time, handler)
	evt := new(SendToDstEvent)
	evt.EventBase = evtBase
	evt.req = req

	return evt
}
