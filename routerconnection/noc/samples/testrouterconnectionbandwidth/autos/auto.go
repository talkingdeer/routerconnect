package main

import (
	"fmt"
	"math"

	"gitlab.com/akita/noc"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/noc/standalone"
)

func main() {
	for q := 0; q < 1; q++ {
		rengine := akita.NewSerialEngine()

		//rengine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

		trafficInjector := standalone.NewGreedyTrafficInjector(rengine)
		trafficInjector.PackageSize = 64

		//trafficCounter := new(noc.TrafficCounter)
		//conn := noc.NewFixedBandwidthConnection(16, engine, 1*akita.GHz)

		InputDegree := 4
		numVCs := 1
		router := noc.NewSimpleRouter("router", InputDegree, InputDegree, 1, rengine, 1*akita.GHz)
		conn := noc.NewRouterConnection(rengine, router, numVCs)
		conn.RouterInputDegree = InputDegree
		router.RoutingTable = make(map[akita.Port]int)

		// butterfly setting

		numStage := 1
		conn.BuildButterFlyNetwork(InputDegree, numStage)
		conn.ConnectingButterFlyNetwork(InputDegree, numStage)
		numAgents := int(math.Pow(float64(InputDegree), float64(numStage)))
		//mesh setting

		/*
			Size := 4
			Dim := 1
			conn.BuildMeshNetwork(Size, Dim)
			conn.ConnectingMeshNetwork(Size, Dim)
			numAgents := int(math.Pow(float64(Size), float64(Dim)))
		*/

		// build mesh, connect mesh

		//conn.TopologyCheck()
		//conn.BuildNetwork(numRouters, 2)

		//for butterfly

		//for mesh
		//numAgents := int(math.Pow(float64(Size), float64(Dim)))
		//numAgents := 4

		var agents []*standalone.Agent
		for i := 0; i < numAgents; i++ {
			name := fmt.Sprintf("agent_%d", i)
			agent := standalone.NewAgent(name, rengine)
			trafficInjector.RegisterAgent(agent)
			conn.PlugIn(agent.ToOut)

			agents = append(agents, agent)
		}
		trafficInjector.InjectRate = 40 + q*2
		trafficInjector.InjectTraffic()

		rengine.Run()

		totalData := conn.TotalDataReport()

		//temp := float64(totalData) / float64(1024*1024*1024) // Bytes to GBytes
		temp := float64(totalData) / float64(1e9)

		fmt.Printf("Bandwidth achieved %0.12f GB/s\n", float64(temp)/float64(rengine.CurrentTime()))
		fmt.Printf("Total data sent over interconnect %d Bytes\n", totalData)
		conn.AveragePacketLatencyReport()

		//fmt.Printf("Bandwidth achieved %f GB/s\n", float64(262144*4*64)/float64(rengine.CurrentTime())/1e9)
		//fmt.Printf("Bandwidth achieved %0.12f GB/s\n", float64(totalData)/float64(rengine.CurrentTime())/1e9)

		//fmt.Printf("Average Packet Latency is %0.12f \n", conn.AveragePacketLatencyReport())

	}
}
