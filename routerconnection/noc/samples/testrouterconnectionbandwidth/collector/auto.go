package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/noc"
	"gitlab.com/akita/noc/standalone"
)

func main() {
	iterNums := 20
	InitVale := 4

	Step := 4
	for q := 0; q < iterNums; q++ {
		rengine := akita.NewSerialEngine()
		VarInject := InitVale + q*Step

		trafficInjector := standalone.NewGreedyTrafficInjector(rengine)
		trafficInjector.PackageSize = 128

		//FlitSize := 32
		numGPUs := 8

		numRouterVC := 1 // Number of Virtual Channels
		RouterFreq := 1 * akita.GHz

		conn := noc.NewRouterConnection(rengine, numGPUs, numRouterVC, RouterFreq)
		NocLogger := noc.NewNocStatLogger(log.New(os.Stdout, "", 0), conn)
		conn.AcceptHook(NocLogger)
		conn.WarmUpCycle = 1
		conn.DrainCycle = 2000
		conn.NumGPUs = numGPUs
		trafficInjector.NumGPUs = numGPUs

		conn.FlitSize = 32
		conn.RouterBufferSize = 4

		rengine.RegisterSimulationEndHandler(NocLogger)

		// DGX-1
		/*
			PlaneDegree := 4
			Stack := 8
			conn.BuildScalable_DGX1_Network(NocLogger, numGPUs, PlaneDegree, Stack)
			conn.ConnectingScalable_DGX1_Network(numGPUs, PlaneDegree, Stack)
			conn.IsDGX1 = true
			conn.Argv1 = PlaneDegree
			conn.Argv2 = Stack
		*/
		// DGX-2

		numSwitchs := 6
		conn.Argv1 = numSwitchs
		conn.BuildScalable_DGX2_Network(NocLogger, numGPUs, numSwitchs)
		conn.ConnectingScalable_DGX2_Network(numGPUs, numSwitchs)
		conn.IsDGX2 = true

		// 4 DGX
		var agents []*standalone.Agent

		for i := 1; i <= numGPUs; i++ {
			name := fmt.Sprintf("GPU_%d.L2TLB", i)
			agent := standalone.NewAgent(name, rengine)
			trafficInjector.RegisterAgent(agent)
			conn.PlugIn(agent.ToOut)

			name2 := fmt.Sprintf("GPU_%d", i)
			agent2 := standalone.NewAgent(name2, rengine)
			trafficInjector.RegisterAgent(agent2)
			conn.PlugIn(agent2.ToOut)

			name3 := fmt.Sprintf("GPU_%d.RDMA", i)
			agent3 := standalone.NewAgent(name3, rengine)
			trafficInjector.RegisterAgent(agent3)
			conn.PlugIn(agent3.ToOut)

			agents = append(agents, agent)
			agents = append(agents, agent2)
			agents = append(agents, agent3)
		}

		driver := fmt.Sprintf("driver")
		a_dri := standalone.NewAgent(driver, rengine)
		trafficInjector.RegisterAgent(a_dri)
		conn.PlugIn(a_dri.ToOut)

		mmu := fmt.Sprintf("MMU")
		a_mmu := standalone.NewAgent(mmu, rengine)
		trafficInjector.RegisterAgent(a_mmu)
		conn.PlugIn(a_mmu.ToOut)

		//trafficInjector.InjectRate = VarInject
		trafficInjector.InjectRate = VarInject
		trafficInjector.InjectTraffic()

		start := rengine.CurrentTime()
		rengine.Run()
		rengine.Finished()
		end := rengine.CurrentTime()

		fmt.Printf("Inject Rate: %d \n", VarInject)

		fmt.Printf("CheolGyu TIME: %0.12f \n", end-start)

		fmt.Printf("Total Flits : %d (expected : %d ) \n", conn.Counter4Debug, (numGPUs*3+2)*4*trafficInjector.NumPackages)
	}
}
