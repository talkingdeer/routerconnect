package main

import (
	"fmt"

	"gitlab.com/akita/noc"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/noc/standalone"
)

func main() {
	engine := akita.NewSerialEngine()
	//engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

	trafficInjector := standalone.NewGreedyTrafficInjector(engine)
	trafficInjector.PackageSize = 64

	trafficCounter := new(noc.TrafficCounter)
	conn := noc.NewFixedBandwidthConnection(16, engine, 1*akita.GHz)
	conn.AcceptHook(trafficCounter)

	var agents []*standalone.Agent
	for i := 0; i < 4; i++ {
		name := fmt.Sprintf("agent_%d", i)
		agent := standalone.NewAgent(name, engine)
		trafficInjector.RegisterAgent(agent)
		conn.PlugIn(agent.ToOut)

		agents = append(agents, agent)
	}

	trafficInjector.InjectTraffic()

	engine.Run()

	fmt.Printf("total size : %d \n", trafficCounter.TotalData)
	fmt.Printf("Total data sent over interconnect %d\n",
		trafficCounter.TotalData)
	fmt.Printf("Bandwidth achieved %0.12f GB/s\n",
		float64(trafficCounter.TotalData)/float64(engine.CurrentTime())/1e9)

	fmt.Printf("Traffic: %0.12f   Sconds:%f \n",
		float64(trafficCounter.TotalData), float64(engine.CurrentTime()))

}
