package noc

import (
	"log"
	"reflect"
	"sync"

	"gitlab.com/akita/akita"
)

// FixedBandwidthConnection models a connection that transfers requests at a
// certain byte per cycle speed.
type FixedBandwidthConnection struct {
	sync.Mutex
	akita.HookableBase

	engine akita.Engine
	ticker *akita.Ticker

	BytesPerCycle int
	NumLanes      int
	busy          int

	Freq              akita.Freq
	SrcBufferCapacity int
	DstBufferCapacity int

	srcBuffers         []*ReqBuffer
	srcPortToBufferMap map[akita.Port]*ReqBuffer
	srcBufferBusy      map[*ReqBuffer]bool
	lastSelectedSrcBuf int

	dstBuffers         []*ReqBuffer
	dstBufferToPortMap map[*ReqBuffer]akita.Port
	dstPortToBufferMap map[akita.Port]*ReqBuffer
	dstBusy            map[akita.Port]bool
	dstNumArrivingReqs map[akita.Port]int

	needTick bool
}

// Handle handles events scheduled on the FixedBandwidthConnection
func (c *FixedBandwidthConnection) Handle(e akita.Event) error {
	now := e.Time()
	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Pos:    akita.HookPosBeforeEvent,
		Item:   e,
	}
	c.InvokeHook(&ctx)

	c.Lock()

	switch evt := e.(type) {
	case akita.TickEvent:
		c.tick(now)
	case *TransferEvent:
		c.doTransfer(evt)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}

	c.Unlock()

	ctx.Pos = akita.HookPosAfterEvent
	c.InvokeHook(&ctx)

	return nil
}

func (c *FixedBandwidthConnection) tick(now akita.VTimeInSec) {
	c.needTick = false

	c.doDeliver(now)
	c.scheduleTransfer(now)

	if c.needTick {
		c.ticker.TickLater(now)
	}
}

func (c *FixedBandwidthConnection) doDeliver(now akita.VTimeInSec) {
	for _, buf := range c.dstBuffers {
		dst := c.dstBufferToPortMap[buf]

		if c.dstBusy[dst] {
			continue
		}

		if len(buf.Buf) == 0 {
			continue
		}

		req := buf.Buf[0]
		req.SetRecvTime(now)
		err := dst.Recv(req)
		if err == nil {
			buf.Buf = buf.Buf[1:]
			c.needTick = true

			ctx := akita.HookCtx{
				Domain: c,
				Now:    now,
				Pos:    akita.HookPosConnDeliver,
				Item:   req,
			}
			c.InvokeHook(&ctx)

			// log.Printf("%.15f: Req %s delivered", now, req.GetID())
		} else {
			c.dstBusy[dst] = true
		}
	}
}

func (c *FixedBandwidthConnection) scheduleTransfer(now akita.VTimeInSec) {
	if c.busy >= c.NumLanes {
		return
	}

	for i := 0; i < len(c.srcBuffers); i++ {

		bufIndex := (i + c.lastSelectedSrcBuf + 1) % len(c.srcBuffers)

		buf := c.srcBuffers[bufIndex]
		if c.srcBufferBusy[buf] {
			continue
		}
		if len(buf.Buf) == 0 {
			continue
		}

		req := buf.Buf[0]
		dst := req.Dst()
		dstBuf := c.dstPortToBufferMap[dst]
		if len(dstBuf.Buf)+c.dstNumArrivingReqs[dst] >= c.DstBufferCapacity {
			continue
		}

		c.needTick = true
		cycles := ((req.ByteSize() - 1) / c.BytesPerCycle) + 1
		transferTime := c.Freq.NCyclesLater(cycles, now)
		transferEvent := NewTransferEvent(transferTime, c, req, 0)
		c.dstNumArrivingReqs[dst]++
		c.engine.Schedule(transferEvent)

		ctx := akita.HookCtx{
			Domain: c,
			Now:    now,
			Pos:    akita.HookPosConnStartTrans,
			Item:   req,
		}
		c.InvokeHook(&ctx)

		c.busy++

		c.srcBufferBusy[buf] = true
		c.lastSelectedSrcBuf = bufIndex
	}
}

func (c *FixedBandwidthConnection) doTransfer(evt *TransferEvent) {
	now := evt.Time()
	req := evt.req
	src := req.Src()
	dst := req.Dst()

	srcBuf := c.srcPortToBufferMap[src]
	dstBuf := c.dstPortToBufferMap[dst]

	srcBuf.Buf = srcBuf.Buf[1:]
	dstBuf.Buf = append(dstBuf.Buf, req)

	c.busy--
	c.srcBufferBusy[srcBuf] = false
	c.dstNumArrivingReqs[dst]--
	c.ticker.TickLater(now)

	// log.Printf("%.15f: Req %s transferred", now, req.GetID())

	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Pos:    akita.HookPosConnDoneTrans,
		Item:   req,
	}
	c.InvokeHook(&ctx)

	src.NotifyAvailable(evt.Time())
}

// Send start the transfer of the request. It guarantees that the request get
// delievered sometime later. It may also return an error indicating that this
// request cannot be sent at the moment.
func (c *FixedBandwidthConnection) Send(req akita.Req) *akita.SendError {
	c.Lock()
	defer c.Unlock()

	buf := c.srcPortToBufferMap[req.Src()]

	if buf == nil {
		log.Panic("not connected")
	}

	if len(buf.Buf) >= c.SrcBufferCapacity {
		return akita.NewSendError()
	}

	// log.Printf("%.15f: Req %s start sending", req.SendTime(), req.GetID())

	buf.Buf = append(buf.Buf, req)
	c.ticker.TickLater(req.SendTime())

	ctx := akita.HookCtx{
		Domain: c,
		Now:    req.SendTime(),
		Pos:    akita.HookPosConnStartSend,
		Item:   req,
	}
	c.InvokeHook(&ctx)

	return nil
}

// PlugIn connects a port to the FixedBandwidthConnection
func (c *FixedBandwidthConnection) PlugIn(port akita.Port) {
	_, connected := c.srcPortToBufferMap[port]
	if connected {
		log.Panic("port already connected")
	}

	srcBuf := &ReqBuffer{
		Capacity: c.SrcBufferCapacity,
	}
	c.srcBuffers = append(c.srcBuffers, srcBuf)
	c.srcPortToBufferMap[port] = srcBuf
	c.srcBufferBusy[srcBuf] = false

	dstBuf := &ReqBuffer{
		Capacity: c.DstBufferCapacity,
	}
	c.dstBuffers = append(c.dstBuffers, dstBuf)
	c.dstBufferToPortMap[dstBuf] = port
	c.dstPortToBufferMap[port] = dstBuf
	c.dstNumArrivingReqs[port] = 0
	c.dstBusy[port] = false

	port.SetConnection(c)
}

// Unplug disconnect the connection with a port
func (FixedBandwidthConnection) Unplug(port akita.Port) {
	panic("implement me")
}

// NotifyAvailable enables the FixedBandwidthConnection to continue try to
// deliver to a desination
func (c *FixedBandwidthConnection) NotifyAvailable(
	now akita.VTimeInSec,
	port akita.Port,
) {
	c.Lock()
	c.dstBusy[port] = false
	c.ticker.TickLater(now)
	c.Unlock()
}

// NewFixedBandwidthConnection creates a new FixedBandwidthConnection
func NewFixedBandwidthConnection(
	bytesPerCycle int,
	engine akita.Engine,
	freq akita.Freq,
) *FixedBandwidthConnection {
	conn := new(FixedBandwidthConnection)

	conn.BytesPerCycle = bytesPerCycle
	conn.SrcBufferCapacity = 1
	conn.DstBufferCapacity = 1
	conn.srcPortToBufferMap = make(map[akita.Port]*ReqBuffer)
	conn.srcBufferBusy = make(map[*ReqBuffer]bool)
	conn.dstBufferToPortMap = make(map[*ReqBuffer]akita.Port)
	conn.dstPortToBufferMap = make(map[akita.Port]*ReqBuffer)
	conn.dstNumArrivingReqs = make(map[akita.Port]int)
	conn.dstBusy = make(map[akita.Port]bool)

	conn.engine = engine
	conn.Freq = freq
	conn.ticker = akita.NewTicker(conn, engine, conn.Freq)

	conn.NumLanes = 1

	return conn
}
