package noc

import (
	"gitlab.com/akita/akita"
)

// DirectConnection connects two components without latency

type Flit struct {
	akita.Req

	RoutingAddress []int
	// new type ! for routing information
	RouterIndexAddress []int
	PortIndexAddress   []int

	ConnectionModule      *RouterConnection
	LastPortCreditPointer *int
	LastRouterIndex       int
	LastRouterPointer     *SimpleRouter
	LastPortIndex         int
	LastVCIndex           int

	LastRouterStageIndex int
	CurrentStage         int
	HopCount             int
	OutputPortIndex      int

	NextVCIndex    int
	CurrentVCIndex int

	NextPortIndex    int
	CurrentPortIndex int

	NextRouterPort akita.Port

	// for packet latnecy measurement
	StartCycle akita.VTimeInSec
	EndCycle   akita.VTimeInSec

	IsHead bool
	IsTail bool

}

func (p *Flit) SetRoutingAddress(Sequence []int) {

	p.RoutingAddress = make([]int, len(Sequence))
	for i := 0; i < len(Sequence); i++ {
		p.RoutingAddress[i] = Sequence[i]
	}

}

func (p *Flit) SetConnectionModule(c *RouterConnection) {
	p.ConnectionModule = c
}

func (p *Flit) GetCurrentRoutingAddress() int {
	return p.RoutingAddress[0]
}
