package standalone

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
)

// TrafficReq is a type of requests that only used in standalone network test.
// It has a byte size, but we do not care about the information it carries.
type TrafficReq struct {
	*akita.ReqBase
}

// NewTrafficReq creates a new traffic request
func NewTrafficReq(src, dst akita.Port, byteSize int) *TrafficReq {
	req := new(TrafficReq)
	req.ReqBase = akita.NewReqBase()
	req.SetSrc(src)
	req.SetDst(dst)
	req.SetByteSize(byteSize)
	return req
}

type StartSendEvent struct {
	*akita.EventBase
	Req *TrafficReq
}

func NewStartSendEvent(
	time akita.VTimeInSec,
	src, dst *Agent,
	byteSize int,
) *StartSendEvent {
	e := new(StartSendEvent)
	e.EventBase = akita.NewEventBase(time, src)
	e.Req = NewTrafficReq(src.ToOut, dst.ToOut, byteSize)
	return e
}

// Agent is a component that connects the network. It can send and receive
// requests to/ from the network.
type Agent struct {
	*akita.TickingComponent

	ToOut akita.Port

	Buffer []*TrafficReq
}

func (a *Agent) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	a.ToOut.Retrieve(now)
	a.TickLater(now)
}

func (a *Agent) Handle(e akita.Event) error {
	switch e := e.(type) {
	case *StartSendEvent:
		a.handleStartSendEvent(e)
	case akita.TickEvent:
		a.tick(e)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(e))
	}
	return nil
}

func (a *Agent) handleStartSendEvent(e *StartSendEvent) {
	now := a.Engine.CurrentTime()
	e.Req.SetRecvTime(now)
	a.Buffer = append(a.Buffer, e.Req)
	a.TickLater(e.Time())
}

func (a *Agent) tick(e akita.TickEvent) {
	now := e.Time()
	a.NeedTick = false

	a.sendDataOut(now)

	if a.NeedTick {
		a.TickLater(now)
	}
}

func (a *Agent) sendDataOut(now akita.VTimeInSec) {
	if len(a.Buffer) == 0 {
		return
	}

	req := a.Buffer[0]
	req.SetSendTime(now)
	err := a.ToOut.Send(req)
	if err == nil {
		a.Buffer = a.Buffer[1:]
		a.NeedTick = true
	} else {
		a.NeedTick = true
	}
}

func NewAgent(name string, engine akita.Engine) *Agent {
	a := new(Agent)
	a.TickingComponent = akita.NewTickingComponent(name, engine, 1*akita.GHz, a)
	a.ToOut = akita.NewLimitNumReqPort(a, 4)

	return a
}
