package standalone

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/akita/akita"
)

type TrafficInjector interface {
	RegisterAgent(a *Agent)
	InjectTraffic()
}

// GreedyTraffic generate a large number of traffic at the beginning of the
// simulation.
type GreedyTrafficInjector struct {
	agents []*Agent

	engine akita.Engine

	PackageSize int
	NumPackages int

	InjectRate int
	NumGPUs    int
}

func NewGreedyTrafficInjector(engine akita.Engine) *GreedyTrafficInjector {
	ti := new(GreedyTrafficInjector)
	ti.PackageSize = 1024

	ti.NumPackages = 2048
	ti.engine = engine
	return ti
}

func (ti *GreedyTrafficInjector) RegisterAgent(a *Agent) {
	ti.agents = append(ti.agents, a)
}

func (ti *GreedyTrafficInjector) InjectTraffic() {
	rand.Seed(time.Now().UnixNano())
	for i, a := range ti.agents {
		for j := 0; j < ti.NumPackages; j++ {
			dstID := rand.Int() % (len(ti.agents) - 1)

			if dstID >= i {
				dstID += 1
			}

			/*
				for {
					dstID = rand.Int() % (len(ti.agents))

					if dstID != i {
						break
					}
				}
				dst := ti.agents[dstID]
			*/

			// Synthetic traffic for dgx system
			numGPUs := ti.NumGPUs
			for {
				dstID = rand.Int() % (numGPUs + 1)
				if dstID != i/3 {
					break
				}
			}

			Offset := 0
			if dstID == numGPUs {
				Offset = rand.Int() % 2
			} else {
				Offset = rand.Int() % 3
			}
			DestinationIndex := dstID*3 + Offset
			dst := ti.agents[DestinationIndex]

			transTime := float64(j) / 1e9
			time := akita.VTimeInSec(transTime)
			//time := akita.VTimeInSec(0)
			pkg := NewStartSendEvent(time, a, dst, ti.PackageSize)

			// for injection rate ..
			prob := rand.Int() % (1000)
			//InjectRate := ti.InjectRate
			//fmt.Printf("%d %d ? \n",i, dstID)

			if ti.InjectRate > prob {
				ti.engine.Schedule(pkg)
			}
		}
	}
}

func (ti *GreedyTrafficInjector) InjectDGX_1_Traffic() {
	rand.Seed(time.Now().UnixNano())
	fmt.Printf(" length of agents : %d \n", len(ti.agents))

	dedicated := [24]int{9, 10, 11, 21, 22, 23, 18, 19, 20, 15, 16, 17, 6, 7, 8, 3, 4, 5, 0, 1, 2, 12, 13, 14}
	for i, a := range ti.agents {

		if i > 23 {
			break
		}
		fmt.Printf("%d to %d \n", i, dedicated[i])
		for j := 0; j < ti.NumPackages; j++ {

			/// Uniform random
			/*
				dstID := rand.Int() % (len(ti.agents) - 1)

				if dstID >= i {
					dstID += 1
				}

				for {
					dstID = rand.Intn(100) % (len(ti.agents) - 2)

					temp_1 := i / 3
					temp_2 := dstID / 3
					if temp_1 != temp_2 {
						//	fmt.Printf("%d %d~  %d %d \n",temp_1,temp_2,i,dstID)
						break
					}
				}
			*/

			// this is for tornado ..
			//fmt.Printf("%d to %d \n", i, dedicated[i])
			dstID := dedicated[i]
			//dstID := 0
			dst := ti.agents[dstID]

			transTime := float64(j) / 1e9
			time := akita.VTimeInSec(transTime)
			//time := akita.VTimeInSec(0)
			pkg := NewStartSendEvent(time, a, dst, ti.PackageSize)

			// for injection rate ..
			prob := rand.Int() % (100)
			InjectRate := ti.InjectRate
			//InjectRate := 5
			//fmt.Printf("%d %d ? \n",i, dstID)

			temp_1 := i / 3
			temp_2 := dstID / 3

			if InjectRate > prob && dstID != i && temp_1 != temp_2 {
				ti.engine.Schedule(pkg)
			}

		}

	}

}

func (ti *GreedyTrafficInjector) InjectDGX_2_Traffic() {
	//this is for DGX2
	rand.Seed(time.Now().UnixNano())
	fmt.Printf(" length of agents : %d \n", len(ti.agents))

	dedicated := [48]int{9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 45, 46, 47, 42, 43, 44, 39, 40, 41, 6, 7, 8, 3, 4, 5, 0, 1, 2, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38}
	for i, a := range ti.agents {

		if i > 47 {
			break
		}

		for j := 0; j < ti.NumPackages; j++ {

			/// Uniform random
			/*
				dstID := rand.Int() % (len(ti.agents) - 1)

				if dstID >= i {
					dstID += 1
				}

				for {
					dstID = rand.Intn(100) % (len(ti.agents) - 2)

					temp_1 := i / 3
					temp_2 := dstID / 3
					if temp_1 != temp_2 {
						//	fmt.Printf("%d %d~  %d %d \n",temp_1,temp_2,i,dstID)
						break
					}
				}
			*/
			//dstID := dedicated[i]
			dstID := dedicated[1]
			dst := ti.agents[dstID]

			transTime := float64(j) / 1e9
			time := akita.VTimeInSec(transTime)
			//time := akita.VTimeInSec(0)
			pkg := NewStartSendEvent(time, a, dst, ti.PackageSize)

			// for injection rate ..
			prob := rand.Int() % (100)
			InjectRate := ti.InjectRate

			//fmt.Printf("%d %d ? \n",i, dstID)

			temp_1 := i / 3
			temp_2 := dstID / 3

			if InjectRate > prob && dstID != i && temp_1 != temp_2 {
				ti.engine.Schedule(pkg)
			}

		}

	}

}
