package noc

import (
	"fmt"
	"log"
	"math"

	"gitlab.com/akita/akita"
)

func (c *RouterConnection) DGX2_TopologyCheck(numGPUs int, numSwitchs int) {

	numRouters := 2*numGPUs + 2*numSwitchs
	for i := 0; i < numRouters; i++ {
		degree := c.routers[i].numInputUnit
		for j := 0; j < degree; j++ {
			t_port := c.routers[i].NextPorts[j]
			if t_port != nil {
				r_port := *(t_port)
				fmt.Printf("%s 's %d port -----> %s \n", c.routers[i].Name(), j, r_port.Component().Name())
			} else {
			}
		}

		fmt.Printf("\n")
	}

}

func (c *RouterConnection) DGX1_TopologyCheck(numGPUs int) {

	for i := 0; i < 2*numGPUs; i++ {
		degree := c.routers[i].numInputUnit
		for j := 0; j < degree; j++ {
			t_port := c.routers[i].NextPorts[j]
			if t_port != nil {
				r_port := *(t_port)
				fmt.Printf("%s 's %d port -----> %s \n", c.routers[i].Name(), j, r_port.Component().Name())
			} else {
			}
		}

		fmt.Printf("\n")
	}

}

// Routing Calculation for Butterfly network
func (c *RouterConnection) CalculatingButterfly(k int, n int, SrcIndex int, DstIndex int, packet *Flit) (int, int) {
	//return first router index and port index
	initRouterIndex := SrcIndex / k
	initRouterPortIndex := SrcIndex % k

	if n >= 1 {
		ReversedRoutingInfo := make([]int, n)

		temp_dst := DstIndex
		quotient := 0
		remainder := 0
		for i := 0; i < n; i++ {
			quotient = temp_dst / k
			remainder = temp_dst % k
			ReversedRoutingInfo[i] = remainder
			temp_dst = quotient
		}

		for i := 0; i < n; i++ {
			temp_val := ReversedRoutingInfo[n-1-i]
			packet.RoutingAddress = append(packet.RoutingAddress, temp_val)
			//packet.RoutingAddress[i] = temp_val
		}
	}

	return initRouterIndex, initRouterPortIndex
}

func (c *RouterConnection) ConnectingButterFlyNetwork(k int, n int) {
	degree := k
	//stage := n
	RoutersPerStage := int(math.Pow(float64(k), float64(n-1)))

	if n >= 2 {
		// [stage][index]
		Routers := make([][]*SimpleRouter, n)

		for i := 0; i < n; i++ {
			Routers[i] = make([]*SimpleRouter, RoutersPerStage)
			for j := 0; j < RoutersPerStage; j++ {
				//	fmt.Printf("Index: %d \n", RoutersPerStage*i+j)
				Routers[i][j] = c.routers[RoutersPerStage*i+j]
			}
		}

		for i := 0; i < n-1; i++ {
			itercounts := int(math.Pow(float64(k), float64(i)))

			// Routers[i], Routers[i+1] itercounts 조각 쳐야함
			UnitSize := RoutersPerStage / itercounts
			for j := 0; j < itercounts; j++ {
				temp1 := make([]*SimpleRouter, UnitSize)
				temp2 := make([]*SimpleRouter, UnitSize)

				for m := 0; m < UnitSize; m++ {
					temp1[m] = Routers[i][m+UnitSize*j]
					temp2[m] = Routers[i+1][m+UnitSize*j]
				}
				ConnectingButterfly(UnitSize, degree, temp1, temp2)
			}
		}

		//copy into ...
		for i := 0; i < n; i++ {
			for j := 0; j < RoutersPerStage; j++ {
				c.routers[RoutersPerStage*i+j] = Routers[i][j]
			}
		}
	}

}

func ConnectingButterfly(RouterPerStage int, degree int, input []*SimpleRouter, output []*SimpleRouter) {
	factor := RouterPerStage / degree
	for i := 0; i < RouterPerStage; i++ {
		GroupIndex := i / factor
		Scale := RouterPerStage / degree

		for j := 0; j < degree; j++ {
			index := i % factor
			input[i].NextPorts[j] = &(output[index+Scale*j].InputPorts[GroupIndex])
			//fmt.Printf("%d th router -> %d router's %d port , index : %d \n", i, index+Scale*j, GroupIndex, index)
		}
	}
}

func (c *RouterConnection) BuildButterFlyNetwork(k int, n int, Logger *NocStatLogger) {
	c.HopCount = n
	RouterDegree := k

	// we need k^(n-1) * n routers
	c.NumRouters = int(math.Pow(float64(k), float64(n-1))) * n

	for i := 0; i < c.NumRouters; i++ {
		name := fmt.Sprintf("router_%d", i)
		temp_router := NewSimpleRouter(name, RouterDegree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, i)

		for j := 0; j < RouterDegree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[i] = temp_router
		c.routers[i].AcceptHook(Logger)
		c.routers[i].RouterConnect = c
	}

}

func (c *RouterConnection) BuildScalable_DGX2_Network(Logger *NocStatLogger, numGPUs int, numSwitchs int) {
	c.NumRouters = numGPUs*2 + numSwitchs*2

	if numGPUs%2 != 0 {
		fmt.Printf("numGPUs should be even number")
	}

	for i := 0; i < numGPUs; i++ {
		name := fmt.Sprintf("GPU_Hub_router_%d", i)
		degree := 6 // L2TLB + RDMA + GPU + MMU + Driver + Node
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, i)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[i] = temp_router
		c.routers[i].AcceptHook(Logger)
		c.routers[i].RouterConnect = c
	}

	for i := 0; i < numGPUs; i++ {
		name := fmt.Sprintf("GPU_Node_router_%d", i)
		degree := numSwitchs + 1
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, numGPUs+i)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[numGPUs+i] = temp_router
		c.routers[numGPUs+i].AcceptHook(Logger)
		c.routers[numGPUs+i].RouterConnect = c
	}

	for i := 0; i < numSwitchs; i++ {
		name := fmt.Sprintf("UP_Switch_%d", i)
		degree := numGPUs
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, numGPUs*2+i)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[numGPUs*2+i] = temp_router
		c.routers[numGPUs*2+i].AcceptHook(Logger)
		c.routers[numGPUs*2+i].RouterConnect = c
	}

	for i := 0; i < numSwitchs; i++ {
		name := fmt.Sprintf("DOWN_Switch_%d", i)
		degree := numGPUs
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, numGPUs*2+numSwitchs+i)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[numGPUs*2+numSwitchs+i] = temp_router
		c.routers[numGPUs*2+numSwitchs+i].AcceptHook(Logger)
		c.routers[numGPUs*2+numSwitchs+i].RouterConnect = c
	}

	for i := 0; i < 1; i++ {
		name := fmt.Sprintf("MMU")
		degree := numGPUs + 1
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, numGPUs*2+numSwitchs*2)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[numGPUs*2+numSwitchs*2] = temp_router
		c.routers[numGPUs*2+numSwitchs*2].AcceptHook(Logger)
		c.routers[numGPUs*2+numSwitchs*2].RouterConnect = c
	}

	for i := 0; i < 1; i++ {
		name := fmt.Sprintf("driver")
		degree := numGPUs + 1
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, numGPUs*2+numSwitchs*2+1)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[numGPUs*2+numSwitchs*2+1] = temp_router
		c.routers[numGPUs*2+numSwitchs*2+1].AcceptHook(Logger)
		c.routers[numGPUs*2+numSwitchs*2+1].RouterConnect = c
	}
}

func (c *RouterConnection) ConnectingScalable_DGX2_Network(numGPUs int, numSwitchs int) {
	// Hub -> Node -> Up Switch -> Down Switch

	// Hub Router Setting
	for i := 0; i < numGPUs; i++ {
		c.routers[i].NextPorts[3] = &c.routers[numGPUs+i].InputPorts[0] // Hub -> Node
		c.routers[numGPUs+i].NextPorts[0] = &c.routers[i].InputPorts[3] // Node -> Hub

		c.routers[i].NextPorts[4] = &c.routers[2*numGPUs+2*numSwitchs].InputPorts[i] // Hub -> MMU
		c.routers[2*numGPUs+2*numSwitchs].NextPorts[i] = &c.routers[i].InputPorts[4] // MMU -> Hub

		c.routers[i].NextPorts[5] = &c.routers[2*numGPUs+2*numSwitchs+1].InputPorts[i] // Hub -> Driver
		c.routers[2*numGPUs+2*numSwitchs+1].NextPorts[i] = &c.routers[i].InputPorts[5] // Driver -> Hub
	}

	// Node Router Setting
	for i := 0; i < numGPUs; i++ {

		if i < numGPUs/2 {
			//Node - Up Router
			for j := 1; j <= numSwitchs; j++ {
				c.routers[numGPUs+i].NextPorts[j] = &c.routers[numGPUs*2+j-1].InputPorts[i]
				c.routers[numGPUs*2+j-1].NextPorts[i] = &c.routers[numGPUs+i].InputPorts[j]
			}
		} else {
			//Node - Down Router
			for j := 1; j <= numSwitchs; j++ {
				c.routers[numGPUs+i].NextPorts[j] = &c.routers[numGPUs*2+numSwitchs+j-1].InputPorts[i-numGPUs/2]
				c.routers[numGPUs*2+numSwitchs+j-1].NextPorts[i-numGPUs/2] = &c.routers[numGPUs+i].InputPorts[j]
			}
		}
	}

	// Switch Router Setting
	for i := 0; i < numSwitchs; i++ {
		for j := 0; j < numGPUs/2; j++ {
			//Top -> Down
			c.routers[numGPUs*2+i].NextPorts[numGPUs/2+j] = &c.routers[numGPUs*2+numSwitchs+i].InputPorts[numGPUs/2+j]

			//Down -> Top
			c.routers[numGPUs*2+numSwitchs+i].NextPorts[numGPUs/2+j] = &c.routers[numGPUs*2+i].InputPorts[numGPUs/2+j]
		}

	}
}

func Connect(Topology [][]int, index_a int, index_b int, val int) {
	Topology[index_a][index_b] = val
	Topology[index_b][index_a] = val
}

func (c *RouterConnection) CalculatingMinimalPathOnDGX2_Scalable(SrcIndex int, DstIndex int, flit *Flit) (int, int) {
	numSwitchs := c.Argv1
	numGPUs := c.NumGPUs

	numNodes := numGPUs*3 + numGPUs*2 + numSwitchs*2 + 2 + 2
	Topology := make([][]int, numNodes)
	PortInfo := make([][]int, numNodes)

	for i := 0; i < numNodes; i++ {
		Topology[i] = make([]int, numNodes)
		PortInfo[i] = make([]int, numNodes)
	}

	HubIndexBase := numGPUs * 3
	NodeIndexBase := numGPUs*3 + numGPUs

	for i := 0; i < numGPUs; i++ {
		//TLB , RDMA, GPU -> Hub
		Connect(Topology, 3*i, numGPUs*3+i, 1)
		Connect(Topology, 3*i+1, numGPUs*3+i, 1)
		Connect(Topology, 3*i+2, numGPUs*3+i, 1)

		Connect(PortInfo, 3*i, numGPUs*3+i, 0)
		Connect(PortInfo, 3*i+1, numGPUs*3+i, 1)
		Connect(PortInfo, 3*i+2, numGPUs*3+i, 2)

		// Hub -> Node
		Connect(Topology, HubIndexBase+i, NodeIndexBase+i, 1)

		PortInfo[HubIndexBase+i][NodeIndexBase+i] = 3
		PortInfo[NodeIndexBase+i][HubIndexBase+i] = 0

		// Hub -> Driver,MMU Router
		Connect(Topology, HubIndexBase+i, numNodes-3, 1)
		Connect(Topology, HubIndexBase+i, numNodes-4, 1)

		PortInfo[HubIndexBase+i][numNodes-3] = 4
		PortInfo[HubIndexBase+i][numNodes-4] = 5

		PortInfo[numNodes-3][HubIndexBase+i] = i
		PortInfo[numNodes-4][HubIndexBase+i] = i

		//MMU, Driver Router -> MMU, Driver
		Connect(Topology, numNodes-1, numNodes-3, 1)
		Connect(Topology, numNodes-2, numNodes-4, 1)

		PortInfo[numNodes-1][numNodes-3] = numGPUs
		PortInfo[numNodes-2][numNodes-4] = numGPUs

		PortInfo[numNodes-3][numNodes-1] = numGPUs
		PortInfo[numNodes-4][numNodes-2] = numGPUs
	}

	// Node -> Switch
	for i := 0; i < numGPUs; i++ {
		if i < numGPUs/2 {
			for j := 1; j <= numSwitchs; j++ {
				Connect(Topology, 4*numGPUs+i, 5*numGPUs+j-1, 1)
				PortInfo[4*numGPUs+i][5*numGPUs+j-1] = j
				PortInfo[5*numGPUs+j-1][4*numGPUs+i] = i
			}
		} else {
			for j := 1; j <= numSwitchs; j++ {
				Connect(Topology, 4*numGPUs+i, 5*numGPUs+numSwitchs+j-1, 1)
				PortInfo[4*numGPUs+i][5*numGPUs+numSwitchs+j-1] = j
				PortInfo[5*numGPUs+numSwitchs+j-1][4*numGPUs+i] = i - numGPUs/2
			}

		}
	}

	// Switch <-> Switch
	for i := 0; i < numSwitchs; i++ {
		for j := 0; j < numGPUs/2; j++ {
			Connect(Topology, 5*numGPUs+i, 5*numGPUs+numSwitchs+i, 1)
			PortInfo[5*numGPUs+i][5*numGPUs+numSwitchs+i] = numGPUs/2 + j
			PortInfo[5*numGPUs+numSwitchs+i][5*numGPUs+i] = numGPUs/2 + j
		}

	}

	src := SrcIndex
	dst := DstIndex

	PrevNode := make([]int, numNodes)
	Cost := make([]int, numNodes)
	Queue := make([]int, 0)

	for i := 0; i < numNodes; i++ {
		Cost[i] = 9999
		PrevNode[i] = -1
	}

	Queue = append(Queue, src)

	if (src != numNodes-1 && dst != numNodes-1) && (src != numNodes-2 && dst != numNodes-2) {
		Cost[numNodes-3] = -1
		Cost[numNodes-4] = -1
	}
	Cost[src] = 0

	for {
		if len(Queue) == 0 {
			break
		}
		CurrentNode := Queue[0]
		for i := 0; i < numNodes; i++ {
			if Topology[CurrentNode][i] == 1 {
				NewCost := Cost[CurrentNode] + 1
				if Cost[i] > NewCost {
					Cost[i] = NewCost
					PrevNode[i] = CurrentNode
					Queue = append(Queue, i)
				}
			}
		}
		Queue = Queue[1:]

	}

	CurIndex := PrevNode[dst]
	ReversedPath := make([]int, 0)
	Path := make([]int, 0)
	ReversedPath = append(ReversedPath, dst)
	for {
		if PrevNode[CurIndex] == -1 {
			break
		}
		ReversedPath = append(ReversedPath, CurIndex)
		CurIndex = PrevNode[CurIndex]
	}

	Path = append(Path, src)
	for i := 0; i < len(ReversedPath); i++ {
		Path = append(Path, ReversedPath[len(ReversedPath)-1-i])
	}

	StartRouterIndex := 0
	StartPortIndex := 0
	if Path[0] < 3*numGPUs || Path[0] >= numNodes-2 {
		StartRouterIndex = Path[1] - 3*numGPUs
		StartPortIndex = PortInfo[src][Path[1]]
	} else {
		StartRouterIndex = Path[0] - 3*numGPUs
		StartPortIndex = PortInfo[src][Path[1]]
	}

	//fmt.Printf("router %d  port %d \n",StartRouterIndex, StartPortIndex)
	for i := 1; i < len(Path)-1; i++ {
		NextPort := PortInfo[Path[i]][Path[i+1]]
		flit.RoutingAddress = append(flit.RoutingAddress, NextPort)
		//fmt.Printf("NextPort: %d (%d , %d) \n",NextPort,Path[i],Path[i+1])
	}

	return StartRouterIndex, StartPortIndex
}

func (c *RouterConnection) BuildScalable_DGX1_Network(Logger *NocStatLogger, numGPUs int, PlaneDegree int, numStack int) {
	c.NumRouters = numGPUs*2 + 2 // Hub + Node + MMU + Driver

	for i := 0; i < numGPUs; i++ {
		name := fmt.Sprintf("GPU_Hub_router_%d", i)
		degree := 6 // L2TLB + RDMA + GPU + MMU + Driver + Node
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, i)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[i] = temp_router
		c.routers[i].AcceptHook(Logger)
		c.routers[i].RouterConnect = c
	}

	for i := 0; i < numGPUs; i++ {
		name := fmt.Sprintf("GPU_Node_router_%d", i)
		degree := 1 + (PlaneDegree - 1) + (numStack - 1)
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, numGPUs+i)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[numGPUs+i] = temp_router
		c.routers[numGPUs+i].AcceptHook(Logger)
		c.routers[numGPUs+i].RouterConnect = c
	}

	for i := 0; i < 1; i++ {
		name := fmt.Sprintf("MMU_router")
		degree := numGPUs + 1
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, 2*numGPUs)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[2*numGPUs] = temp_router
		c.routers[2*numGPUs].AcceptHook(Logger)
		c.routers[2*numGPUs].RouterConnect = c
	}

	for i := 0; i < 1; i++ {
		name := fmt.Sprintf("driver_router")
		degree := numGPUs + 1
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, 2*numGPUs+1)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[2*numGPUs+1] = temp_router
		c.routers[2*numGPUs+1].AcceptHook(Logger)
		c.routers[2*numGPUs+1].RouterConnect = c
	}

}

func (c *RouterConnection) ConnectingScalable_DGX1_Network(numGPUs int, PlaneDegree int, numStack int) {
	//Hub router => L2TLB / RDMA / GPU / Node Router ..  // MMU + Driver

	// Hub Router
	for i := 0; i < numGPUs; i++ {
		c.routers[i].NextPorts[3] = &c.routers[numGPUs+i].InputPorts[0] // Hub -> Node
		c.routers[numGPUs+i].NextPorts[0] = &c.routers[i].InputPorts[3] // Node -> Hub

		c.routers[i].NextPorts[4] = &c.routers[2*numGPUs].InputPorts[i] // Hub -> MMU
		c.routers[2*numGPUs].NextPorts[i] = &c.routers[i].InputPorts[4] // MMU -> Hub

		c.routers[i].NextPorts[5] = &c.routers[2*numGPUs+1].InputPorts[i] // Hub -> Driver
		c.routers[2*numGPUs+1].NextPorts[i] = &c.routers[i].InputPorts[5] // Driver -> Hub
	}

	// Node Router
	for i := 0; i < numStack; i++ {
		// flattend butterfly connection on single plane (fully connected)
		for j := 0; j < PlaneDegree-1; j++ {
			RouterIndex := numGPUs + i*PlaneDegree + j

			for k := j + 1; k < PlaneDegree; k++ {
				c.routers[RouterIndex].NextPorts[k] = &c.routers[RouterIndex+(k-j)].InputPorts[j+1]
				c.routers[RouterIndex+(k-j)].NextPorts[j+1] = &c.routers[RouterIndex].InputPorts[k]
			}
		}
	}

	for i := 0; i < PlaneDegree; i++ {
		for j := 0; j < numStack-1; j++ {
			RouterIndex := numGPUs + j*PlaneDegree + i

			for k := j + 1; k < numStack; k++ {
				c.routers[RouterIndex].NextPorts[PlaneDegree+k-1] = &c.routers[RouterIndex+(k-j)*PlaneDegree].InputPorts[PlaneDegree+j]
				c.routers[RouterIndex+(k-j)*PlaneDegree].NextPorts[PlaneDegree+j] = &c.routers[RouterIndex].InputPorts[PlaneDegree+k-1]
			}
		}
	}

}

func (c *RouterConnection) CalculatingMinimalPathOnDGX1_Scalable(SrcIndex int, DstIndex int, flit *Flit) (int, int) {

	PlaneDegree := c.Argv1
	numStack := c.Argv2

	numGPUs := c.NumGPUs

	if c.NumGPUs != PlaneDegree*numStack {
		log.Panic("Wrong DGX-1 Configuration")
	}

	numNodes := numGPUs*3 + numGPUs*2 + 1 + 1 + 2
	Topology := make([][]int, numNodes)
	PortInfo := make([][]int, numNodes)

	for i := 0; i < numNodes; i++ {
		Topology[i] = make([]int, numNodes)
		PortInfo[i] = make([]int, numNodes)
	}
	HubIndexBase := numGPUs * 3
	NodeIndexBase := numGPUs*3 + numGPUs

	// Hub Router
	for i := 0; i < numGPUs; i++ {

		// TLB, RDMA, GPU

		Connect(Topology, 3*i, HubIndexBase+i, 1)
		Connect(Topology, 3*i+1, HubIndexBase+i, 1)
		Connect(Topology, 3*i+2, HubIndexBase+i, 1)

		Connect(PortInfo, 3*i, HubIndexBase+i, 0)
		Connect(PortInfo, 3*i+1, HubIndexBase+i, 1)
		Connect(PortInfo, 3*i+2, HubIndexBase+i, 2)

		// Hub -> Node

		Connect(Topology, HubIndexBase+i, NodeIndexBase+i, 1)
		PortInfo[HubIndexBase+i][NodeIndexBase+i] = 3
		PortInfo[NodeIndexBase+i][HubIndexBase+i] = 0

		// Hub -> MMU, Driver Router

		Connect(Topology, HubIndexBase+i, numNodes-3, 1)
		Connect(Topology, HubIndexBase+i, numNodes-4, 1)

		PortInfo[HubIndexBase+i][numNodes-3] = 4
		PortInfo[HubIndexBase+i][numNodes-4] = 5

		PortInfo[numNodes-3][HubIndexBase+i] = i
		PortInfo[numNodes-4][HubIndexBase+i] = i

		// MMU, Driver Router -> MMU, Driver
		Connect(Topology, numNodes-1, numNodes-3, 1)
		Connect(Topology, numNodes-2, numNodes-4, 1)

		Connect(PortInfo, numNodes-1, numNodes-3, numGPUs)
		Connect(PortInfo, numNodes-2, numNodes-4, numGPUs)
	}

	// Node Router
	for i := 0; i < numStack; i++ {
		for j := 0; j < PlaneDegree-1; j++ {
			RouterIndex := numGPUs + i*PlaneDegree + j

			for k := j + 1; k < PlaneDegree; k++ {
				Topology[RouterIndex+numGPUs*3][RouterIndex+numGPUs*3+(k-j)] = 1
				Topology[RouterIndex+numGPUs*3+(k-j)][RouterIndex+numGPUs*3] = 1

				PortInfo[RouterIndex+numGPUs*3][RouterIndex+numGPUs*3+(k-j)] = k
				PortInfo[RouterIndex+numGPUs*3+(k-j)][RouterIndex+numGPUs*3] = j + 1
			}
		}
	}

	for i := 0; i < PlaneDegree; i++ {
		for j := 0; j < numStack-1; j++ {
			RouterIndex := numGPUs + j*PlaneDegree + i

			for k := j + 1; k < numStack; k++ {
				Topology[RouterIndex+numGPUs*3][RouterIndex+numGPUs*3+(k-j)*PlaneDegree] = 1
				Topology[RouterIndex+numGPUs*3+(k-j)*PlaneDegree][RouterIndex+numGPUs*3] = 1

				PortInfo[RouterIndex+numGPUs*3][RouterIndex+numGPUs*3+(k-j)*PlaneDegree] = PlaneDegree + k - 1
				PortInfo[RouterIndex+numGPUs*3+(k-j)*PlaneDegree][RouterIndex+numGPUs*3] = PlaneDegree + j
			}
		}
	}

	//just for debugging
	/*
		for i := 0; i < numNodes; i++ {
			for j := 0; j < numNodes; j++ {
				fmt.Printf("%d ", Topology[i][j])
			}
			fmt.Printf("\n")
		}
	*/

	src := SrcIndex
	dst := DstIndex

	PrevNode := make([]int, numNodes)
	Cost := make([]int, numNodes)
	Queue := make([]int, 0)

	for i := 0; i < numNodes; i++ {
		Cost[i] = 9999
		PrevNode[i] = -1
	}

	Queue = append(Queue, src)

	if (src != numNodes-1 && dst != numNodes-1) && (src != numNodes-2 && dst != numNodes-2) {
		Cost[numNodes-3] = -1
		Cost[numNodes-4] = -1
	}

	Cost[src] = 0

	for {
		if len(Queue) == 0 {
			break
		}
		CurrentNode := Queue[0]
		for i := 0; i < numNodes; i++ {
			if Topology[CurrentNode][i] == 1 {
				NewCost := Cost[CurrentNode] + 1
				if Cost[i] > NewCost {
					Cost[i] = NewCost
					PrevNode[i] = CurrentNode
					Queue = append(Queue, i)
				}
			}
		}
		Queue = Queue[1:]

	}

	CurIndex := PrevNode[dst]
	ReversedPath := make([]int, 0)
	Path := make([]int, 0)
	ReversedPath = append(ReversedPath, dst)
	for {
		if PrevNode[CurIndex] == -1 {
			break
		}
		ReversedPath = append(ReversedPath, CurIndex)
		CurIndex = PrevNode[CurIndex]
	}

	Path = append(Path, src)
	for i := 0; i < len(ReversedPath); i++ {
		Path = append(Path, ReversedPath[len(ReversedPath)-1-i])
	}

	StartRouterIndex := 0
	StartPortIndex := 0
	if Path[0] < 3*numGPUs || Path[0] >= numNodes-2 {
		StartRouterIndex = Path[1] - 3*numGPUs
		StartPortIndex = PortInfo[src][Path[1]]
	} else {
		StartRouterIndex = Path[0] - 3*numGPUs
		StartPortIndex = PortInfo[src][Path[1]]
	}

	for i := 1; i < len(Path)-1; i++ {
		NextPort := PortInfo[Path[i]][Path[i+1]]
		flit.RoutingAddress = append(flit.RoutingAddress, NextPort)
		//fmt.Printf("NextPort: %d (%d , %d) \n", NextPort, Path[i], Path[i+1])
	}

	/*
		if DstIndex == 43 {
			fmt.Printf("%s -> %s \n", flit.Src().Component().Name(), flit.Dst().Component().Name())
			for i := 0; i < len(flit.RoutingAddress); i++ {
				fmt.Printf("%d ", flit.RoutingAddress[i])
			}
			fmt.Printf("\n")
		}
	*/

	return StartRouterIndex, StartPortIndex
}

// build mesh network,  n -> diameter..? , dim -> dimension of network ..
func (c *RouterConnection) BuildMeshNetwork(n int, dim int, Logger *NocStatLogger) {

	c.NumRouters = int(math.Pow(float64(n), float64(dim)))
	//c.RouterInputDegree = 2*dim + 1

	degree := 2*dim + 1 + 1

	for i := 0; i < c.NumRouters; i++ {
		name := fmt.Sprintf("router_%d", i)
		temp_router := NewSimpleRouter(name, degree, c.RouterBufferSize, c.VCcounts, c.engine, c.RouterFreq, i)
		for j := 0; j < degree; j++ {
			temp_router.InputPorts[j].SetConnection(c)
		}
		c.routers[i] = temp_router
		c.routers[i].AcceptHook(Logger)
		c.routers[i].RouterConnect = c
	}
}

func (c *RouterConnection) ConnectingMeshNetwork(n int, dim int) {

	totalNum := c.NumRouters

	// we support maximum 4-dimension mesh network
	Routers := make([][][][]*SimpleRouter, n)
	for i := 0; i < n; i++ {
		Routers[i] = make([][][]*SimpleRouter, n)
		for j := 0; j < n; j++ {
			Routers[i][j] = make([][]*SimpleRouter, n)
			for k := 0; k < n; k++ {
				Routers[i][j][k] = make([]*SimpleRouter, n)
			}
		}
	}

	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			for k := 0; k < n; k++ {
				for l := 0; l < n; l++ {
					index := l + n*k + n*n*j + n*n*n*i
					if index >= totalNum {
						i = n
						j = n
						k = n
						break
					}
					Routers[i][j][k][l] = c.routers[l+n*k+n*n*j+n*n*n*i]
				}
			}
		}
	}

	// case 1 - D

	Controller := make([]int, 4)

	for i := 0; i < 4; i++ {
		if i < dim {
			Controller[i] = n
		} else {
			Controller[i] = 1
		}
	}

	// Connect start ...
	for i := 0; i < Controller[3]; i++ {
		for j := 0; j < Controller[2]; j++ {
			for k := 0; k < Controller[1]; k++ {
				for l := 0; l < Controller[0]; l++ {

					var Target4Port1 akita.Port
					var Target4Port2 akita.Port
					var Target4Port3 akita.Port
					var Target4Port4 akita.Port
					var Target4Port5 akita.Port
					var Target4Port6 akita.Port

					// 1-D case
					if l+1 < n {
						Target4Port1 = (Routers[i][j][k][l+1].InputPorts[2])
					}

					if l-1 > -1 {
						Target4Port2 = (Routers[i][j][k][l-1].InputPorts[1])
					}
					Routers[i][j][k][l].NextPorts[1] = &Target4Port1
					Routers[i][j][k][l].NextPorts[2] = &Target4Port2

					if dim > 1 {
						if k+1 < n {
							Target4Port3 = (Routers[i][j][k+1][l].InputPorts[4])
						}

						if k-1 > -1 {
							Target4Port4 = (Routers[i][j][k-1][l].InputPorts[3])
						}

						Routers[i][j][k][l].NextPorts[3] = &Target4Port3
						Routers[i][j][k][l].NextPorts[4] = &Target4Port4

						if dim > 2 {

							if j+1 < n {
								Target4Port5 = (Routers[i][j+1][k][l].InputPorts[6])
							}

							if j-1 > -1 {
								Target4Port6 = (Routers[i][j-1][k][l].InputPorts[5])
							}

							Routers[i][j][k][l].NextPorts[5] = &Target4Port5
							Routers[i][j][k][l].NextPorts[6] = &Target4Port6
						}
					}

				}
			}
		}
	}

	// Copy..
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			for k := 0; k < n; k++ {
				for l := 0; l < n; l++ {
					index := l + n*k + n*n*j + n*n*n*i
					if index >= totalNum {
						i = n
						j = n
						k = n
						break
					}
					c.routers[l+n*k+n*n*j+n*n*n*i] = Routers[i][j][k][l]
				}
			}
		}
	}

}

func (c *RouterConnection) CalculatingMesh(n int, dim int, SrcIndex int, DstIndex int, packet *Flit) (int, int) {

	totalNum := c.NumRouters
	SrcInfo := make([]int, 4)
	DstInfo := make([]int, 4)

	// find index for Src, Dst ..
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			for k := 0; k < n; k++ {
				for l := 0; l < n; l++ {
					index := l + n*k + n*n*j + n*n*n*i

					if index == SrcIndex {
						SrcInfo[0] = i
						SrcInfo[1] = j
						SrcInfo[2] = k
						SrcInfo[3] = l
					}

					if index == DstIndex {
						DstInfo[0] = i
						DstInfo[1] = j
						DstInfo[2] = k
						DstInfo[3] = l
					}
					if index >= totalNum {
						i = n
						j = n
						k = n
						break
					}
				}
			}
		}
	}

	// X Routing

	curSize := 0
	for i := 0; i < dim; i++ {
		Result := DstInfo[3-i] - SrcInfo[3-i]

		//FillUpPath(Result, 2*i+2, 2*i+1, packet.RoutingAddress, curSize)
		positive := 2*i + 1
		negative := 2*i + 2
		selectedNum := positive
		if Result < 0 {
			Result = Result * (-1)
			selectedNum = negative
		}

		for i := 0; i < Result; i++ {
			packet.RoutingAddress = append(packet.RoutingAddress, selectedNum)
		}
		curSize = curSize + Result
	}
	packet.RoutingAddress = append(packet.RoutingAddress, 0)

	// should return index of router and port

	return SrcIndex, 0
}
