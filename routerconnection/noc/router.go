package noc

import (
	"fmt"
	"log"
	"math/rand"
	"reflect"
	"sync"
	"time"

	"gitlab.com/akita/akita"
)

type SendResult struct {
	RouterIndex  int
	ChannelIndex int
	FlitSize     int
}

func NewSendResult(RouterIndex int, ChannelIndex int, FlitSize int) *SendResult {
	info := new(SendResult)
	info.RouterIndex = RouterIndex
	info.ChannelIndex = ChannelIndex
	info.FlitSize = FlitSize

	return info
}

type SimpleRouter struct {
	akita.HookableBase
	RouterIndex int
	*akita.ComponentBase
	sync.Mutex

	engine akita.Engine
	ticker *akita.Ticker

	Freq akita.Freq

	numInputUnit  int
	numOutputUnit int
	numVC         int
	flitSize      int

	RoutingStageDelay  int
	VCAllocStageDelay  int
	SWAllocStageDelay  int
	SWTravelStageDelay int

	RouterConnect *RouterConnection

	// numInputUnit == number of ToTop ports
	InputPorts []akita.Port

	// numOutputUnit == number of ToBottom ports
	NextPorts []*akita.Port

	// for Arbitration
	UnitPriorityBuffer [][]int
	UnitPriority       []int

	NeedTicking bool

	RoutingStageBuffer     [][][]*Flit
	SwitchAllocStageBuffer [][][]*Flit
	VCAllocStageBuffer     [][][]*Flit

	RCDelayBuffer [][][]*Flit
	VADelayBuffer [][][]*Flit
	SADelayBuffer [][]*Flit
	STDelayBuffer [][]*Flit

	OutputPort      [][]*Flit
	OutputVCStatus  [][]bool // [InputUnitIndex][IndexOfChannels]
	InputVCStatus   [][]bool
	InputVCOwner    [][]akita.Req
	NewCreditCounts [][]int // [InputUnitIndex][IndexOfChannels]
	NewStallFlag    [][]bool
}

//Making a routing table
func (r *SimpleRouter) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {

}

func (r *SimpleRouter) NotifyRecv(now akita.VTimeInSec, port akita.Port) {

	r.ticker.TickLater(now)
}

func (r *SimpleRouter) RC2VA(e akita.Event) error {

	for i := 0; i < r.numInputUnit; i++ {
		for j := 0; j < r.numVC; j++ {
			if len(r.RCDelayBuffer[i][j]) != 0 {
				entry := r.RCDelayBuffer[i][j][0]

				r.VCAllocStageBuffer[i][j] = append(r.VCAllocStageBuffer[i][j], entry)

				//pop
				r.RCDelayBuffer[i][j] = r.RCDelayBuffer[i][j][1:]
			}
		}
	}

	return nil
}

func (r *SimpleRouter) VA2SA(e akita.Event) error {

	for i := 0; i < r.numInputUnit; i++ {
		for j := 0; j < r.numVC; j++ {
			if len(r.VADelayBuffer[i][j]) != 0 {
				entry := r.VADelayBuffer[i][j][0]

				r.SwitchAllocStageBuffer[i][j] = append(r.SwitchAllocStageBuffer[i][j], entry)

				//pop
				r.VADelayBuffer[i][j] = r.VADelayBuffer[i][j][1:]
			}
		}
	}

	return nil
}

func (r *SimpleRouter) SA2ST(e akita.Event) error {
	for i := 0; i < r.numOutputUnit; i++ {
		if len(r.SADelayBuffer[i]) != 0 {
			entry := r.SADelayBuffer[i][0]
			r.OutputPort[i] = append(r.OutputPort[i], entry)

			//pop
			r.SADelayBuffer[i] = r.SADelayBuffer[i][1:]
		}
	}

	return nil
}

func (r *SimpleRouter) ST2DST(e akita.Event) error {

	return nil
}

func (r *SimpleRouter) Handle(e akita.Event) error {

	switch e := e.(type) {
	case akita.TickEvent:
		return r.handleTickEvent(e)
	case *RoutingEvent:
		return r.RC2VA(e)
	case *VCAllocateEvent:
		return r.VA2SA(e)
	case *SwitchAllocateEvent:
		return r.SA2ST(e)

	default:
		log.Panicf("cannot handle event of %s", reflect.TypeOf(e))
	}
	return nil
}

func (r *SimpleRouter) SendSchedule(now akita.VTimeInSec, req akita.Req) {
	transferTime := r.Freq.NCyclesLater(r.SWTravelStageDelay-1, now)
	transferEvent := NewSendToDstEvent(transferTime, r.RouterConnect, req)
	r.engine.Schedule(transferEvent)
}

func (r *SimpleRouter) handleTickEvent(e akita.TickEvent) error {
	now := e.Time()

	r.NeedTicking = false

	//r.RouterReport(now) -> Debugging
	r.NewStallFlagInitialize()
	r.ReqSend(now)
	r.GreedySwitchAllocStage(now)
	r.VCAllocStage(now)
	r.RoutingStage(now)
	r.InputReqHandling(now)

	if r.NeedTicking {
		r.ticker.TickLater(now)
	}

	return nil
}

// For Debugging
func (r *SimpleRouter) CreditReport() {
	for i := 0; i < r.numInputUnit; i++ {
		for j := 0; j < r.numVC; j++ {
			//if r.NewCreditCounts[i][j] == 0 {
			log.Printf("%s -- [%d][%d] : %d \n", r.Name(), i, j, r.NewCreditCounts[i][j])
			//}
		}
	}
}

func (r *SimpleRouter) NewStallFlagInitialize() {
	for i := 0; i < r.numInputUnit; i++ {
		for j := 0; j < r.numVC; j++ {
			r.NewStallFlag[i][j] = false
		}
	}
}

func (r *SimpleRouter) SetNextVC(req akita.Req, VCIndex int) {
	packet := r.RouterConnect.Req2FlitMapping[req]
	delete(r.RouterConnect.Req2FlitMapping, packet)
	packet.NextVCIndex = VCIndex
	r.RouterConnect.Req2FlitMapping[req] = packet
}

func (r *SimpleRouter) GetNextVC(req akita.Req) int {

	packet := r.RouterConnect.Req2FlitMapping[req]
	return packet.NextVCIndex
}

func (r *SimpleRouter) GetDestination(req akita.Req) int {
	// based on request info (Actually, packet info.. ), decide the output port !!
	// return output port index

	// first, we need translate a req to a packet
	packet := r.RouterConnect.Req2FlitMapping[req]
	if packet == nil {
		fmt.Printf("Error ! \n")
	}
	outputportindex := packet.GetCurrentRoutingAddress()

	return outputportindex
}

func (r *SimpleRouter) ReqSend(now akita.VTimeInSec) {
	for i := 0; i < r.numOutputUnit; i++ {

		if len(r.OutputPort[i]) > 0 {
			Req := r.OutputPort[i][0]

			r.SendSchedule(now, Req)
			Channel := Req.NextVCIndex

			Req.LastPortCreditPointer = &r.NewCreditCounts[i][Channel]
			Req.LastRouterPointer = r
			Req.LastPortIndex = i
			Req.LastVCIndex = Channel

			r.OutputPort[i] = r.OutputPort[i][1:]

			r.NeedTicking = true
		}
	}
}

func (r *SimpleRouter) InputReqHandling(now akita.VTimeInSec) {

	for i := 0; i < r.numInputUnit; i++ {
		if r.InputPorts[i].Peek() != nil {

			flit := r.InputPorts[i].Peek()

			switch flit := flit.(type) {
			case Flit:
				CurrentVCIndex := flit.NextVCIndex

				if r.NewStallFlag[i][CurrentVCIndex] != true {
					r.InputVCOwner[i][CurrentVCIndex] = flit.Req

					/*
						if flit.IsHead == true {
							r.InputVCStatus[i][CurrentVCIndex] = false
						}
					*/

					r.InputPorts[i].Retrieve(now)
					r.RoutingStageBuffer[i][CurrentVCIndex] = append(r.RoutingStageBuffer[i][CurrentVCIndex], &flit)
					r.NeedTicking = true
				}

			default:
				log.Panicf("cannot handle request of type %s ", reflect.TypeOf(flit))
			}
		}
	}
}

func (r *SimpleRouter) RoutingStage(now akita.VTimeInSec) {
	//log.Printf("time:%0.12f  RoutingStage \n",now)

	for i := 0; i < r.numInputUnit; i++ {
		for j := 0; j < r.numVC; j++ {
			if len(r.RoutingStageBuffer[i][j]) > 0 && r.NewStallFlag[i][j] != true {
				req := r.RoutingStageBuffer[i][j][0]
				r.RoutingStageBuffer[i][j] = r.RoutingStageBuffer[i][j][1:]
				// caculating Dst Part ..
				//DstIndex := r.GetDestination(req)

				//r.VCAllocStageBuffer[i][j] = append(r.VCAllocStageBuffer[i][j], req)
				r.RCDelayBuffer[i][j] = append(r.RCDelayBuffer[i][j], req)
				transferTime := r.Freq.NCyclesLater(r.RoutingStageDelay-1, now)
				RCDelayEvent := NewRoutingEvent(transferTime, r)
				r.engine.Schedule(RCDelayEvent)

				r.NeedTicking = true
			}
			unitpriority := r.UnitPriority[i]
			r.UnitPriorityBuffer[i] = append(r.UnitPriorityBuffer[i], unitpriority)
		}
	}
}

func (r *SimpleRouter) VCAllocStage(now akita.VTimeInSec) {

	for i := 0; i < r.numOutputUnit; i++ {
		for j := 0; j < r.numVC; j++ {
			if len(r.VCAllocStageBuffer[i][j]) > 0 && r.NewStallFlag[i][j] != true {
				req := r.VCAllocStageBuffer[i][j][0] // peek

				// find availabe output VC Channels
				DstIndex := req.GetCurrentRoutingAddress()
				checker := false

				SelectedOutputChannelIndex := r.VCAllocator(i, j, req)
				if r.NewCreditCounts[DstIndex][SelectedOutputChannelIndex] > 0 {
					checker = true
				}

				// It means there are no available output VCs
				if checker == false {
					//fmt.Printf("Not Enough Credits on VCAlloc Stage \n")
					r.NewStallFlag[i][j] = true
					r.NeedTicking = true
				} else {
					//SelectedOutputChannelIndex := r.VCAllocator(i, j, req)
					//r.SetNextVC(req, SelectedOutputChannelIndex)
					req.NextVCIndex = SelectedOutputChannelIndex
					r.VCAllocStageBuffer[i][j] = r.VCAllocStageBuffer[i][j][1:]

					//r.SwitchAllocStageBuffer[i][j] = append(r.SwitchAllocStageBuffer[i][j], req)

					r.VADelayBuffer[i][j] = append(r.VADelayBuffer[i][j], req)
					transferTime := r.Freq.NCyclesLater(r.VCAllocStageDelay-1, now)
					VCDelayEvent := NewVCAllocEvent(transferTime, r)
					r.engine.Schedule(VCDelayEvent)

					r.NeedTicking = true
				}
			}
		}
	}

}

func (r *SimpleRouter) GreedySwitchAllocStage(now akita.VTimeInSec) {

	ArbitResults := make([]int, r.numInputUnit)

	for i := 0; i < r.numInputUnit; i++ {
		ArbitResults[i] = -1
	}

	for i := 0; i < r.numOutputUnit; i++ {
		for j := 0; j < r.numInputUnit; j++ {
			Counter := 0
			for k := 0; k < r.numVC; k++ {
				if len(r.SwitchAllocStageBuffer[j][k]) > 0 {
					req := r.SwitchAllocStageBuffer[j][k][0]
					//DstIndex := r.GetDestination(req)
					DstIndex := req.GetCurrentRoutingAddress()
					Counter++
					NextVC := req.NextVCIndex
					//NextVC := r.GetNextVC(req)
					if DstIndex == i && r.NewCreditCounts[DstIndex][NextVC] > 0 {
						// Switch Allocation !
						if req.IsHead == true {
							r.InputVCStatus[j][k] = true
						}

						r.SwitchAllocStageBuffer[j][k] = r.SwitchAllocStageBuffer[j][k][1:]
						//r.OutputPort[DstIndex] = append(r.OutputPort[DstIndex], req)

						r.SADelayBuffer[DstIndex] = append(r.SADelayBuffer[DstIndex], req)
						transferTime := r.Freq.NCyclesLater(r.SWAllocStageDelay-1, now)
						SADelayEvent := NewSwitchAllocateEvent(transferTime, r)
						r.engine.Schedule(SADelayEvent)

						r.NewCreditCounts[DstIndex][NextVC]--
						Counts := r.NewCreditCounts[DstIndex][NextVC]
						if Counts == 0 {
							r.OutputVCStatus[DstIndex][NextVC] = false
						}
						//r.NewCreditReturn(j, k, req, now)
						r.NewCreditReturn(req.LastPortIndex, req.LastVCIndex, req, now)
						r.NeedTicking = true

						ArbitResults[j] = k

						j = r.numInputUnit
						k = r.numVC

					}
				}
			}
		}
	}

	// Stall.. !
	for i := 0; i < r.numInputUnit; i++ {
		for j := 0; j < r.numVC; j++ {
			if ArbitResults[i] != j && len(r.SwitchAllocStageBuffer[i][j]) > 0 {
				r.NewStallFlag[i][j] = true
				r.NeedTicking = true
			}
		}
	}

}

func (r *SimpleRouter) NewSwitchAllocStage(now akita.VTimeInSec) {

	// for random arbitration
	rand.Seed(time.Now().UnixNano())

	DstPortBuffer := make([][]int, r.numOutputUnit)
	Find_InputUnit_SelectedVC := make([]int, r.numInputUnit) // Find..[InputUnit Index] = selected VC Index

	PossibleList := make([][]int, r.numInputUnit)

	for i := 0; i < r.numInputUnit; i++ {

		PossibleList[i] = make([]int, 0)
		for j := 0; j < r.numVC; j++ {
			if len(r.SwitchAllocStageBuffer[i][j]) > 0 {
				PossibleList[i] = append(PossibleList[i], j)
			}
		}

		if len(PossibleList[i]) > 0 {
			varRange := len(PossibleList[i])
			selectedIndex := rand.Intn(varRange)

			SelectedChannelIndex := PossibleList[i][selectedIndex]
			Find_InputUnit_SelectedVC[i] = SelectedChannelIndex

			for j := 0; j < len(PossibleList); j++ {
				if j != selectedIndex {
					Channel := PossibleList[i][j]
					r.NewStallFlag[i][Channel] = true
					r.NeedTicking = true
				}
			}

			req := r.SwitchAllocStageBuffer[i][SelectedChannelIndex][0] // peek
			//log.Printf("selected  unit %d channel %d  -  req : %s \n", i,SelectedChannelIndex,req.GetID())
			dstPortIndex := r.GetDestination(req)
			DstPortBuffer[dstPortIndex] = append(DstPortBuffer[dstPortIndex], i) // remember it's input unit index !
		}
	}

	for i := 0; i < r.numOutputUnit; i++ {
		if len(DstPortBuffer[i]) > 0 {

			//need arbitration
			selectedInputUnit := 0
			if len(DstPortBuffer[i]) > 1 {

				//Random Arbitration
				varRange := len(DstPortBuffer[i])
				selectedInputUnit = rand.Intn(varRange)

				//Round Robin
				//selectedIndex := r.RRArbitration(varRange, i)
				//selectedIndex := r.RRArbitration2(DstPortBuffer[i])
				//selectedInputUnitIndex := DstPortBuffer[i][selectedIndex]

				for j := 0; j < len(DstPortBuffer[i]); j++ {
					if j != selectedInputUnit {
						InputUnitIndex := DstPortBuffer[i][j]
						ChannelIndex := Find_InputUnit_SelectedVC[InputUnitIndex]
						r.NewStallFlag[InputUnitIndex][ChannelIndex] = true
						r.NeedTicking = true

						//fmt.Printf("Stall Selc  unit %d channel %d  - for Output : %d \n", InputUnitIndex ,ChannelIndex,i)
					}
				}

			}
			UnitIndex := DstPortBuffer[i][selectedInputUnit]
			ChannelIndex := Find_InputUnit_SelectedVC[UnitIndex]
			req := r.SwitchAllocStageBuffer[UnitIndex][ChannelIndex][0]
			r.SwitchAllocStageBuffer[UnitIndex][ChannelIndex] = r.SwitchAllocStageBuffer[UnitIndex][ChannelIndex][1:]
			//fmt.Printf("Finally Selc  unit %d channel %d  - for Output : %d \n", UnitIndex,ChannelIndex,i)

			//decrease credit + creditreturn
			NextChannel := r.GetNextVC(req)
			r.NewCreditCounts[i][NextChannel]--
			Count := r.NewCreditCounts[i][NextChannel]

			// Should call credit return
			r.NewCreditReturn(UnitIndex, ChannelIndex, req, now)

			if Count == 0 {
				r.OutputVCStatus[i][NextChannel] = false
			}

			r.OutputPort[i] = append(r.OutputPort[i], req)
			r.NeedTicking = true
		}

	}

}

func (r *SimpleRouter) NewCreditReturn(LastPortIndex int, LastVCIndex int, req *Flit, now akita.VTimeInSec) {
	//packet := r.RouterConnect.Req2FlitMapping[req]

	if req.LastPortCreditPointer != nil {
		(*req.LastPortCreditPointer)++
		PrevRouter := req.LastRouterPointer
		//fmt.Printf("							%s   ->  %s 						%s: LASPORT:%d LastVC:%d \n", req.Src().Component().Name(), req.Dst().Component().Name(), PrevRouter.Name(), LastPortIndex, LastVCIndex)
		PrevRouter.OutputVCStatus[LastPortIndex][LastVCIndex] = true
		PrevRouter.ticker.TickLater(now)
	}
}

func (r *SimpleRouter) VCAllocator(InputPortIndex int, InputChannelIndex int, req *Flit) int {

	DstIndex := req.GetCurrentRoutingAddress()
	/*
		// try to avoid H.O.L effect..

		// find available VC index .. !
		PossibleList := make([]int, 0)
		for i := 0; i < r.numVC; i++ {
			if r.OutputVCStatus[DstIndex][i] == true {
				PossibleList = append(PossibleList, i)
			}
		}

		if len(PossibleList) == 0 {
			log.Printf("Error..")
		}

		varRange := len(PossibleList)
			selected := rand.Intn(varRange)
	*/

	return DstIndex % (r.numVC)
}

func (r *SimpleRouter) RRArbitration2(IndexArray []int) int {
	// intput : array which consist of input index
	// output : index of input array which has the highest prioirty

	ArraySize := len(IndexArray)

	maxPriority := -1
	maxPriorityIndex := 0
	for i := 0; i < ArraySize; i++ {
		if r.UnitPriority[IndexArray[i]] > maxPriority {
			maxPriority = r.UnitPriority[IndexArray[i]]
			maxPriorityIndex = i
		}
	}

	// and change the priority
	for i := 0; i < IndexArray[maxPriorityIndex]; i++ {
		r.UnitPriority[i]++
	}
	r.UnitPriority[IndexArray[maxPriorityIndex]] = 0

	return maxPriorityIndex
}

func (r *SimpleRouter) RRArbitration(EntrySize int, InputUnitIndex int) int {

	maxPriority := -1
	maxPriorityIndex := 0

	// find the highest priority element !
	for i := 0; i < EntrySize; i++ {

		if r.UnitPriorityBuffer[InputUnitIndex][i] > maxPriority {
			maxPriority = r.UnitPriorityBuffer[InputUnitIndex][i]
			maxPriorityIndex = i
		}
	}

	// priority update ! ( the highest priority element should have the lowest priority, and the others should be increased )

	// find the index of element which have maximum priority in this case
	TempIndex := 0
	for i := 0; i < r.numInputUnit; i++ {
		if maxPriority == r.UnitPriority[i] {
			TempIndex = i
		}
	}

	// and then, except that element, increase priority of the other elements
	for i := 0; i < r.numInputUnit; i++ {
		if i != TempIndex {
			r.UnitPriority[i]++
		}
	}

	// finally, assign 0(zero) priority to the element which selected
	r.UnitPriority[TempIndex] = 0

	return maxPriorityIndex
}

func (r *SimpleRouter) RouterReport(now akita.VTimeInSec) {
	// Print status of current Router

	fmt.Printf("=============== %s Report =============== time:%0.12f \n", r.Name(), now)
	fmt.Printf("	 ToTop			Routing				 VCAlloc			SWAlloc		 OutPut \n")
	for i := 0; i < r.numInputUnit; i++ {
		for j := 0; j < r.numVC; j++ {
			IsStall := "No"
			if r.NewStallFlag[i][j] == true {
				IsStall = "Yes"
			}
			fmt.Printf("[%d][%d]:   %d				%d				%d				%d				%d     Stall:%s\n", i, j,
				99999,
				len(r.RoutingStageBuffer[i][j]),
				len(r.VCAllocStageBuffer[i][j]),
				len(r.SwitchAllocStageBuffer[i][j]),
				len(r.OutputPort[i]),
				IsStall)
		}
	}

}

func NewSimpleRouter(name string, UnitSize int, BufferSize int, NumOfVC int, engine akita.Engine, freq akita.Freq, RouterIndex int) *SimpleRouter {
	router := new(SimpleRouter)
	router.RouterIndex = RouterIndex
	//router.TickingComponent = akita.NewTickingComponent(name, engine, freq, router)
	router.ComponentBase = akita.NewComponentBase(name)
	router.engine = engine
	router.Freq = freq
	router.ticker = akita.NewTicker(router, engine, freq)
	router.numInputUnit = UnitSize
	router.numOutputUnit = UnitSize
	router.numVC = NumOfVC

	router.RoutingStageDelay = 1
	router.VCAllocStageDelay = 1
	router.SWAllocStageDelay = 1
	router.SWTravelStageDelay = 1

	router.InputPorts = make([]akita.Port, UnitSize)
	router.NextPorts = make([]*akita.Port, UnitSize)

	//just for debugging
	router.UnitPriority = make([]int, UnitSize)
	router.UnitPriorityBuffer = make([][]int, UnitSize)

	// delaybuffer
	router.RCDelayBuffer = make([][][]*Flit, router.numInputUnit)
	router.VADelayBuffer = make([][][]*Flit, router.numInputUnit)
	router.SADelayBuffer = make([][]*Flit, router.numInputUnit)
	router.STDelayBuffer = make([][]*Flit, router.numInputUnit)

	// kind of Register file...?
	router.RoutingStageBuffer = make([][][]*Flit, router.numInputUnit)
	router.VCAllocStageBuffer = make([][][]*Flit, router.numInputUnit)
	router.SwitchAllocStageBuffer = make([][][]*Flit, router.numInputUnit)

	router.NewCreditCounts = make([][]int, router.numOutputUnit)
	router.NewStallFlag = make([][]bool, router.numInputUnit)
	router.OutputVCStatus = make([][]bool, router.numOutputUnit)
	router.InputVCStatus = make([][]bool, router.numInputUnit)
	router.InputVCOwner = make([][]akita.Req, router.numInputUnit)

	router.OutputPort = make([][]*Flit, UnitSize)

	for i := 0; i < UnitSize; i++ {
		router.InputPorts[i] = akita.NewLimitNumReqPort(router, 4)

		// arbit priority should be same as inputunit size ... Maybe ?
		router.UnitPriority[i] = i

		router.RoutingStageBuffer[i] = make([][]*Flit, router.numVC)
		router.VCAllocStageBuffer[i] = make([][]*Flit, router.numVC)
		router.SwitchAllocStageBuffer[i] = make([][]*Flit, router.numVC)

		//delaybuffer
		router.RCDelayBuffer[i] = make([][]*Flit, router.numVC)
		router.VADelayBuffer[i] = make([][]*Flit, router.numVC)

		router.NewCreditCounts[i] = make([]int, router.numVC)
		router.NewStallFlag[i] = make([]bool, router.numVC)
		router.OutputVCStatus[i] = make([]bool, router.numVC)
		router.InputVCStatus[i] = make([]bool, router.numVC)
		router.InputVCOwner[i] = make([]akita.Req, router.numVC)

		for j := 0; j < router.numVC; j++ {
			router.OutputVCStatus[i][j] = true
			router.InputVCStatus[i][j] = true
			router.NewCreditCounts[i][j] = 4
			router.NewStallFlag[i][j] = false
		}
	}

	router.engine = engine
	router.Freq = freq

	return router
}
