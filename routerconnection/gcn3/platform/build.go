package platform

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/gcn3/gpubuilder"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/noc"
	"gitlab.com/akita/vis/trace"
)

var UseParallelEngine bool
var DebugISA bool
var TraceVis bool
var TraceMem bool

// BuildEmuPlatform creates a simple platform for emulation purposes
func BuildEmuPlatform() (
	akita.Engine,
	*gcn3.GPU,
	*driver.Driver,
	*mem.IdealMemController,
) {
	var engine akita.Engine

	if UseParallelEngine {
		engine = akita.NewParallelEngine()
	} else {
		engine = akita.NewSerialEngine()
	}
	// engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

	mmu := vm.NewMMU("MMU", engine, &vm.DefaultPageTableFactory{})
	gpuDriver := driver.NewDriver(engine, mmu)
	connection := akita.NewDirectConnection(engine)

	gpuBuilder := gpubuilder.NewEmuGPUBuilder(engine)
	gpuBuilder.Driver = gpuDriver
	gpuBuilder.MMU = mmu
	gpuBuilder.GPUMemAddrOffset = 4 * mem.GB
	if DebugISA {
		gpuBuilder.EnableISADebug = true
	}
	if TraceMem {
		gpuBuilder.EnableMemTracing = true
	}
	gpu, globalMem := gpuBuilder.BuildEmulationGPU()
	gpuDriver.RegisterGPU(gpu, 4*mem.GB)

	connection.PlugIn(gpuDriver.ToGPUs)
	connection.PlugIn(gpu.ToDriver)
	gpu.Driver = gpuDriver.ToGPUs

	return engine, gpu, gpuDriver, globalMem
}

//BuildNR9NanoPlatform creates a platform that equips with several R9Nano GPUs
func BuildNR9NanoPlatform(
	numGPUs int,
) (
	akita.Engine,
	*driver.Driver,
) {
	var engine akita.Engine

	if UseParallelEngine {
		engine = akita.NewParallelEngine()
	} else {
		engine = akita.NewSerialEngine()
	}
	// engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

	mmu := vm.NewMMU("MMU", engine, &vm.DefaultPageTableFactory{})
	mmu.Latency = 100
	mmu.ShootdownLatency = 50
	gpuDriver := driver.NewDriver(engine, mmu)
	//connection := akita.NewDirectConnection(engine)
	connection := noc.NewFixedBandwidthConnection(32, engine, 1*akita.GHz)
	connection.SrcBufferCapacity = 40960000

	gpuBuilder := gpubuilder.R9NanoGPUBuilder{
		GPUName:           "GPU",
		Engine:            engine,
		Driver:            gpuDriver,
		EnableISADebug:    DebugISA,
		EnableMemTracing:  TraceMem,
		EnableInstTracing: TraceVis,
		EnableVisTracing:  TraceVis,
		MMU:               mmu,
		ExternalConn:      connection,
	}

	if TraceVis {
		tracer := trace.NewMongoDBTracer()
		tracer.Init()
		hook := trace.NewHook(tracer)
		gpuBuilder.SetTraceHook(hook)

		gpuDriver.AcceptHook(hook)
	}

	rdmaAddressTable := new(cache.BankedLowModuleFinder)
	rdmaAddressTable.BankSize = 4 * mem.GB
	rdmaAddressTable.LowModules = append(rdmaAddressTable.LowModules, nil)
	for i := 1; i < numGPUs+1; i++ {
		gpuBuilder.GPUName = fmt.Sprintf("GPU_%d", i)
		gpuBuilder.GPUMemAddrOffset = uint64(i) * 4 * mem.GB
		gpu := gpuBuilder.Build(uint64(i))
		gpuDriver.RegisterGPU(gpu, 4*mem.GB)
		gpu.Driver = gpuDriver.ToGPUs

		gpu.RDMAEngine.RemoteRDMAAddressTable = rdmaAddressTable
		rdmaAddressTable.LowModules = append(
			rdmaAddressTable.LowModules,
			gpu.RDMAEngine.ToOutside)
		connection.PlugIn(gpu.RDMAEngine.ToOutside)
	}

	connection.PlugIn(gpuDriver.ToGPUs)
	connection.PlugIn(mmu.ToTop)

	return engine, gpuDriver
}

func BuildNR9NanoPlatformWithRouterConnection(numGPUs int, Topology string) (
	akita.Engine,
	*driver.Driver,
) {
	var engine akita.Engine

	if UseParallelEngine {
		engine = akita.NewParallelEngine()
	} else {
		engine = akita.NewSerialEngine()
	}
	// engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

	mmu := vm.NewMMU("MMU", engine, &vm.DefaultPageTableFactory{})
	mmu.Latency = 100
	mmu.ShootdownLatency = 50
	gpuDriver := driver.NewDriver(engine, mmu)

	/// RouterConnection Configuration

	numRouterVC := 1 // Number of Virtual Channels
	RouterFreq := 1 * akita.GHz

	conn := noc.NewRouterConnection(engine, numGPUs, numRouterVC, RouterFreq)
	NocLogger := noc.NewNocStatLogger(log.New(os.Stdout, "", 0), conn)
	conn.AcceptHook(NocLogger)

	conn.FlitSize = 32
	conn.RouterBufferSize = 4

	engine.RegisterSimulationEndHandler(NocLogger)

	if Topology == "butterfly" {

		k_array := 3*numGPUs + 2
		numStage := 1
		conn.BuildButterFlyNetwork(k_array, numStage, NocLogger)
		conn.ConnectingButterFlyNetwork(k_array, numStage)
		conn.IsButterFly = true
		conn.Argv1 = k_array
		conn.Argv2 = numStage

	} else if Topology == "mesh" {

		Mesh_k_array := 6
		Mesh_Dim := 2
		conn.BuildMeshNetwork(Mesh_k_array, Mesh_Dim, NocLogger)
		conn.ConnectingMeshNetwork(Mesh_k_array, Mesh_Dim)
		conn.IsMesh = true
		conn.Argv1 = Mesh_k_array
		conn.Argv2 = Mesh_Dim

	} else if Topology == "dgx1" {
		PlaneDegree := 8
		Stack := 4
		conn.BuildScalable_DGX1_Network(NocLogger, numGPUs, PlaneDegree, Stack)
		conn.ConnectingScalable_DGX1_Network(numGPUs, PlaneDegree, Stack)
		conn.Argv1 = PlaneDegree
		conn.Argv2 = Stack
		conn.IsDGX1 = true

	} else if Topology == "dgx2" {
		numSwitchs := 6
		conn.Argv1 = numSwitchs
		conn.BuildScalable_DGX2_Network(NocLogger, numGPUs, numSwitchs)
		conn.ConnectingScalable_DGX2_Network(numGPUs, numSwitchs)
		conn.IsDGX2 = true
	} else {
		log.Panic("Wrong Topology Name")
	}

	gpuBuilder := gpubuilder.R9NanoGPUBuilder{
		GPUName:           "GPU",
		Engine:            engine,
		Driver:            gpuDriver,
		EnableISADebug:    DebugISA,
		EnableMemTracing:  TraceMem,
		EnableInstTracing: TraceVis,
		EnableVisTracing:  TraceVis,
		MMU:               mmu,
		ExternalConn:      conn,
	}

	if TraceVis {
		tracer := trace.NewMongoDBTracer()
		tracer.Init()
		hook := trace.NewHook(tracer)
		gpuBuilder.SetTraceHook(hook)

		gpuDriver.AcceptHook(hook)
	}

	rdmaAddressTable := new(cache.BankedLowModuleFinder)
	rdmaAddressTable.BankSize = 4 * mem.GB
	rdmaAddressTable.LowModules = append(rdmaAddressTable.LowModules, nil)
	for i := 1; i < numGPUs+1; i++ {
		gpuBuilder.GPUName = fmt.Sprintf("GPU_%d", i)
		gpuBuilder.GPUMemAddrOffset = uint64(i) * 4 * mem.GB
		gpu := gpuBuilder.Build(uint64(i))
		gpuDriver.RegisterGPU(gpu, 4*mem.GB)
		gpu.Driver = gpuDriver.ToGPUs

		gpu.RDMAEngine.RemoteRDMAAddressTable = rdmaAddressTable
		rdmaAddressTable.LowModules = append(
			rdmaAddressTable.LowModules,
			gpu.RDMAEngine.ToOutside)
		conn.PlugIn(gpu.RDMAEngine.ToOutside)
	}

	conn.PlugIn(gpuDriver.ToGPUs)
	conn.PlugIn(mmu.ToTop)

	return engine, gpuDriver

}
