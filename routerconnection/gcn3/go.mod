module gitlab.com/akita/gcn3

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/mock v1.2.0
	github.com/golang/protobuf v1.3.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.4.3
	github.com/rs/xid v1.2.1
	gitlab.com/akita/akita v1.3.2
	gitlab.com/akita/mem v1.1.4
	gitlab.com/akita/noc v1.1.3
	gitlab.com/akita/vis v0.2.0
	golang.org/x/net v0.0.0-20190311183353-d8887717615a // indirect
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
)
