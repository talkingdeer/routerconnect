package cache

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

// A WriteBackCache is a cache that performs the write-through policy
type WriteBackCache struct {
	*akita.ComponentBase
	ticker *akita.Ticker

	ToTop    akita.Port
	ToBottom akita.Port

	engine          akita.Engine
	Directory       Directory
	mshr            MSHR
	lowModuleFinder LowModuleFinder
	Storage         *mem.Storage

	Freq                akita.Freq
	DirectoryLatency    int
	Latency             int
	NumBank             int
	BlockSizeAsPowerOf2 uint64

	// Parse Stage
	processingReqs      map[uint64][]mem.AccessReq
	processingWriteReqs []*mem.WriteReq

	// Directory Stage
	directoryLookupReq       akita.Req
	directoryLookupCycleLeft int

	// Bank Stage
	bankBusy []bankBusyInfo

	// Fulfill Stage
	fulfillStageBusy bool
	fulfillingAddr   uint64
	fulfillingData   []byte

	// Send Stage
	// No need for buffer limitation as it cannot exceed MSHR entries
	readBuffer            []*mem.ReadReq
	pendingReadReqs       map[string]*mem.ReadReq
	writeBuffer           []*mem.WriteReq
	pendingWriteReqs      map[string]*mem.WriteReq
	writeBufferCapacity   int
	upGoingBuffer         []akita.Req
	upGoingBufferCapacity int

	evictingList map[uint64]bool

	needTick bool
}

func (c *WriteBackCache) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
	c.ticker.TickLater(now)
}

func (c *WriteBackCache) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	c.ticker.TickLater(now)
}

func (c *WriteBackCache) Handle(e akita.Event) error {
	c.Lock()

	switch evt := e.(type) {
	case akita.TickEvent:
		c.handleTickEvent(evt)
	case *SetBankCycleLeftToZeroEvent:
		c.bankBusy[evt.bankNum].cycleLeft = 0
		c.ticker.TickLater(evt.Time())
	}

	c.Unlock()
	return nil
}

func (c *WriteBackCache) handleTickEvent(evt akita.TickEvent) error {
	now := evt.Time()
	c.needTick = false

	c.sendReqs(now)
	c.runFulfillStage(now)
	for i := 0; i < c.NumBank; i++ {
		c.runBank(i, now)
	}
	c.runDirectoryStage(now)
	c.runParseStage(now)

	if c.needTick {
		c.ticker.TickLater(now)
	}

	return nil
}

func (c *WriteBackCache) runParseStage(now akita.VTimeInSec) {
	if c.parseReqFromBottom(now) {
		return
	}
	c.parseReqFromTop(now)
}

func (c *WriteBackCache) parseReqFromBottom(now akita.VTimeInSec) bool {
	req := c.ToBottom.Peek()
	if req == nil {
		return false
	}

	c.needTick = true
	switch req := req.(type) {
	case *mem.DataReadyRsp:
		read := c.pendingReadReqs[req.RespondTo]
		c.traceMem(now, "recv-data-ready",
			read.Address, read.MemByteSize, req.Data)
		c.parseDataReady(now, req)
	case *mem.DoneRsp:
		c.processingDoneRspAfterParsing(now, req)
	default:
		log.Panicf("cannot process req of type %s", reflect.TypeOf(req))
	}
	return true
}

func (c *WriteBackCache) parseDataReady(
	now akita.VTimeInSec,
	dataReady *mem.DataReadyRsp,
) {
	readBottom := c.pendingReadReqs[dataReady.RespondTo]
	addr := readBottom.Address
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	mshrEntry := c.mshr.Query(cacheLineID)
	block := mshrEntry.Block

	bankNum := bankID(block, c.Directory.WayAssociativity(), c.NumBank)
	if c.bankBusy[bankNum].req != nil {
		c.traceMem(now, "data-ready-stall-bank-busy",
			addr, uint64(len(dataReady.Data)), dataReady.Data)
		return
	}

	delete(c.evictingList, block.Tag)

	block.Tag = readBottom.Address
	block.IsValid = true
	c.Directory.Lookup(block.Tag) // Put the block in the most recent used position

	c.startBank(now, bankNum, block, nil, nil, dataReady)

	delete(c.pendingReadReqs, readBottom.ID)
	c.ToBottom.Retrieve(now)
	c.needTick = true
}

func (c *WriteBackCache) processingDoneRspAfterParsing(
	now akita.VTimeInSec,
	rsp *mem.DoneRsp,
) {
	writeBottom := c.pendingWriteReqs[rsp.RespondTo]
	address := writeBottom.Address
	cacheLineID, _ := GetCacheLineID(address, c.BlockSizeAsPowerOf2)

	c.traceMem(now, "parse-done", address, c.blockSizeInByte(), writeBottom.Data)

	mshrEntry := c.mshr.QueryByBlockTag(cacheLineID)
	if mshrEntry == nil {
		log.Panicf("address 0x%X is not being evicted", cacheLineID)
	}

	isWritingWholeBlock := false
	for _, req := range mshrEntry.Requests {
		writeReq, isWrite := req.(*mem.WriteReq)
		if isWrite && uint64(len(writeReq.Data)) == c.blockSizeInByte() {

			block := mshrEntry.Block
			bankNum := bankID(block, c.Directory.WayAssociativity(), c.NumBank)
			if c.bankBusy[bankNum].req != nil {
				return
			}

			c.writeWholeBlockLocal(now, mshrEntry, writeReq)
			isWritingWholeBlock = true
		}
	}

	if !isWritingWholeBlock {
		newCacheLineID, _ := GetCacheLineID(mshrEntry.Requests[0].GetAddress(),
			c.BlockSizeAsPowerOf2)
		mshrEntry.Block.Tag = newCacheLineID
		c.readFromBottom(now, mshrEntry.Block, mshrEntry.Requests[0])
	}

	delete(c.evictingList, writeBottom.Address)
	delete(c.pendingWriteReqs, writeBottom.ID)
	c.ToBottom.Retrieve(now)
	c.needTick = true
}

func (c *WriteBackCache) parseReqFromTop(now akita.VTimeInSec) bool {
	req := c.ToTop.Peek()
	if req == nil {
		return false
	}
	c.needTick = true

	switch req := req.(type) {
	case *mem.ReadReq:
		c.parseReadReq(now, req)
	case *mem.WriteReq:
		c.parseWriteReq(now, req)
	default:
		log.Panicf("cannot process req of type %s", reflect.TypeOf(req))
	}
	return true
}

func (c *WriteBackCache) parseReadReq(now akita.VTimeInSec, req *mem.ReadReq) {
	addr := req.Address
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	_, isReadingSameAddress := c.processingReqs[cacheLineID]

	c.traceMem(now, "parse-read-req-"+req.ID, req.Address, req.MemByteSize, nil)

	if isReadingSameAddress {
		c.traceMem(now, "parse-read-req-add-to-reading-list", req.Address, req.MemByteSize, nil)
		c.addToReadList(req)
		c.ToTop.Retrieve(now)
		return
	}

	if c.isWritingLine(addr) {
		c.traceMem(now, "parse-read-req-stall-pending-write", req.Address, req.MemByteSize, nil)
		return
	}

	if c.isEvictingLine(addr) {
		c.traceMem(now, "parse-read-req-stall-evicting-same-line", req.Address, req.GetByteSize(), nil)
		return
	}

	if c.directoryLookupReq != nil {
		c.traceMem(now, "parse-read-req-stall-directory-busy", req.Address, req.MemByteSize, nil)
		return
	}

	c.traceMem(now, "parse-read-req-send-to-directory", req.Address, req.MemByteSize, nil)
	c.sendReqToDirectoryStage(req)
	c.addToReadList(req)
	c.ToTop.Retrieve(now)
}

func (c *WriteBackCache) isWritingLine(addr uint64) bool {
	lineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	for _, write := range c.processingWriteReqs {
		writeLineID, _ := GetCacheLineID(write.Address, c.BlockSizeAsPowerOf2)
		if lineID == writeLineID {
			return true
		}
	}
	return false
}

func (c *WriteBackCache) isEvictingLine(addr uint64) bool {
	lineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	if c.evictingList[lineID] == true {
		return true
	}
	return false
}

func (c *WriteBackCache) parseWriteReq(now akita.VTimeInSec, req *mem.WriteReq) {
	addr := req.Address
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	_, isReadingSameAddress := c.processingReqs[cacheLineID]

	c.traceMem(now, "parse-write-req-"+req.ID, req.Address, req.GetByteSize(), req.Data)

	if isReadingSameAddress {
		c.traceMem(now, "parse-write-req-stall-processing-same-line", req.Address, req.GetByteSize(), req.Data)
		return
	}

	if c.isEvictingLine(addr) {
		c.traceMem(now, "parse-read-write-stall-evicting-line",
			req.Address, req.GetByteSize(), nil)
		return
	}

	if c.directoryLookupReq != nil {
		c.traceMem(now, "parse-write-req-stall-directory-busy", req.Address, req.GetByteSize(), req.Data)
		return
	}

	c.traceMem(now, "parse-write-req-send-to-directory", req.Address, req.GetByteSize(), req.Data)
	c.processingWriteReqs = append(c.processingWriteReqs, req)
	c.processingReqs[cacheLineID] = append(c.processingReqs[cacheLineID], req)
	c.sendReqToDirectoryStage(req)
	c.ToTop.Retrieve(now)
}

func (c *WriteBackCache) addToReadList(req *mem.ReadReq) {
	addr := req.Address
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	reqs, addressExist := c.processingReqs[cacheLineID]

	if !addressExist {
		reqs = make([]mem.AccessReq, 0)
	}

	reqs = append(reqs, req)
	c.processingReqs[cacheLineID] = reqs
}

func (c *WriteBackCache) sendReqToDirectoryStage(req akita.Req) {
	c.directoryLookupReq = req
	c.directoryLookupCycleLeft = c.DirectoryLatency
	c.needTick = true
}

//func (c *WriteBackCache) getProcessingWriteReq(address uint64) *mem.WriteReq {
//	for _, req := range c.processingWriteReqs {
//		if req.Address == address {
//			return req
//		}
//	}
//	log.Panic("Cannot find incoming write request")
//	return nil
//}

func (c *WriteBackCache) removeProcessingWriteReq(write *mem.WriteReq) {
	var i int
	var req *mem.WriteReq
	for i, req = range c.processingWriteReqs {
		if req == write {
			c.processingWriteReqs = append(c.processingWriteReqs[:i],
				c.processingWriteReqs[i+1:]...)
			return
		}
	}

	log.Panic("Cannot find incoming write request")
}

func (c *WriteBackCache) runDirectoryStage(now akita.VTimeInSec) {
	if c.directoryLookupReq == nil {
		return
	}

	if c.directoryLookupCycleLeft <= 0 {
		switch req := c.directoryLookupReq.(type) {
		case *mem.ReadReq:
			c.processReadReqAfterDirectoryLookup(now, req)
		case *mem.WriteReq:
			c.processWriteReqAfterDirectoryLookup(now, req)
		default:
			log.Panicf("cannot process req %s", reflect.TypeOf(req))
		}
	} else {
		c.directoryLookupCycleLeft--
		c.needTick = true
	}
}

func (c *WriteBackCache) processReadReqAfterDirectoryLookup(now akita.VTimeInSec, req *mem.ReadReq) {
	addr := req.Address
	cacheLindID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	block := c.Directory.Lookup(cacheLindID)

	if block != nil {
		c.doReadHit(now, block, req)
	} else {
		c.doReadMiss(now, req)
	}
}

func (c *WriteBackCache) doReadHit(now akita.VTimeInSec, block *Block, req *mem.ReadReq) {
	addr := req.Address
	byteSize := req.MemByteSize
	if block.IsLocked {
		c.traceMem(now, "dir-r-hit-stall-block-locked", addr, byteSize, nil)
		return
	}

	bankNum := bankID(block, c.Directory.WayAssociativity(), c.NumBank)
	if c.bankBusy[bankNum].req == nil {
		c.traceMem(now, "r-hit", addr, byteSize, nil)
		block.IsLocked = true
		c.traceMem(now, "dir-r-hit-block-lock-read-hit", block.Tag, c.blockSizeInByte(), nil)
		c.startBank(now, int(bankNum), block, nil, nil, req)
		c.directoryLookupReq = nil
		c.needTick = true
	} else {
		c.traceMem(now, "dir-r-hit-stall-bank-conflict", addr, byteSize, nil)
	}
}

func (c *WriteBackCache) doReadMiss(
	now akita.VTimeInSec,
	req *mem.ReadReq,
) {
	addr := req.GetAddress()
	byteSize := req.GetByteSize()
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	block := c.Directory.Evict(cacheLineID)
	bankNum := bankID(block, c.Directory.WayAssociativity(), c.NumBank)
	mshrEntry := c.mshr.Query(cacheLineID)

	// Do not evict or overwrite locked block
	if block.IsLocked {
		c.traceMem(now, "dir-r-miss-stall-block-locked", addr, byteSize, nil)
		return
	}

	// MSHR is required no matter if eviction is required
	if c.mshr.IsFull() && mshrEntry == nil {
		c.traceMem(now, "dir-r-miss-stall-mshr-full", addr, byteSize, nil)
		return
	}

	// If eviction is necessary, we need to check if bank is available
	if block.IsValid && block.IsDirty && c.bankBusy[bankNum].req != nil {
		c.traceMem(now, "dir-r-miss-stall-bank-busy", addr, byteSize, nil)
		return
	}

	// Block valid, but clean
	if block.IsValid {
		c.traceMem(now, "dir-r-miss-evict-without-flushing", block.Tag, byteSize, nil)
	}

	// Read local for eviction
	c.insertIntoMSHR(now, block, req)
	if block.IsValid && block.IsDirty {
		c.readForEviction(now, block, req)
	} else {
		if mshrEntry == nil {
			c.readFromBottom(now, block, req)
		}
	}

	c.directoryLookupReq = nil
	c.needTick = true
}

func (c *WriteBackCache) insertIntoMSHR(
	now akita.VTimeInSec,
	block *Block,
	req mem.AccessReq,
) *MSHREntry {
	addr := req.GetAddress()
	byteSize := req.GetByteSize()
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	mshrEntry := c.mshr.Query(cacheLineID)
	if mshrEntry != nil {
		mshrEntry.Requests = append(mshrEntry.Requests, req)
		c.traceMem(now, "r-miss", addr, req.GetByteSize(), nil)
		c.traceMem(now, "insert-mshr-"+req.GetID(), addr, byteSize, nil)
		return mshrEntry
	}

	mshrEntry = c.mshr.Add(cacheLineID)
	mshrEntry.Requests = append(mshrEntry.Requests, req)
	mshrEntry.Block = block
	c.traceMem(now, "new-mshr-entry", cacheLineID, c.blockSizeInByte(), nil)
	c.traceMem(now, "insert-mshr-"+req.GetID(), addr, byteSize, nil)
	c.traceMem(now, "mshr-block", block.Tag, c.blockSizeInByte(), nil)

	return mshrEntry
}

func (c *WriteBackCache) readForEviction(now akita.VTimeInSec, block *Block, req mem.AccessReq) {
	bankNum := bankID(block, c.Directory.WayAssociativity(), c.NumBank)

	block.IsValid = false
	block.IsLocked = true
	c.traceMem(now, "block-lock-read-for-eviction", block.Tag, c.blockSizeInByte(), nil)

	c.startBank(now, int(bankNum), nil, block, nil, req)
	c.directoryLookupReq = nil

	c.evictingList[block.Tag] = true

	c.traceMem(now, "read-for-eviction", block.Tag, c.blockSizeInByte(), nil)
}

func (c *WriteBackCache) readFromBottom(now akita.VTimeInSec, block *Block, req mem.AccessReq) {
	addr := req.GetAddress()
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)

	// Create new MSHR entry
	block.IsLocked = true
	c.traceMem(now, "block-lock-read-from-bottom", block.Tag, c.blockSizeInByte(), nil)

	lowModule := c.lowModuleFinder.Find(cacheLineID)
	readFromBottom := mem.NewReadReq(now, c.ToBottom, lowModule, cacheLineID, c.blockSizeInByte())
	c.readBuffer = append(c.readBuffer, readFromBottom)
	c.pendingReadReqs[readFromBottom.ID] = readFromBottom
	c.traceMem(now, "read-from-bottom", addr, c.blockSizeInByte(), nil)
}

func (c *WriteBackCache) processWriteReqAfterDirectoryLookup(now akita.VTimeInSec, req *mem.WriteReq) {
	addr := req.Address
	byteSize := uint64(len(req.Data))
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	block := c.Directory.Lookup(cacheLineID)

	if block != nil {
		bankNum := bankID(block, c.Directory.WayAssociativity(), c.NumBank)
		if c.bankBusy[bankNum].req != nil {
			c.traceMem(now, "dir-w-hit-stall-bank-busy", addr, uint64(len(req.Data)), req.Data)
			return
		}

		if block.IsLocked {
			c.traceMem(now, "dir-w-hit-stall-block-locked", addr, uint64(len(req.Data)), req.Data)
			return
		}

		c.traceMem(now, "w-hit", addr, uint64(len(req.Data)), req.Data)
		c.startBank(now, int(bankNum), block, nil, nil, req)
		block.IsLocked = true
		block.IsDirty = true
		c.traceMem(now, "block-lock-write", block.Tag, c.blockSizeInByte(), nil)

		c.directoryLookupReq = nil
		c.needTick = true
		return
	}

	mshrEntry := c.mshr.Query(cacheLineID)
	if mshrEntry == nil && c.mshr.IsFull() {
		c.traceMem(now, "dir-w-miss-stall-mshr-full", addr, byteSize, req.Data)
		return
	}

	evict := c.Directory.Evict(cacheLineID)
	if evict.IsLocked {
		c.traceMem(now, "dir-w-miss-stall-block-locked", addr, byteSize, req.Data)
		return
	}

	if mshrEntry != nil {
		c.insertIntoMSHR(now, evict, req)
	} else {
		if evict.IsDirty && evict.IsValid {
			bankNum := bankID(evict, c.Directory.WayAssociativity(), c.NumBank)
			if c.bankBusy[bankNum].req != nil {
				c.traceMem(now, "dir-w-miss-stall-bank-busy", addr, uint64(len(req.Data)), req.Data)
				return
			}
			c.insertIntoMSHR(now, evict, req)
			c.readForEviction(now, evict, req)
		} else if uint64(byteSize) == c.blockSizeInByte() {
			bankNum := bankID(evict, c.Directory.WayAssociativity(), c.NumBank)
			if c.bankBusy[bankNum].req != nil {
				c.traceMem(now, "dir-w-miss-stall-bank-busy", addr, uint64(len(req.Data)), req.Data)
				return
			}
			insertedEntry := c.insertIntoMSHR(now, evict, req)
			c.writeWholeBlockLocal(now, insertedEntry, req)
		} else {
			c.insertIntoMSHR(now, evict, req)
			c.readFromBottom(now, evict, req)
		}
	}
	c.traceMem(now, "w-miss", addr, uint64(len(req.Data)), req.Data)
	c.directoryLookupReq = nil
	c.needTick = true
}

func (c *WriteBackCache) writeWholeBlockLocal(
	now akita.VTimeInSec,
	entry *MSHREntry,
	req *mem.WriteReq,
) {
	block := entry.Block
	addr := req.GetAddress()
	bankNum := bankID(block, c.Directory.WayAssociativity(), c.NumBank)

	block.IsValid = true
	block.IsLocked = true
	block.Tag = addr
	block.IsDirty = true
	c.traceMem(now, "block-lock-write-whole-cache-line", block.Tag, c.blockSizeInByte(), nil)

	c.startBank(now, int(bankNum), block, nil, entry, req)

	c.traceMem(now, "write-whole-cache-line", block.Tag, c.blockSizeInByte(), nil)
}

func (c *WriteBackCache) runBank(bankID int, now akita.VTimeInSec) {
	if c.bankBusy[bankID].req == nil {
		return
	}

	if c.bankBusy[bankID].cycleLeft == 0 {
		switch req := c.bankBusy[bankID].req.(type) {
		case *mem.ReadReq:
			if c.bankBusy[bankID].evict != nil {
				c.completeReadForEvict(bankID, now, req)
			} else {
				c.completeLocalRead(bankID, now, req)
			}
		case *mem.DataReadyRsp:
			c.completeWriteFromBottom(bankID, now, req)
		case *mem.WriteReq:
			if c.bankBusy[bankID].evict != nil {
				c.completeReadForEvict(bankID, now, req)
			} else {
				c.completeLocalWrite(bankID, now, req)
			}
		default:
			log.Panicf("cannot process request %s", reflect.TypeOf(req))
		}
	}
}

func (c *WriteBackCache) completeReadForEvict(
	bankID int,
	now akita.VTimeInSec,
	req mem.AccessReq,
) {
	block := c.bankBusy[bankID].evict
	data, _ := c.Storage.Read(block.CacheAddress, c.blockSizeInByte())
	lowModule := c.lowModuleFinder.Find(block.Tag)
	writeToBottom := mem.NewWriteReq(now, c.ToBottom, lowModule, block.Tag)
	writeToBottom.Data = data

	if len(c.writeBuffer) >= c.writeBufferCapacity {
		c.traceMem(now, "read-for-evict-stall-write-buffer-full",
			block.Tag, c.blockSizeInByte(), data)
		return
	}
	c.writeBuffer = append(c.writeBuffer, writeToBottom)
	c.pendingWriteReqs[writeToBottom.ID] = writeToBottom

	c.bankBusy[bankID].evict = nil
	c.bankBusy[bankID].req = nil
	c.needTick = true

	c.traceMem(now, "read-for-evict-complete",
		block.Tag, c.blockSizeInByte(), data)
}

func (c *WriteBackCache) completeLocalRead(
	bankID int,
	now akita.VTimeInSec,
	req *mem.ReadReq,
) {
	block := c.bankBusy[bankID].block
	_, offset := GetCacheLineID(req.Address, c.BlockSizeAsPowerOf2)

	if c.fulfillStageBusy {
		c.traceMem(now, "bank-read-local-stall-fulfill-stage-busy",
			block.Tag+offset, c.blockSizeInByte(), nil)

		return
	}

	data, _ := c.Storage.Read(block.CacheAddress, c.blockSizeInByte())
	c.fulfillStageBusy = true
	c.fulfillingAddr = block.Tag
	c.fulfillingData = data

	c.bankBusy[bankID].req = nil
	block.IsLocked = false
	c.traceMem(now, "block-unlock", block.Tag, c.blockSizeInByte(), nil)
	c.needTick = true

	c.traceMem(now, "bank- read-local-complete",
		block.Tag, c.blockSizeInByte(), data)
}

func (c *WriteBackCache) completeWriteFromBottom(
	bankID int,
	now akita.VTimeInSec,
	dataReady *mem.DataReadyRsp,
) {
	block := c.bankBusy[bankID].block
	if c.fulfillStageBusy {
		c.traceMem(now, "bank-write-from-bottom-stall-fulfill-stage-busy",
			block.Tag, c.blockSizeInByte(), nil)
		return
	}

	c.mshr.Remove(block.Tag)
	c.traceMem(now, "remove-from-mshr", block.Tag, c.blockSizeInByte(), nil)

	c.combineProcessingWriteWithDataReady(now, dataReady, block)

	c.Storage.Write(block.CacheAddress, dataReady.Data)
	block.IsValid = true
	block.IsLocked = false
	c.traceMem(now, "block-unlock", block.Tag, c.blockSizeInByte(), nil)
	c.bankBusy[bankID].req = nil

	c.fulfillStageBusy = true
	c.fulfillingAddr = block.Tag
	c.fulfillingData = dataReady.Data

	c.traceMem(now, "write-from-bottom-complete",
		block.Tag, c.blockSizeInByte(), dataReady.Data)
	c.needTick = true
}

func (c *WriteBackCache) combineProcessingWriteWithDataReady(
	now akita.VTimeInSec, dataReady *mem.DataReadyRsp, block *Block,
) {
	for _, req := range c.processingWriteReqs {
		address := req.Address
		cacheLineID, offset := GetCacheLineID(address, c.BlockSizeAsPowerOf2)
		if block.Tag == cacheLineID {
			copy(dataReady.Data[offset:], req.Data)
			c.traceMem(now, "use write-buffer-data",
				block.Tag+offset, uint64(len(req.Data)), req.Data)
			block.IsDirty = true
		}
	}
}

func (c *WriteBackCache) completeLocalWrite(
	bankID int,
	now akita.VTimeInSec,
	write *mem.WriteReq,
) {
	if c.fulfillStageBusy {
		c.traceMem(now, "local_write_stall_fulfill_stage_busy",
			write.Address, write.GetByteSize(), write.Data)
		return
	}

	c.needTick = true
	block := c.bankBusy[bankID].block
	_, offset := GetCacheLineID(write.Address, c.BlockSizeAsPowerOf2)
	c.Storage.Write(block.CacheAddress+offset, write.Data)
	c.bankBusy[bankID].req = nil
	c.traceMem(now, "write-local-done", write.Address, uint64(len(write.Data)), write.Data)

	cacheLineData, _ := c.Storage.Read(block.CacheAddress, c.blockSizeInByte())
	c.fulfillStageBusy = true
	c.fulfillingAddr = block.Tag
	c.fulfillingData = cacheLineData
	block.IsLocked = false
	c.traceMem(now, "block-unlock", block.Tag, c.blockSizeInByte(), nil)

	if c.bankBusy[bankID].mshrEntry != nil {
		c.traceMem(now, "remove-mshr-entry", block.Tag, c.blockSizeInByte(), nil)
		c.mshr.Remove(block.Tag)
		c.bankBusy[bankID].mshrEntry = nil
	}
}

func (c *WriteBackCache) runFulfillStage(now akita.VTimeInSec) {
	if !c.fulfillStageBusy {
		return
	}

	for len(c.processingReqs[c.fulfillingAddr]) > 0 {
		if len(c.upGoingBuffer) >= c.upGoingBufferCapacity {
			c.traceMem(now, "fulfill-stall-up-going-buffer-full",
				c.fulfillingAddr, c.blockSizeInByte(), c.fulfillingData)
			break
		}

		switch req := c.processingReqs[c.fulfillingAddr][0].(type) {
		case *mem.ReadReq:
			byteSize := req.MemByteSize
			addr := req.Address
			_, offset := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
			dataReady := mem.NewDataReadyRsp(now, c.ToTop, req.Src(), req.ID)
			dataReady.Data = c.fulfillingData[offset : offset+byteSize]
			c.upGoingBuffer = append(c.upGoingBuffer, dataReady)
			c.processingReqs[c.fulfillingAddr] =
				c.processingReqs[c.fulfillingAddr][1:]
			c.traceMem(now, "fulfill-read-send-up", addr, byteSize, dataReady.Data)
			c.needTick = true
		case *mem.WriteReq:
			doneRsp := mem.NewDoneRsp(now, c.ToTop, req.Src(), req.ID)
			c.upGoingBuffer = append(c.upGoingBuffer, doneRsp)
			c.processingReqs[c.fulfillingAddr] =
				c.processingReqs[c.fulfillingAddr][1:]
			c.removeProcessingWriteReq(req)
			c.traceMem(now, "fulfill-write-send-up", req.Address, req.GetByteSize(), nil)
			c.needTick = true
		}
	}

	if len(c.processingReqs[c.fulfillingAddr]) == 0 {
		c.fulfillStageBusy = false
		delete(c.processingReqs, c.fulfillingAddr)
		c.traceMem(now, "fulfill-remove-processing-req-addr",
			c.fulfillingAddr, c.blockSizeInByte(), nil)
		c.needTick = true
		return
	}

}

func (c *WriteBackCache) sendReqs(now akita.VTimeInSec) {
	for len(c.upGoingBuffer) > 0 {
		req := c.upGoingBuffer[0]
		req.SetSendTime(now)
		err := c.ToTop.Send(req)
		if err == nil {
			switch req := req.(type) {
			case *mem.DataReadyRsp:
				c.traceMem(now, "rsp-data-ready-"+req.RespondTo, 0, 0, req.Data)
			case *mem.DoneRsp:
				c.traceMem(now, "rsp-done-"+req.RespondTo, 0, 0, nil)
			}
			c.upGoingBuffer = c.upGoingBuffer[1:]
			c.needTick = true
		} else {
			break
		}
	}

	for len(c.writeBuffer) > 0 {
		req := c.writeBuffer[0]
		req.SetSendTime(now)
		err := c.ToBottom.Send(req)
		if err == nil {
			c.writeBuffer = c.writeBuffer[1:]
			c.needTick = true
		} else {
			break
		}
	}

	for len(c.readBuffer) > 0 {
		req := c.readBuffer[0]
		req.SetSendTime(now)
		err := c.ToBottom.Send(req)
		if err == nil {
			c.readBuffer = c.readBuffer[1:]
			c.needTick = true
		} else {
			break
		}
	}
}

func (c *WriteBackCache) startBank(
	now akita.VTimeInSec,
	bankNum int,
	block *Block,
	evict *Block,
	mshrEntry *MSHREntry,
	req akita.Req,
) {
	bankInfo := &c.bankBusy[bankNum]

	if bankInfo.req != nil {
		log.Panic("bank not available")
	}

	bankInfo.cycleLeft = c.Latency
	bankInfo.block = block
	bankInfo.evict = evict
	bankInfo.mshrEntry = mshrEntry
	bankInfo.req = req

	evt := NewSetBankCycleLeftToZeroEvent(
		c.Freq.NCyclesLater(c.Latency, now), c, bankNum)
	c.engine.Schedule(evt)
}

func (c *WriteBackCache) blockSizeInByte() uint64 {
	return 1 << c.BlockSizeAsPowerOf2
}

func (c *WriteBackCache) traceMem(
	now akita.VTimeInSec,
	what string,
	addr uint64, byteSize uint64,
	data []byte,
) {
	traceInfo := mem.TraceInfo{
		Where:    c.Name(),
		When:     now,
		What:     what,
		Address:  addr,
		ByteSize: byteSize,
		Data:     data,
	}

	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Item:   traceInfo,
	}

	c.InvokeHook(&ctx)
}

func (c *WriteBackCache) SetNumBanks(n int) {
	c.NumBank = n
	c.bankBusy = make([]bankBusyInfo, n)
}

// NewWriteBackCache creates a new write-through cache,
// injecting the dependency of the engine, the directory and the storage.
func NewWriteBackCache(
	name string,
	engine akita.Engine,
	directory Directory,
	mshr MSHR,
	lowModuleFinder LowModuleFinder,
	storage *mem.Storage,
) *WriteBackCache {
	cache := new(WriteBackCache)
	cache.ComponentBase = akita.NewComponentBase(name)
	cache.ticker = akita.NewTicker(cache, engine, 1*akita.GHz)

	cache.ToTop = akita.NewLimitNumReqPort(cache, 1)
	cache.ToBottom = akita.NewLimitNumReqPort(cache, 1)

	cache.engine = engine
	cache.Directory = directory
	cache.mshr = mshr
	cache.lowModuleFinder = lowModuleFinder
	cache.Storage = storage

	cache.DirectoryLatency = 2
	cache.Latency = 10
	cache.Freq = 1 * akita.GHz
	cache.NumBank = 4

	cache.processingWriteReqs = make([]*mem.WriteReq, 0)
	cache.processingReqs = make(map[uint64][]mem.AccessReq)

	cache.bankBusy = make([]bankBusyInfo, cache.NumBank)
	cache.BlockSizeAsPowerOf2 = 6

	cache.readBuffer = make([]*mem.ReadReq, 0)
	cache.pendingReadReqs = make(map[string]*mem.ReadReq)
	cache.writeBuffer = make([]*mem.WriteReq, 0)
	cache.pendingWriteReqs = make(map[string]*mem.WriteReq)
	cache.writeBufferCapacity = 409600
	cache.upGoingBuffer = make([]akita.Req, 0)
	cache.upGoingBufferCapacity = 409600
	cache.evictingList = make(map[uint64]bool)

	return cache
}
