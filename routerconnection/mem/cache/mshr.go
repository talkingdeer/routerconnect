package cache

import (
	"log"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

// MSHREntry is an entry in MSHR
type MSHREntry struct {
	Requests []mem.AccessReq
	Data     []byte
	Block    *Block
}

// NewMSHREntry returns a new MSHR entry object
func NewMSHREntry() *MSHREntry {
	e := new(MSHREntry)
	e.Requests = make([]mem.AccessReq, 0)
	return e
}

// MSHR is an interface that controls MSHR
type MSHR interface {
	Query(addr uint64) *MSHREntry
	QueryByBlockTag(addr uint64) *MSHREntry
	Add(addr uint64) *MSHREntry
	Remove(addr uint64) *MSHREntry
	IsFull() bool
}

// MSHRImpl is a default implementation of MSHR
type MSHRImpl struct {
	*akita.ComponentBase

	CapacityInNumEntries int
	entries              map[uint64]*MSHREntry
}

// Add creats a new entry in the mshr. The Address passed in needs to be the
// block ID.
func (m *MSHRImpl) Add(addr uint64) *MSHREntry {
	entry, found := m.entries[addr]
	if found {
		log.Panic("address already in MSHR")
	}

	if len(m.entries) >= m.CapacityInNumEntries {
		log.Panic("MSHR is full")
	}

	entry = NewMSHREntry()
	m.entries[addr] = entry
	return entry
}

// Query returns the entry mapped by the input address. If the address is not
// in MSHR, return nil
func (m *MSHRImpl) Query(addr uint64) *MSHREntry {
	entry, _ := m.entries[addr]
	return entry
}

func (m *MSHRImpl) QueryByBlockTag(addr uint64) *MSHREntry {
	for _, entry := range m.entries {
		if entry.Block == nil {
			continue
		}

		if entry.Block.Tag == addr {
			return entry
		}
	}
	return nil
}

// Remove deletes the entry in the MSHR and returns the deleted entry.
func (m *MSHRImpl) Remove(addr uint64) *MSHREntry {
	entry, found := m.entries[addr]
	if !found {
		log.Panic("no such entry in the MSHR")
	}

	delete(m.entries, addr)

	return entry
}

// IsFull returns true if no more MSHR entries can be added
func (m *MSHRImpl) IsFull() bool {
	if len(m.entries) >= m.CapacityInNumEntries {
		return true
	}
	return false
}

// NewMSHR returns a new MSHRImpl object
func NewMSHR(capacity int) *MSHRImpl {
	m := new(MSHRImpl)
	m.entries = make(map[uint64]*MSHREntry)
	m.CapacityInNumEntries = capacity
	return m
}
