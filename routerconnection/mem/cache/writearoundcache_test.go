package cache

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/akita/mock_akita"
	"gitlab.com/akita/mem"
)

var _ = Describe("Write-Around Cache", func() {

	var (
		mockCtrl *gomock.Controller

		engine          *mock_akita.MockEngine
		cache           *WriteAroundCache
		directory       *MockDirectory
		storage         *mem.Storage
		mshr            *MSHRImpl
		lowModuleFinder *SingleLowModuleFinder

		topModule akita.Port

		toTop    *mock_akita.MockPort
		toBottom *mock_akita.MockPort
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())

		engine = mock_akita.NewMockEngine(mockCtrl)
		mshr = NewMSHR(128)
		directory = new(MockDirectory)
		lowModuleFinder = new(SingleLowModuleFinder)
		storage = mem.NewStorage(4 * mem.GB)

		cache = NewWriteAroundCache("Cache", engine, directory, mshr, lowModuleFinder, storage)
		toTop = mock_akita.NewMockPort(mockCtrl)
		toBottom = mock_akita.NewMockPort(mockCtrl)
		cache.ToTop = toTop
		cache.ToBottom = toBottom
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Context("parse stage", func() {

		Context("read", func() {
			var (
				read *mem.ReadReq
			)

			BeforeEach(func() {
				read = mem.NewReadReq(6, topModule, cache.ToTop, 0x104, 4)
				cache.needTick = false
			})

			It("should stall if writing the same line", func() {
				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(read)

				write := mem.NewWriteReq(4, topModule, cache.ToTop, 0x100)
				cache.processingWriteReqs = append(cache.processingWriteReqs, write)

				cache.runParseStage(11)

				Expect(cache.needTick).To(BeFalse())
			})

			It("should add to processing list, if the address is being processed", func() {
				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(read)
				toTop.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(read)

				cache.processingReqs[0x100] = make([]mem.AccessReq, 1)

				cache.runParseStage(11)

				Expect(cache.processingReqs[0x100]).To(ContainElement(read))
				Expect(cache.needTick).To(BeTrue())
			})

			It("should stall if directory is busy", func() {
				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(read)

				cache.directoryLookupReq = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupCycleLeft = 2

				cache.runParseStage(11)

				Expect(cache.needTick).To(BeFalse())
			})

			It("should send to directory lookup stage", func() {
				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(read)
				toTop.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(read)

				cache.runParseStage(11)

				Expect(cache.processingReqs[0x100]).To(ContainElement(read))
				Expect(cache.directoryLookupReq).To(BeIdenticalTo(read))
				Expect(cache.directoryLookupCycleLeft).To(Equal(2))
				Expect(cache.needTick).To(BeTrue())
			})
		})

		Context("write", func() {
			var (
				data  []byte
				write *mem.WriteReq
			)

			BeforeEach(func() {
				data = []byte{1, 2, 3, 4}
				write = mem.NewWriteReq(6, topModule, cache.ToTop, 0x104)
				write.Data = data
				cache.needTick = false
			})

			It("should stall if line is busy", func() {
				read := mem.NewReadReq(4, topModule, cache.ToTop, 0x108, 4)
				cache.processingReqs[0x100] = append(cache.processingReqs[0x100], read)

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(write)

				cache.runParseStage(11)

				// Expect(cache.ToTop.Buf).To(HaveLen(1))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(cache.needTick).To(BeFalse())
			})

			It("should stall if directory is busy", func() {
				cache.directoryLookupReq = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupCycleLeft = 2

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(write)

				cache.runParseStage(11)

				Expect(cache.needTick).To(BeFalse())
			})

			It("should move request to directory stage", func() {
				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(write)
				toTop.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(write)

				cache.runParseStage(11)

				// Expect(cache.ToTop.Buf).To(HaveLen(0))
				Expect(cache.processingReqs[0x100]).To(ContainElement(write))
				Expect(cache.processingWriteReqs).To(ContainElement(write))
				Expect(cache.directoryLookupReq).To(BeIdenticalTo(write))
				Expect(cache.directoryLookupCycleLeft).To(Equal(cache.DirectoryLatency))
				Expect(cache.needTick).To(BeTrue())
			})
		})

		Context("data-ready", func() {
			var (
				block      *Block
				mshrEntry  *MSHREntry
				readBottom *mem.ReadReq
				dataReady  *mem.DataReadyRsp
			)

			BeforeEach(func() {
				block = new(Block)
				mshrEntry = mshr.Add(0x100)
				mshrEntry.Block = block

				readBottom = mem.NewReadReq(8, cache.ToBottom, nil, 0x100, 64)
				cache.pendingReadReqs[readBottom.ID] = readBottom
				dataReady = mem.NewDataReadyRsp(10, nil, cache.ToBottom, readBottom.ID)
				dataReady.Data = []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}
				// cache.ToBottom.Recv(dataReady)

				directory.ExpectLookup(0x100, block)
			})

			It("should stall if bank is busy", func() {
				toBottom.EXPECT().Peek().Return(dataReady)
				cache.bankBusy[0].req = mem.NewReadReq(8, nil, cache.ToBottom, 0x104, 4)
				cache.bankBusy[0].cycleLeft = 10

				cache.runParseStage(11)
			})

			It("should write to local", func() {
				toBottom.EXPECT().Peek().Return(dataReady)
				toBottom.EXPECT().
					Retrieve(akita.VTimeInSec(11)).
					Return(dataReady)

				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runParseStage(11)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(dataReady))
				Expect(cache.bankBusy[0].cycleLeft).To(Equal(10))
				Expect(cache.bankBusy[0].block).To(BeIdenticalTo(block))
				Expect(cache.pendingReadReqs).NotTo(HaveKey(readBottom.ID))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(mshr.Query(0x100)).To(BeNil())
			})

		})
	})

	Context("directory", func() {
		It("should decrement directory lookup cycle left", func() {
			cache.directoryLookupReq = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
			cache.directoryLookupCycleLeft = 2

			cache.runDirectoryStage(11)

			Expect(cache.directoryLookupCycleLeft).To(Equal(1))
		})

		Context("read hit", func() {
			var (
				block *Block
				read  *mem.ReadReq
			)

			BeforeEach(func() {
				block = new(Block)
				block.CacheAddress = 0x200
				directory.ExpectLookup(0x100, block)

				read = mem.NewReadReq(6, topModule, cache.ToTop, 0x104, 4)
				cache.directoryLookupReq = read
				cache.directoryLookupCycleLeft = 0
				cache.needTick = false
			})

			It("should stall if bank busy", func() {
				cache.bankBusy[0].req = mem.NewReadReq(0,
					topModule, cache.ToTop, 0x204, 4)

				cache.runDirectoryStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(read))
				Expect(cache.needTick).To(BeFalse())
			})

			It("should stall is block is locked", func() {
				block.IsLocked = true

				cache.runDirectoryStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(read))
				Expect(cache.needTick).To(BeFalse())
			})

			It("should do local read", func() {
				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(10)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(read))
				Expect(cache.bankBusy[0].block).To(BeIdenticalTo(block))
				Expect(cache.bankBusy[0].cycleLeft).To(Equal(cache.Latency))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(block.IsLocked).To(BeTrue())
				Expect(cache.needTick).To(BeTrue())
			})
		})

		Context("read miss", func() {
			var (
				block *Block
				read  *mem.ReadReq
			)

			BeforeEach(func() {
				block = new(Block)
				block.CacheAddress = 0x200
				directory.ExpectLookup(0x100, nil)
				directory.ExpectEvict(0x100, block)

				read = mem.NewReadReq(6, topModule, cache.ToTop, 0x104, 4)
				cache.directoryLookupReq = read
				cache.directoryLookupCycleLeft = 0
				cache.needTick = false
			})

			It("should insert into existing mshr entry if possible", func() {
				mshrEntry := mshr.Add(0x100)

				directory.ExpectLookup(0x100, nil)

				cache.runDirectoryStage(11)

				Expect(cache.readBuffer).To(HaveLen(0))
				Expect(mshrEntry.Requests).To(HaveLen(1))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(cache.needTick).To(BeTrue())
			})

			It("should stall if mshr is full", func() {
				for i := 0; i < mshr.CapacityInNumEntries; i++ {
					mshr.Add(uint64(i)*cache.blockSizeInByte() + 0x200)
				}

				cache.runDirectoryStage(11)

				Expect(cache.readBuffer).To(HaveLen(0))
				Expect(cache.directoryLookupReq).To(BeIdenticalTo(read))
				Expect(cache.needTick).To(BeFalse())
			})

			It("should request from bottom", func() {
				cache.runDirectoryStage(11)

				Expect(cache.readBuffer).To(HaveLen(1))
				mshrEntry := mshr.Query(0x100)
				Expect(mshrEntry).NotTo(BeNil())
				Expect(mshrEntry.Requests).To(HaveLen(1))
				Expect(mshrEntry.Block).To(BeIdenticalTo(block))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(cache.pendingReadReqs).To(HaveLen(1))
				Expect(block.IsLocked).To(BeTrue())
				Expect(block.Tag).To(Equal(uint64(0x100)))
				Expect(cache.needTick).To(BeTrue())
			})
		})

		Context("write hit", func() {
			var (
				write *mem.WriteReq
				block *Block
			)

			BeforeEach(func() {
				write = mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = write
				cache.directoryLookupCycleLeft = 0
				cache.needTick = false

				block = new(Block)
				directory.ExpectLookup(0x100, block)
			})

			It("should stall if the write buffer is full", func() {
				cache.writeBuffer = make([]*mem.WriteReq, cache.writeBufferCapacity)

				cache.runDirectoryStage(10)

				Expect(cache.writeBuffer).To(HaveLen(cache.writeBufferCapacity))
				Expect(cache.directoryLookupReq).NotTo(BeNil())
				Expect(cache.needTick).To(BeFalse())
			})

			It("should stall if hit but bank is busy", func() {
				cache.bankBusy[0].req = mem.NewReadReq(
					8, nil, cache.ToTop, 0x500, 4)

				cache.runDirectoryStage(10)

				Expect(cache.writeBuffer).To(HaveLen(0))
				Expect(cache.pendingWriteReqs).To(HaveLen(0))
				Expect(cache.directoryLookupReq).NotTo(BeNil())
			})

			It("should stall if block is locked", func() {
				block.IsLocked = true

				cache.runDirectoryStage(10)

				Expect(cache.writeBuffer).To(HaveLen(0))
				Expect(cache.pendingWriteReqs).To(HaveLen(0))
				Expect(cache.directoryLookupReq).NotTo(BeNil())
				Expect(cache.needTick).To(BeFalse())
			})

			It("should write to local ", func() {
				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(10)

				Expect(cache.writeBuffer).To(HaveLen(1))
				Expect(cache.pendingWriteReqs).To(HaveLen(1))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(write))
				Expect(cache.bankBusy[0].cycleLeft).To(Equal(cache.Latency))
				Expect(cache.bankBusy[0].block).To(BeIdenticalTo(block))
				Expect(block.IsLocked).To(BeTrue())
				Expect(cache.needTick).To(BeTrue())
			})

			It("should send write down", func() {
				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(10)

				Expect(cache.writeBuffer).To(HaveLen(1))
				Expect(cache.pendingWriteReqs).To(HaveLen(1))
				Expect(cache.directoryLookupReq).To(BeNil())
			})
		})
	})

	Context("bank stage", func() {

		Context("read", func() {
			var (
				block *Block
				read  *mem.ReadReq
			)

			BeforeEach(func() {
				block = new(Block)
				block.Tag = 0x100
				block.CacheAddress = 0x200
				block.IsLocked = true
				read = mem.NewReadReq(10, topModule, cache.ToTop,
					0x104, 4)
				storage.Write(0x204, []byte{1, 2, 3, 4})

				cache.bankBusy[0].req = read
				cache.bankBusy[0].block = block
				cache.bankBusy[0].cycleLeft = 0
				cache.needTick = false
			})

			It("should stall is fulfill stage is busy", func() {
				cache.fulfillStageBusy = true

				cache.runBankStage(0, 11)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(read))
				Expect(cache.needTick).To(BeFalse())
			})

			It("should send to fulfill stage", func() {
				cache.runBankStage(0, 11)

				Expect(cache.fulfillStageBusy).To(BeTrue())
				Expect(cache.fulfillingData).To(Equal([]byte{
					0, 0, 0, 0, 1, 2, 3, 4,
					0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0,
				}))
				Expect(cache.fulfillingAddr).To(Equal(uint64(0x100)))
				Expect(block.IsLocked).To(BeFalse())
				Expect(cache.needTick).To(BeTrue())
				Expect(cache.bankBusy[0].req).To(BeNil())
			})
		})

		Context("write", func() {
			It("should complete local write", func() {
				block := new(Block)
				block.IsLocked = true
				block.CacheAddress = 0x200
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				req.Data = []byte{1, 2, 3, 4}
				cache.bankBusy[0].req = req
				cache.bankBusy[0].block = block

				cache.runBankStage(0, 11)

				data, _ := storage.Read(0x204, 4)
				Expect(data).To(Equal([]byte{1, 2, 3, 4}))
				Expect(cache.bankBusy[0].req).To(BeNil())
				Expect(block.IsLocked).To(BeFalse())
			})
		})

		Context("write from bottom", func() {
			var (
				block     *Block
				dataReady *mem.DataReadyRsp
			)

			BeforeEach(func() {
				block = new(Block)
				block.Tag = 0x100
				block.CacheAddress = 0x200
				block.IsLocked = true
				dataReady = mem.NewDataReadyRsp(10, nil, cache.ToBottom, "")
				dataReady.Data = []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}
				cache.bankBusy[0].block = block
				cache.bankBusy[0].req = dataReady
				cache.bankBusy[0].cycleLeft = 0
			})

			It("should stall if fulfill stage is busy", func() {
				cache.fulfillStageBusy = true

				cache.runBankStage(0, 11)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(dataReady))
				Expect(cache.needTick).To(BeFalse())
			})

			It("should write to local storage and send to fulfill stage", func() {
				cache.runBankStage(0, 11)

				data, _ := storage.Read(0x200, 64)
				Expect(data).To(Equal(dataReady.Data))
				Expect(cache.bankBusy[0].req).To(BeNil())
				Expect(cache.fulfillStageBusy).To(BeTrue())
				Expect(cache.fulfillingAddr).To(Equal(uint64(0x100)))
				Expect(cache.fulfillingData).To(Equal(dataReady.Data))
				Expect(block.IsLocked).To(BeFalse())
				Expect(cache.needTick).To(BeTrue())
			})
		})

		Context("write done", func() {
			It("should reply done when receive done from bottom", func() {
				writeFromTop := mem.NewWriteReq(6, nil, cache.ToTop, 0x104)
				cache.processingWriteReqs = append(cache.processingWriteReqs, writeFromTop)
				writeToBottom := mem.NewWriteReq(8, cache.ToBottom, nil, 0x104)
				cache.pendingWriteReqs[writeToBottom.ID] = writeToBottom
				done := mem.NewDoneRsp(10, nil, cache.ToTop, writeToBottom.ID)

				toBottom.EXPECT().Peek().Return(done)
				toBottom.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(done)

				// cache.ToBottom.Recv(done)

				cache.runParseStage(11)

				Expect(cache.upGoingBuffer).To(HaveLen(1))
				Expect(cache.processingWriteReqs).To(HaveLen(0))
				Expect(cache.processingWriteReqs).NotTo(ContainElement(writeFromTop))
				Expect(cache.pendingWriteReqs).NotTo(HaveKey(writeToBottom.ID))
			})
		})
	})

	Context("fulfill stage", func() {
		var (
			req1 *mem.ReadReq
		)

		BeforeEach(func() {
			req1 = mem.NewReadReq(10, nil, cache.ToTop, 0x104, 4)

			cache.processingReqs[0x100] = append(cache.processingReqs[0x100], req1)

			cache.fulfillStageBusy = true
			cache.fulfillingAddr = 0x100
			cache.fulfillingData = []byte{
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
			}
		})

		It("should clear fulfill stage if no more request to fulfill", func() {
			cache.processingReqs[0x100] = nil

			cache.runFulfillStage(16)

			Expect(cache.fulfillStageBusy).To(BeFalse())
		})

		It("should stall if up-going buffer is full", func() {
			cache.upGoingBuffer = make([]akita.Req, cache.upGoingBufferCapacity)

			cache.runFulfillStage(16)

			Expect(cache.fulfillStageBusy).To(BeTrue())
			Expect(cache.processingReqs[0x100]).To(HaveLen(1))
		})

		It("should put data-ready in up-going buffer", func() {
			cache.runFulfillStage(16)

			expectedRsp := mem.NewDataReadyRsp(16, cache.ToTop, nil, req1.ID)
			expectedRsp.Data = []byte{5, 6, 7, 8}

			Expect(cache.upGoingBuffer).To(HaveLen(1))
			Expect(cache.processingReqs[0x100]).NotTo(ContainElement(req1))
		})
	})
})

type mockComponent struct {
	*akita.ComponentBase

	ToMem akita.Port

	receivedReqs []akita.Req
}

func (c *mockComponent) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
	panic("implement me")
}

func (c *mockComponent) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	req := port.Retrieve(now)
	c.receivedReqs = append(c.receivedReqs, req)
}

func (mockComponent) Handle(e akita.Event) error {
	panic("implement me")
}

func newMockComponent() *mockComponent {
	componentBase := akita.NewComponentBase("mock")
	component := new(mockComponent)
	component.ComponentBase = componentBase
	component.ToMem = akita.NewLimitNumReqPort(component, 1)
	return component
}

var _ = Describe("Write-Around Cache Integration", func() {

	var (
		engine          akita.Engine
		evictor         *LRUEvictor
		directory       *DirectoryImpl
		mshr            *MSHRImpl
		lowModuleFinder *SingleLowModuleFinder
		storage         *mem.Storage
		cache           *WriteAroundCache
		dram            *mem.IdealMemController
		conn            *akita.DirectConnection
		agent           *mockComponent
	)

	BeforeEach(func() {
		agent = newMockComponent()

		engine = akita.NewSerialEngine()
		evictor = NewLRUEvictor()
		directory = NewDirectory(1024, 4, 64, evictor)
		mshr = NewMSHR(4)
		lowModuleFinder = new(SingleLowModuleFinder)
		storage = mem.NewStorage(4 * mem.MB)
		cache = NewWriteAroundCache("Cache", engine, directory, mshr,
			lowModuleFinder, storage)
		cache.Freq = 1 * akita.GHz
		cache.Latency = 4

		dram = mem.NewIdealMemController("Dram", engine, 4*mem.GB)
		dram.Freq = 1 * akita.GHz
		dram.Latency = 20
		lowModuleFinder.LowModule = dram.ToTop

		conn = akita.NewDirectConnection(engine)
		conn.PlugIn(cache.ToTop)
		conn.PlugIn(cache.ToBottom)
		conn.PlugIn(dram.ToTop)
		conn.PlugIn(agent.ToMem)
	})

	It("should handle read", func() {
		dram.Storage.Write(5000, []byte{1, 2, 3, 4})

		readReq := mem.NewReadReq(10, agent.ToMem, cache.ToTop, 5000, 4)
		readReq.SetRecvTime(10)

		cache.ToTop.Recv(readReq)
		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(1))
		dataReady := agent.receivedReqs[0].(*mem.DataReadyRsp)
		Expect(dataReady.Data).To(Equal([]byte{1, 2, 3, 4}))
	})

	It("should handle write", func() {
		writeReq := mem.NewWriteReq(8, agent.ToMem, cache.ToTop, 5000)
		writeReq.SetRecvTime(8)
		writeReq.Data = []byte{1, 2, 3, 4}

		cache.ToTop.Recv(writeReq)
		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(1))
		done := agent.receivedReqs[0].(*mem.DoneRsp)
		Expect(done.RespondTo).To(Equal(writeReq.ID))

		readReq := mem.NewReadReq(10, agent.ToMem, cache.ToTop, 5000, 4)
		readReq.SetRecvTime(10)

		cache.ToTop.Recv(readReq)
		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(2))
		dataReady := agent.receivedReqs[1].(*mem.DataReadyRsp)
		Expect(dataReady.Data).To(Equal([]byte{1, 2, 3, 4}))
	})

	//It("should handle read after write", func() {
	//	writeReq := mem.NewWriteReq(8, agent, cache, 5000)
	//	writeReq.SetRecvTime(8)
	//	writeReq.Data = []byte{1, 2, 3, 4}
	//
	//	readReq := mem.NewReadReq(10, agent, cache, 5000, 4)
	//	readReq.SetRecvTime(8)
	//
	//	cache.Recv(writeReq)
	//	cache.Recv(readReq)
	//	engine.Run()
	//
	//	Expect(agent.receivedReqs).To(HaveLen(2))
	//	done := agent.receivedReqs[1].(*mem.DoneRsp)
	//	Expect(done.RespondTo).To(Equal(writeReq.ID))
	//	dataReady := agent.receivedReqs[0].(*mem.DataReadyRsp)
	//	Expect(dataReady.Data).To(Equal([]byte{1, 2, 3, 4}))
	//})

})
