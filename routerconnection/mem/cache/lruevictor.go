package cache

// LRUEvictor structure
type LRUEvictor struct {
}

// NewLRUEvictor returns a newly constructed lru evictor
func NewLRUEvictor() *LRUEvictor {
	e := new(LRUEvictor)
	return e
}

// Evict implements the eviction method
func (e *LRUEvictor) Evict(set *Set) *Block {
	// First try evicting an empty block
	for _, block := range set.LRUQueue {
		if !block.IsValid && !block.IsLocked {
			return block
		}
	}

	for _, block := range set.LRUQueue {
		if !block.IsLocked {
			return block
		}
	}

	return set.LRUQueue[0]
}
