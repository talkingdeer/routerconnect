package cache

import "log"

type MockDirectory struct {
	expectLookupAddr  []uint64
	expectLookupBlock []*Block

	expectEvictAddr  []uint64
	expectEvictBlock []*Block

	Resetted bool
}

func (d *MockDirectory) TotalSize() uint64 {
	panic("not implemented")
}

func (d *MockDirectory) Lookup(address uint64) *Block {
	if len(d.expectLookupAddr) == 0 {
		log.Panicf("address not expected 0x%x", address)
	}

	ret := d.expectLookupBlock[0]
	d.expectLookupAddr = d.expectLookupAddr[1:]
	d.expectLookupBlock = d.expectLookupBlock[1:]
	return ret
}

func (d *MockDirectory) Evict(address uint64) *Block {
	if len(d.expectEvictAddr) == 0 || address != d.expectEvictAddr[0] {
		log.Panicf("address not expected 0x%x", address)
	}

	block := d.expectEvictBlock[0]
	d.expectEvictAddr = d.expectEvictAddr[1:]
	d.expectEvictBlock = d.expectEvictBlock[1:]

	return block
}

func (d *MockDirectory) WayAssociativity() int {
	return 1
}

func (d *MockDirectory) ExpectEvict(addr uint64, block *Block) {
	d.expectEvictAddr = append(d.expectEvictAddr, addr)
	d.expectEvictBlock = append(d.expectEvictBlock, block)
}

func (d *MockDirectory) ExpectLookup(addr uint64, block *Block) {
	d.expectLookupAddr = append(d.expectLookupAddr, addr)
	d.expectLookupBlock = append(d.expectLookupBlock, block)
}

func (d *MockDirectory) AllExpectedCalled() bool {
	if len(d.expectEvictAddr) > 0 {
		return false
	}
	if len(d.expectLookupAddr) > 0 {
		return false
	}
	return true
}

func (d *MockDirectory) Reset() {
	d.Resetted = true
}
