package cache

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Directory", func() {

	var (
		evictor   *LRUEvictor
		directory *DirectoryImpl
	)

	BeforeEach(func() {
		evictor = NewLRUEvictor()
		directory = NewDirectory(1024, 4, 64, evictor)
	})

	It("should be able to get total size", func() {
		Expect(directory.TotalSize()).To(Equal(uint64(262144)))
	})

	It("should find empty spot at beginning", func() {
		block := directory.Evict(0x40)

		Expect(block.SetID).To(Equal(int(1)))
		Expect(block.WayID).To(Equal(int(0)))
		Expect(block.CacheAddress).To(Equal(uint64(0x100)))

		block.Tag = 0x40
		block.IsValid = true
	})

	It("should be able to lookup occupied block", func() {
		block := directory.Evict(0x40)
		block.Tag = 0x40
		block.IsValid = true

		block = directory.Lookup(0x40)

		Expect(block).NotTo(BeNil())
		Expect(block.CacheAddress).To(Equal(uint64(0x100)))
	})

	It("should not be able to lookup unoccupied block", func() {
		block := directory.Evict(0x40)
		block.Tag = 0x40
		block.IsValid = true

		block = directory.Lookup(0x80)
		Expect(block).To(BeNil())

		block = directory.Lookup(0x140)
		Expect(block).To(BeNil())
	})

	It("should be 4-way associative", func() {
		block := directory.Evict(0x40)
		Expect(block.CacheAddress).To(Equal(uint64(0x100)))
		block.Tag = 0x40
		block.IsValid = true

		block = directory.Evict(0x10040)
		Expect(block.CacheAddress).To(Equal(uint64(0x140)))
		block.IsValid = true
		block.Tag = 0x10040

		block = directory.Evict(0x20040)
		Expect(block.CacheAddress).To(Equal(uint64(0x180)))
		block.IsValid = true
		block.Tag = 0x20040

		block = directory.Evict(0x40040)
		Expect(block.CacheAddress).To(Equal(uint64(0x1C0)))
		block.IsValid = true
		block.Tag = 0x40040

		block = directory.Evict(0x50040)
		Expect(block).NotTo(BeNil())
		Expect(block.CacheAddress).To(Equal(uint64(0x100)))
	})

	It("should reuse evicted blocks", func() {
		block := directory.Evict(0x10040)
		block.IsValid = true
		block.Tag = 0x10040

		block = directory.Evict(0x40)
		block.Tag = 0x40
		block.IsValid = true

		block = directory.Evict(0x20040)
		block.IsValid = true
		block.Tag = 0x20040

		block = directory.Evict(0x40040)
		block.IsValid = true
		block.Tag = 0x40040

		block = directory.Evict(0x50040)
		Expect(block.Tag).To(Equal(uint64(0x10040)))
		Expect(block.CacheAddress).To(Equal(uint64(0x100)))
	})

	It("should locate the correct evicted blocks", func() {
		block := directory.Evict(0x40)
		block.Tag = 0x40
		block.IsValid = true

		block = directory.Evict(0x10040)
		block.IsValid = true
		block.Tag = 0x10040

		block = directory.Evict(0x20040)
		block.IsValid = true
		block.Tag = 0x20040

		block = directory.Evict(0x40040)
		block.IsValid = true
		block.Tag = 0x40040

		directory.Lookup(0x40)

		block = directory.Evict(0x50040)
		Expect(block.CacheAddress).To(Equal(uint64(0x140)))
		Expect(block.Tag).To(Equal(uint64(0x10040)))
	})

})
