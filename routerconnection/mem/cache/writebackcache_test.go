package cache

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/akita/mock_akita"
	"gitlab.com/akita/mem"
)

var _ = Describe("Write-Back Cache", func() {

	var (
		mockCtrl *gomock.Controller

		engine          *mock_akita.MockEngine
		cache           *WriteBackCache
		directory       *MockDirectory
		storage         *mem.Storage
		mshr            *MSHRImpl
		lowModuleFinder *SingleLowModuleFinder

		toTop    *mock_akita.MockPort
		toBottom *mock_akita.MockPort
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())

		engine = mock_akita.NewMockEngine(mockCtrl)
		mshr = NewMSHR(128)
		directory = new(MockDirectory)
		lowModuleFinder = new(SingleLowModuleFinder)
		storage = mem.NewStorage(4 * mem.GB)

		cache = NewWriteBackCache("Cache", engine, directory, mshr, lowModuleFinder, storage)
		toTop = mock_akita.NewMockPort(mockCtrl)
		toBottom = mock_akita.NewMockPort(mockCtrl)
		cache.ToTop = toTop
		cache.ToBottom = toBottom
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Context("parse stage", func() {
		Context("read req", func() {
			var req *mem.ReadReq

			BeforeEach(func() {
				req = mem.NewReadReq(10, nil, cache.ToTop, 0x104, 4)
			})

			It("should append it to the reading list", func() {
				cache.processingReqs[0x100] = make([]mem.AccessReq, 0)

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)
				toTop.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(nil)

				cache.runParseStage(11)

				Expect(cache.processingReqs[0x100]).To(HaveLen(1))
			})

			It("should append it to the reading list, even if the directory is busy", func() {
				cache.directoryLookupReq = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupCycleLeft = 2

				cache.processingReqs[0x100] = make([]mem.AccessReq, 0)

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)
				toTop.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(nil)

				cache.runParseStage(10)

				Expect(cache.processingReqs[0x100]).To(HaveLen(1))
			})

			It("should stall if writing the same cache-line", func() {
				writeReq := mem.NewWriteReq(10, nil, cache.ToTop, 0x108)
				cache.processingWriteReqs = append(cache.processingWriteReqs, writeReq)

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)

				cache.runParseStage(10)

				Expect(cache.processingReqs).NotTo(HaveKey(0x100))
				Expect(cache.directoryLookupReq).NotTo(BeIdenticalTo(req))
			})

			It("should stall if evicting the same address", func() {
				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)

				cache.evictingList[0x100] = true
				cache.runParseStage(10)
			})

			It("should send to directory stage", func() {
				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)
				toTop.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(nil)

				cache.runParseStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
				Expect(cache.directoryLookupCycleLeft).To(Equal(2))
				Expect(cache.processingReqs[0x100]).To(HaveLen(1))
			})

			It("should stall if directory is busy", func() {
				cache.directoryLookupReq = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupCycleLeft = 2

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)

				cache.runParseStage(10)

				// Expect(cache.ToTop.Buf).To(HaveLen(1))
				Expect(cache.processingReqs).NotTo(HaveKey(0x100))
				Expect(cache.directoryLookupReq).NotTo(BeIdenticalTo(req))
			})
		})

		Context("write req", func() {
			var req *mem.WriteReq

			BeforeEach(func() {
				req = mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
			})

			It("should stall if reading to the same address", func() {
				cache.processingReqs[0x100] = make([]mem.AccessReq, 1)

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)

				cache.runParseStage(10)
			})

			It("should stall if evicting the same address", func() {
				cache.evictingList[0x100] = true

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)

				cache.runParseStage(10)
			})

			It("should stall if directory stage busy", func() {
				cache.directoryLookupReq = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupCycleLeft = 2

				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)

				cache.runParseStage(10)

				Expect(cache.directoryLookupReq).NotTo(BeIdenticalTo(req))
			})

			It("should send to directory", func() {
				toBottom.EXPECT().Peek().Return(nil)
				toTop.EXPECT().Peek().Return(req)
				toTop.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(req)

				cache.runParseStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
				Expect(cache.directoryLookupCycleLeft).To(Equal(cache.DirectoryLatency))
				Expect(cache.processingWriteReqs).To(ContainElement(req))
				Expect(cache.processingReqs[0x100]).To(ContainElement(req))
			})
		})

		Context("data-ready", func() {
			var (
				block      *Block
				readBottom *mem.ReadReq
				dataReady  *mem.DataReadyRsp
				mshrEntry  *MSHREntry
			)

			BeforeEach(func() {
				block = new(Block)
				block.Tag = 0x400
				block.CacheAddress = 0x200
				mshrEntry = mshr.Add(0x100)
				mshrEntry.Block = block
				readBottom = mem.NewReadReq(8, cache.ToBottom, nil, 0x100, 64)
				cache.pendingReadReqs[readBottom.ID] = readBottom
				dataReady = mem.NewDataReadyRsp(10, nil, cache.ToBottom, readBottom.ID)
				dataReady.Data = []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}
				directory.ExpectLookup(0x100, block)
			})

			It("should write to local", func() {
				// cache.ToBottom.Recv(dataReady)
				toBottom.EXPECT().Peek().Return(dataReady)
				toBottom.EXPECT().
					Retrieve(akita.VTimeInSec(11)).
					Return(dataReady)

				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runParseStage(11)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(dataReady))
				Expect(cache.bankBusy[0].cycleLeft).To(Equal(10))
				Expect(cache.bankBusy[0].block).To(BeIdenticalTo(block))
				Expect(cache.pendingReadReqs).NotTo(HaveKey(readBottom.ID))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(block.Tag).To(Equal(uint64(0x100)))
			})

			It("should stall if bank is busy", func() {
				toBottom.EXPECT().Peek().Return(dataReady)

				cache.bankBusy[0].req = mem.NewReadReq(8, nil, cache.ToBottom, 0x104, 4)
				cache.bankBusy[0].cycleLeft = 10

				cache.runParseStage(11)
			})
		})

		Context("write-done", func() {
			var (
				writeToBottom *mem.WriteReq
				done          *mem.DoneRsp
				block         *Block
				mshrEntry     *MSHREntry
			)

			BeforeEach(func() {
				writeToBottom = mem.NewWriteReq(10, cache.ToBottom, nil, 0x400)
				cache.pendingWriteReqs[writeToBottom.ID] = writeToBottom
				done = mem.NewDoneRsp(11, nil, cache.ToBottom, writeToBottom.ID)

				block = new(Block)
				block.Tag = 0x400
				mshrEntry = mshr.Add(0x100)
				mshrEntry.Block = block

				cache.evictingList[0x400] = true
			})

			It("should read from bottom when receive done rsp for eviction", func() {
				readFromTop := mem.NewReadReq(6, nil, cache.ToTop, 0x104, 4)
				mshrEntry.Requests = append(mshrEntry.Requests, readFromTop)

				toBottom.EXPECT().Peek().Return(done)
				toBottom.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(nil)

				cache.runParseStage(11)

				Expect(cache.readBuffer).To(HaveLen(1))
				Expect(cache.pendingReadReqs).To(HaveLen(1))
				Expect(cache.evictingList).To(HaveLen(0))
			})

			Context("write whole-block", func() {
				var (
					write *mem.WriteReq
				)

				BeforeEach(func() {
					write = mem.NewWriteReq(6, nil, cache.ToTop, 0x100)
					write.Data = []byte{
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
					}

					mshrEntry.Requests = append(mshrEntry.Requests, write)
				})

				It("should stall if bank busy", func() {
					cache.bankBusy[0].req = mem.NewReadReq(5, nil, cache.ToTop, 0x204, 4)

					toBottom.EXPECT().Peek().Return(done)

					cache.runParseStage(11)
				})
			})

		})
	})

	Context("directory stage", func() {
		It("should decrement directory lookup cycle left", func() {
			cache.directoryLookupReq = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
			cache.directoryLookupCycleLeft = 2

			cache.runDirectoryStage(11)

			Expect(cache.directoryLookupCycleLeft).To(Equal(1))
		})

		Context("read hit", func() {
			var block *Block

			BeforeEach(func() {
				block = new(Block)
				block.CacheAddress = 0x200
				directory.ExpectLookup(0x100, block)
			})

			It("should do local read on read hit", func() {
				req := mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(11)

				Expect(cache.bankBusy[0].cycleLeft).To(Equal(cache.Latency))
				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(req))
				Expect(cache.bankBusy[0].block).To(BeIdenticalTo(block))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(block.IsLocked).To(BeTrue())
			})

			It("should stall on bank busy", func() {
				req := mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				cache.bankBusy[0].cycleLeft = 2
				cache.bankBusy[0].req = mem.NewReadReq(8, nil, cache.ToTop, 0x208, 4)

				cache.runDirectoryStage(11)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
				Expect(cache.bankBusy[0].req).NotTo(BeIdenticalTo(req))
			})

			It("should stall on block locked", func() {
				req := mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				block.IsLocked = true

				cache.runDirectoryStage(11)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
			})
		})

		Context("read-miss, need eviction", func() {
			var block *Block
			BeforeEach(func() {
				directory.ExpectLookup(0x100, nil)
				block = new(Block)
				block.Tag = 0x200
				block.IsDirty = true
				block.IsValid = true
				block.IsLocked = false
				directory.ExpectEvict(0x100, block)
			})

			It("should read for evict", func() {
				req := mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)

				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				block.IsDirty = true
				block.IsValid = true

				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(10)

				Expect(block.IsLocked).To(BeTrue())
				Expect(cache.bankBusy[0].evict).To(BeIdenticalTo(block))
				Expect(cache.bankBusy[0].cycleLeft).To(Equal(cache.Latency))
				mshrEntry := mshr.Query(0x100)
				Expect(mshrEntry).NotTo(BeNil())
				Expect(mshrEntry.Requests).To(HaveLen(1))
				Expect(mshrEntry.Block).To(BeIdenticalTo(block))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(cache.evictingList).To(HaveKey(uint64(0x200)))
			})
		})

		Context("read-miss, no eviction", func() {
			var block *Block
			BeforeEach(func() {
				directory.ExpectLookup(0x100, nil)
				block = new(Block)
				block.IsDirty = false
				block.IsValid = false
				block.IsLocked = false
				directory.ExpectEvict(0x100, block)
			})

			It("should send read-request if no need for eviction", func() {
				req := mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)

				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				cache.runDirectoryStage(11)

				Expect(block.IsLocked).To(BeTrue())
				mshrEntry := mshr.Query(0x100)
				Expect(mshrEntry).NotTo(BeNil())
				Expect(mshrEntry.Requests).To(HaveLen(1))
				Expect(mshrEntry.Block).To(BeIdenticalTo(block))
				Expect(cache.directoryLookupReq).To(BeNil())
			})

			It("should stall if bank is busy", func() {
				req := mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)

				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				cache.bankBusy[0].req = mem.NewReadReq(8, nil, cache.ToTop, 0x200, 4)
				cache.bankBusy[0].cycleLeft = 10

				cache.runDirectoryStage(11)

				Expect(directory.AllExpectedCalled()).To(BeTrue())
				mshrEntry := mshr.Query(0x100)
				Expect(mshrEntry).NotTo(BeNil())
				Expect(mshrEntry.Requests).To(HaveLen(1))
				Expect(mshrEntry.Block).To(BeIdenticalTo(block))
				Expect(cache.directoryLookupReq).To(BeNil())
				Expect(cache.readBuffer).To(HaveLen(1))
			})

			It("should add request to MSHR entry if the address is already in MSHR", func() {
				req := mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				mshrEntry := mshr.Add(0x100)

				cache.runDirectoryStage(11)

				Expect(cache.readBuffer).To(HaveLen(0))
				Expect(mshrEntry.Requests).To(HaveLen(1))
				Expect(cache.directoryLookupReq).To(BeNil())
			})

			It("should stall if mshr is full", func() {
				for i := 0; i < mshr.CapacityInNumEntries; i++ {
					mshr.Add(uint64(i)*cache.blockSizeInByte() + 0x200)
				}

				req := mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				cache.runDirectoryStage(11)

				Expect(cache.readBuffer).To(HaveLen(0))
				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
			})
		})

		Context("write-hit", func() {
			var block *Block

			BeforeEach(func() {
				block = new(Block)
				block.CacheAddress = 0x200
				directory.ExpectLookup(0x100, block)
			})

			It("should stall if bank is busy", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				cache.bankBusy[0].req = mem.NewReadReq(10, nil, cache.ToTop, 0x100, 4)

				cache.runDirectoryStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
			})

			It("should stall if block is locked", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				block.IsLocked = true

				// engine.EXPECT().
				// 	Schedule(gomock.AssignableToTypeOf(
				// 		&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
			})

			It("should write to local", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(10)

				Expect(directory.AllExpectedCalled()).To(BeTrue())
				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(req))
				Expect(cache.bankBusy[0].block).To(BeIdenticalTo(block))
				Expect(cache.bankBusy[0].cycleLeft).To(Equal(cache.Latency))
				Expect(cache.directoryLookupReq).To(BeNil())
			})

		})

		Context("write-miss, no-eviction", func() {
			var evict *Block
			BeforeEach(func() {
				directory.ExpectLookup(0x100, nil)

				evict = new(Block)
				evict.IsLocked = false
				evict.IsDirty = false
				evict.IsValid = true
				directory.ExpectEvict(0x100, evict)
			})

			It("should stall if address is not in mshr and mshr is full", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				for i := 0; i < mshr.CapacityInNumEntries; i++ {
					mshr.Add(uint64(i*0x40 + 0x1000))
				}

				cache.runDirectoryStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
			})

			It("should stall if the evict block is locked", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				evict.IsLocked = true

				cache.runDirectoryStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
			})

			It("should insert into mshr if the address is already in mshr", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				mshrEntry := mshr.Add(0x100)

				cache.runDirectoryStage(10)

				Expect(mshrEntry.Requests[0]).To(BeIdenticalTo(req))
			})

			It("should create mshr entry and send read to bottom", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				cache.runDirectoryStage(10)

				mshrEntry := mshr.Query(0x100)
				Expect(mshrEntry.Requests[0]).To(BeIdenticalTo(req))
				Expect(cache.readBuffer).To(HaveLen(1))
				Expect(cache.pendingReadReqs).To(HaveLen(1))
			})

			It("should write directly, if the write is to a whole cacheline", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				req.Data = []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(10)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(req))
				Expect(cache.bankBusy[0].block).To(BeIdenticalTo(evict))
				Expect(cache.bankBusy[0].cycleLeft).To(Equal(cache.Latency))
			})
		})

		Context("write-miss, need eviction", func() {
			var evict *Block
			BeforeEach(func() {
				directory.ExpectLookup(0x100, nil)

				evict = new(Block)
				evict.Tag = 0x200
				evict.IsLocked = false
				evict.IsDirty = true
				evict.IsValid = true
				directory.ExpectEvict(0x100, evict)
			})

			It("should stall if bank busy", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0
				cache.bankBusy[0].req = mem.NewReadReq(
					10, nil, cache.ToTop, 0x200, 4,
				)

				cache.runDirectoryStage(10)

				Expect(cache.directoryLookupReq).To(BeIdenticalTo(req))
			})

			It("should create mshr entry and read for eviction", func() {
				req := mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				cache.directoryLookupReq = req
				cache.directoryLookupCycleLeft = 0

				engine.EXPECT().
					Schedule(gomock.AssignableToTypeOf(
						&SetBankCycleLeftToZeroEvent{}))

				cache.runDirectoryStage(10)

				mshrEntry := mshr.Query(0x100)
				Expect(mshrEntry.Requests[0]).To(BeIdenticalTo(req))
				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(req))
				Expect(cache.bankBusy[0].evict).To(BeIdenticalTo(evict))
				Expect(cache.bankBusy[0].cycleLeft).To(Equal(cache.Latency))
				Expect(cache.evictingList).To(HaveKey(uint64(0x200)))
			})
		})
	})

	Context("bank stage", func() {

		Context("read-hit", func() {
			var (
				block         *Block
				req           *mem.ReadReq
				cacheLineData []byte
			)

			BeforeEach(func() {
				block = new(Block)
				block.Tag = 0x100
				block.CacheAddress = 0x200
				block.IsLocked = true

				req = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.bankBusy[0].cycleLeft = 0
				cache.bankBusy[0].req = req
				cache.bankBusy[0].block = block

				cacheLineData = []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}
				storage.Write(0x200, cacheLineData)
			})

			It("should stall if fulfill stage busy", func() {
				cache.fulfillStageBusy = true

				cache.runBank(0, 12)

				Expect(cache.bankBusy[0].req).NotTo(BeNil())
			})

			It("should send to fulfill stage", func() {

				cache.runBank(0, 12)

				Expect(cache.fulfillStageBusy).To(BeTrue())
				Expect(cache.fulfillingAddr).To(Equal(uint64(0x100)))
				Expect(cache.fulfillingData).To(Equal(cacheLineData))
				Expect(cache.bankBusy[0].req).To(BeNil())
				Expect(block.IsLocked).To(BeFalse())
			})
		})

		Context("read for eviction", func() {
			var block *Block
			var req *mem.ReadReq
			BeforeEach(func() {
				block = new(Block)
				block.CacheAddress = 0x200
				block.IsLocked = true
				block.IsValid = true
				block.IsDirty = true
				block.Tag = 0x400

				req = mem.NewReadReq(10, nil, cache.ToTop, 0x108, 4)
				cache.bankBusy[0].evict = block
				cache.bankBusy[0].req = req
				cache.bankBusy[0].cycleLeft = 0
			})

			It("should read and write to bottom", func() {
				storage.Write(0x200, []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				})

				expectWriteToBottom := mem.NewWriteReq(11, cache.ToBottom, nil, 0x400)
				expectWriteToBottom.Data = []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}

				cache.runBank(0, 11)

				Expect(cache.writeBuffer).To(HaveLen(1))
				Expect(cache.pendingWriteReqs).To(HaveLen(1))
				Expect(cache.bankBusy[0].req).To(BeNil())
			})

			It("should stall read for eviction if write-buffer is full", func() {
				cache.writeBuffer = make([]*mem.WriteReq, cache.writeBufferCapacity)

				cache.runBank(0, 11)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(req))
			})
		})

		Context("write-hit", func() {
			var req *mem.WriteReq
			var block *Block
			BeforeEach(func() {
				block = new(Block)
				block.Tag = 0x100
				block.CacheAddress = 0x200
				req = mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				req.Data = []byte{1, 2, 3, 4}
				cache.bankBusy[0].req = req
				cache.bankBusy[0].block = block
				cache.bankBusy[0].cycleLeft = 0
				cache.processingWriteReqs = append(cache.processingWriteReqs, req)
			})

			It("should stall if the fulfill stage is busy", func() {
				cache.fulfillStageBusy = true

				cache.runBank(0, 10)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(req))
			})

			It("should send to fulfill stage when write complete", func() {
				cache.runBank(0, 10)

				data, _ := storage.Read(0x204, 4)
				Expect(data).To(Equal([]byte{1, 2, 3, 4}))
				Expect(cache.fulfillStageBusy).To(BeTrue())
				Expect(cache.fulfillingAddr).To(Equal(uint64(0x100)))
				Expect(cache.bankBusy[0].req).To(BeNil())
				Expect(cache.bankBusy[0].mshrEntry).To(BeNil())
			})
		})

		Context("write a whole cache line", func() {
			var (
				req       *mem.WriteReq
				block     *Block
				mshrEntry *MSHREntry
			)

			BeforeEach(func() {
				mshrEntry = mshr.Add(0x100)
				block = new(Block)
				block.Tag = 0x100
				block.CacheAddress = 0x200
				req = mem.NewWriteReq(10, nil, cache.ToTop, 0x104)
				req.Data = []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}
				cache.bankBusy[0].req = req
				cache.bankBusy[0].block = block
				cache.bankBusy[0].cycleLeft = 0
				cache.bankBusy[0].mshrEntry = mshrEntry
				cache.processingWriteReqs = append(cache.processingWriteReqs, req)

			})

			It("should stall if fulfill stage is busy", func() {
				cache.fulfillStageBusy = true

				cache.runBank(0, 10)

				Expect(cache.bankBusy[0].req).To(BeIdenticalTo(req))
			})

			It("should send to fulfill stage when write complete", func() {
				cache.runBank(0, 10)

				data, _ := storage.Read(0x204, 4)
				Expect(data).To(Equal([]byte{1, 2, 3, 4}))
				Expect(cache.fulfillStageBusy).To(BeTrue())
				Expect(cache.fulfillingAddr).To(Equal(uint64(0x100)))
				Expect(cache.bankBusy[0].req).To(BeNil())
				mshrEntry := mshr.Query(0x100)
				Expect(mshrEntry).To(BeNil())
				Expect(cache.bankBusy[0].mshrEntry).To(BeNil())
			})
		})

		Context("write from bottom", func() {
			var (
				block     *Block
				dataReady *mem.DataReadyRsp
				mshrEntry *MSHREntry
			)

			BeforeEach(func() {
				block = new(Block)
				block.Tag = 0x100
				block.CacheAddress = 0x200
				dataReady = mem.NewDataReadyRsp(10, nil, cache.ToBottom, "")
				dataReady.Data = []byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}
				cache.bankBusy[0].block = block
				cache.bankBusy[0].req = dataReady
				cache.bankBusy[0].cycleLeft = 0

				mshrEntry = mshr.Add(0x100)
				mshrEntry.Block = block
			})

			It("should write to local storage", func() {
				cache.runBank(0, 11)

				data, _ := storage.Read(0x200, 64)
				Expect(data).To(Equal(dataReady.Data))
				Expect(cache.bankBusy[0].req).To(BeNil())
				//Expect(cache.fulfillingMSHREntry).To(BeIdenticalTo(mshrEntry))
				Expect(cache.fulfillStageBusy).To(BeTrue())
				Expect(cache.fulfillingAddr).To(Equal(uint64(0x100)))
				Expect(cache.fulfillingData).To(Equal(dataReady.Data))
				Expect(mshr.Query(0x100)).To(BeNil())
			})

			It("should stall if fulfill stage is busy", func() {
				cache.fulfillStageBusy = true

				cache.bankBusy[0].block = block
				cache.bankBusy[0].req = dataReady
				cache.bankBusy[0].cycleLeft = 0

				cache.runBank(0, 10)

				Expect(cache.bankBusy[0].req).NotTo(BeNil())
			})
		})
	})

	Context("fulfill stage", func() {
		var (
			req1 *mem.ReadReq
			req2 *mem.WriteReq
		)

		BeforeEach(func() {
			req1 = mem.NewReadReq(10, nil, cache.ToTop, 0x104, 4)
			req2 = mem.NewWriteReq(14, nil, cache.ToTop, 0x108)
			req2.Data = []byte{5, 6, 7, 8}

			cache.processingReqs[0x100] = append(cache.processingReqs[0x100], req1)
			cache.processingReqs[0x100] = append(cache.processingReqs[0x100], req2)
			cache.processingWriteReqs = append(cache.processingWriteReqs, req2)

			cache.fulfillStageBusy = true
			cache.fulfillingAddr = 0x100
			cache.fulfillingData = []byte{
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
				1, 2, 3, 4, 5, 6, 7, 8,
			}
		})

		It("should clear fulfill stage if no more request to fulfill", func() {
			cache.processingReqs[0x100] = nil

			cache.runFulfillStage(16)

			Expect(cache.fulfillStageBusy).To(BeFalse())
		})

		It("should stall if up-going buffer is full", func() {
			cache.upGoingBuffer = make([]akita.Req, cache.upGoingBufferCapacity)

			cache.runFulfillStage(16)

			Expect(cache.fulfillStageBusy).To(BeTrue())
			Expect(cache.processingReqs[0x100]).To(HaveLen(2))
		})

		It("should put data-ready in up-going buffer", func() {
			cache.runFulfillStage(16)

			expectedRsp := mem.NewDataReadyRsp(16, cache.ToTop, nil, req1.ID)
			expectedRsp.Data = []byte{5, 6, 7, 8}

			Expect(cache.upGoingBuffer).To(HaveLen(2))
			Expect(cache.processingReqs[0x100]).NotTo(ContainElement(req1))
		})

		It("should put done response in up-going buffer", func() {
			cache.processingReqs[0x100] = cache.processingReqs[0x100][1:]

			cache.runFulfillStage(16)

			Expect(cache.upGoingBuffer).To(HaveLen(1))
			Expect(cache.processingReqs[0x100]).NotTo(ContainElement(req1))
			Expect(cache.processingWriteReqs).NotTo(ContainElement(req2))
		})
	})

	Context("send stage", func() {
		It("should send message out", func() {
			req1 := mem.NewDataReadyRsp(10, cache.ToTop, nil, "")
			cache.upGoingBuffer = append(cache.upGoingBuffer, req1)
			req2 := mem.NewReadReq(10, cache.ToBottom, nil, 0x100, 64)
			cache.readBuffer = append(cache.readBuffer, req2)
			req3 := mem.NewWriteReq(10, cache.ToBottom, nil, 0x100)
			cache.writeBuffer = append(cache.writeBuffer, req3)

			toTop.EXPECT().Send(req1).Return(nil)
			toBottom.EXPECT().Send(req3).Return(nil)
			toBottom.EXPECT().Send(req2).Return(nil)

			cache.sendReqs(11)
		})
	})

})

var _ = Describe("Write-Back Cache Integration", func() {

	var (
		engine          akita.Engine
		evictor         *LRUEvictor
		directory       *DirectoryImpl
		mshr            *MSHRImpl
		lowModuleFinder *SingleLowModuleFinder
		storage         *mem.Storage
		cache           *WriteBackCache
		dram            *mem.IdealMemController
		conn            *akita.DirectConnection
		agent           *mockComponent
	)

	BeforeEach(func() {
		agent = newMockComponent()

		engine = akita.NewSerialEngine()
		evictor = NewLRUEvictor()
		directory = NewDirectory(1024, 4, 64, evictor)
		mshr = NewMSHR(4)
		lowModuleFinder = new(SingleLowModuleFinder)
		storage = mem.NewStorage(4 * mem.MB)
		cache = NewWriteBackCache("Cache", engine, directory, mshr,
			lowModuleFinder, storage)
		cache.Freq = 1 * akita.GHz
		cache.Latency = 4

		dram = mem.NewIdealMemController("Dram", engine, 4*mem.GB)
		dram.Freq = 1 * akita.GHz
		dram.Latency = 20
		lowModuleFinder.LowModule = dram.ToTop

		conn = akita.NewDirectConnection(engine)
		conn.PlugIn(cache.ToTop)
		conn.PlugIn(cache.ToBottom)
		conn.PlugIn(dram.ToTop)
		conn.PlugIn(agent.ToMem)
	})

	It("should handle read", func() {
		dram.Storage.Write(5000, []byte{1, 2, 3, 4})

		readReq := mem.NewReadReq(10, agent.ToMem, cache.ToTop, 5000, 4)
		readReq.SetRecvTime(10)

		cache.ToTop.Recv(readReq)
		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(1))
		dataReady := agent.receivedReqs[0].(*mem.DataReadyRsp)
		Expect(dataReady.Data).To(Equal([]byte{1, 2, 3, 4}))
	})

	It("should handle write", func() {
		writeReq := mem.NewWriteReq(8, agent.ToMem, cache.ToTop, 5000)
		writeReq.SetRecvTime(8)
		writeReq.Data = []byte{1, 2, 3, 4}

		cache.ToTop.Recv(writeReq)
		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(1))
		done := agent.receivedReqs[0].(*mem.DoneRsp)
		Expect(done.RespondTo).To(Equal(writeReq.ID))

		readReq := mem.NewReadReq(10, agent.ToMem, cache.ToTop, 5000, 4)
		readReq.SetRecvTime(10)

		cache.ToTop.Recv(readReq)
		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(2))
		dataReady := agent.receivedReqs[1].(*mem.DataReadyRsp)
		Expect(dataReady.Data).To(Equal([]byte{1, 2, 3, 4}))
	})

	//It("should handle read after write", func() {
	//	writeReq := mem.NewWriteReq(8, agent, cache, 5000)
	//	writeReq.SetRecvTime(8)
	//	writeReq.Data = []byte{1, 2, 3, 4}
	//
	//	readReq := mem.NewReadReq(10, agent, cache, 5000, 4)
	//	readReq.SetRecvTime(8)
	//
	//	cache.Recv(writeReq)
	//	cache.Recv(readReq)
	//	engine.Run()
	//
	//	Expect(agent.receivedReqs).To(HaveLen(2))
	//	done := agent.receivedReqs[1].(*mem.DoneRsp)
	//	Expect(done.RespondTo).To(Equal(writeReq.ID))
	//	dataReady := agent.receivedReqs[0].(*mem.DataReadyRsp)
	//	Expect(dataReady.Data).To(Equal([]byte{1, 2, 3, 4}))
	//})

})
