package cache

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

// A Builder can build different types of cache components
type Builder struct {
	Engine          akita.Engine
	LowModuleFinder LowModuleFinder
}

// BuildWriteAroundCache creates a write-around cache
func (b *Builder) BuildWriteAroundCache(
	name string,
	wayAssociativity int,
	byteSize uint64,
	numMSHREntry int,
) *WriteAroundCache {
	blockSize := 64
	evictor := NewLRUEvictor()
	numSet := int(byteSize / uint64(wayAssociativity*blockSize))
	directory := NewDirectory(numSet, wayAssociativity, blockSize, evictor)

	mshr := NewMSHR(numMSHREntry)
	storage := mem.NewStorage(byteSize)

	cache := NewWriteAroundCache(
		name, b.Engine, directory, mshr,
		b.LowModuleFinder, storage)

	return cache
}

//BuildWriteBackCache creates a write-back cache
func (b *Builder) BuildWriteBackCache(
	name string,
	wayAssociativity int,
	byteSize uint64,
	numMSHREntry int,
) *WriteBackCache {
	blockSize := 64
	evictor := NewLRUEvictor()
	numSet := int(byteSize / uint64(wayAssociativity*blockSize))
	directory := NewDirectory(numSet, wayAssociativity, blockSize, evictor)

	mshr := NewMSHR(numMSHREntry)
	storage := mem.NewStorage(byteSize)

	cache := NewWriteBackCache(name, b.Engine, directory, mshr,
		b.LowModuleFinder, storage)

	return cache
}
