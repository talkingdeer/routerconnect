package cache

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

type bankBusyInfo struct {
	req       akita.Req
	block     *Block
	evict     *Block
	cycleLeft int
	mshrEntry *MSHREntry
}

// A WriteAroundCache is a cache that performs the write-through policy
type WriteAroundCache struct {
	*akita.ComponentBase
	ticker *akita.Ticker

	ToTop    akita.Port
	ToBottom akita.Port

	engine          akita.Engine
	dir             Directory
	mshr            MSHR
	LowModuleFinder LowModuleFinder
	storage         *mem.Storage

	Freq akita.Freq

	DirectoryLatency    int
	Latency             int
	NumBank             int
	BlockSizeAsPowerOf2 uint64

	// Parse Stage
	processingWriteReqs []*mem.WriteReq
	processingReqs      map[uint64][]mem.AccessReq

	// Directory Stage
	directoryLookupReq       akita.Req
	directoryLookupCycleLeft int

	// Bank Stage
	bankBusy []bankBusyInfo

	// Fulfill stage
	fulfillStageBusy bool
	fulfillingAddr   uint64
	fulfillingData   []byte

	// Send Stage
	// No need for buffer limitation as it cannot exceed MSHR entries
	readBuffer            []*mem.ReadReq
	pendingReadReqs       map[string]*mem.ReadReq
	writeBuffer           []*mem.WriteReq
	pendingWriteReqs      map[string]*mem.WriteReq
	writeBufferCapacity   int
	upGoingBuffer         []akita.Req
	upGoingBufferCapacity int

	needTick bool
}

func (c *WriteAroundCache) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
	c.ticker.TickLater(now)
}

func (c *WriteAroundCache) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	c.ticker.TickLater(now)
}

func (c *WriteAroundCache) Handle(e akita.Event) error {
	switch evt := e.(type) {
	case akita.TickEvent:
		return c.handleTickEvent(evt)
	case *SetBankCycleLeftToZeroEvent:
		c.bankBusy[evt.bankNum].cycleLeft = 0
		c.ticker.TickLater(evt.Time())
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}
	return nil
}

func (c *WriteAroundCache) handleTickEvent(evt akita.TickEvent) error {
	now := evt.Time()
	c.needTick = false

	c.runSendStage(now)
	c.runFulfillStage(now)
	for i := 0; i < c.NumBank; i++ {
		c.runBankStage(i, now)
	}
	c.runDirectoryStage(now)
	c.runParseStage(now)

	if c.needTick {
		c.ticker.TickLater(now)
	}

	return nil
}

func (c *WriteAroundCache) runParseStage(now akita.VTimeInSec) {
	if c.parseReqFromBottom(now) {
		return
	}
	c.parseReqFromTop(now)
}

func (c *WriteAroundCache) parseReqFromBottom(now akita.VTimeInSec) bool {
	req := c.ToBottom.Peek()
	if req == nil {
		return false
	}

	c.needTick = true
	switch req := req.(type) {
	case *mem.DataReadyRsp:
		read := c.pendingReadReqs[req.RespondTo]
		c.traceMem(now, "recv-data-ready",
			read.Address, read.MemByteSize, req.Data)
		c.processDataReadyRspAfterParsing(now, req)
	case *mem.DoneRsp:
		write := c.pendingWriteReqs[req.RespondTo]
		c.traceMem(now, "recv-done",
			write.Address, uint64(len(write.Data)), write.Data)
		c.processingDoneRspAfterParsing(now, req)
	default:
		log.Panicf("cannot process req of type %s", reflect.TypeOf(req))
	}
	return true
}

func (c *WriteAroundCache) processDataReadyRspAfterParsing(
	now akita.VTimeInSec,
	dataReady *mem.DataReadyRsp,
) {
	readBottom := c.pendingReadReqs[dataReady.RespondTo]
	addr := readBottom.Address
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)

	mshrEntry := c.mshr.Query(cacheLineID)
	block := mshrEntry.Block
	bankNum := bankID(block, c.dir.WayAssociativity(), c.NumBank)
	if c.bankBusy[bankNum].req != nil {
		c.traceMem(now, "data-ready-stall-bank-busy", addr, uint64(len(dataReady.Data)), dataReady.Data)
		return
	}

	block.Tag = cacheLineID
	c.dir.Lookup(block.Tag)

	c.sendToBank(now, bankNum, dataReady, block)
	c.mshr.Remove(cacheLineID)
	c.traceMem(now, "remove-mshr", cacheLineID, c.blockSizeInByte(), nil)

	delete(c.pendingReadReqs, readBottom.ID)
	c.ToBottom.Retrieve(now)
	c.needTick = true
}

func (c *WriteAroundCache) processingDoneRspAfterParsing(
	now akita.VTimeInSec,
	rsp *mem.DoneRsp,
) {
	// FIXME: check upgoing buffer capacity
	writeBottom := c.pendingWriteReqs[rsp.RespondTo]
	address := writeBottom.Address
	cacheLineID, _ := GetCacheLineID(address, c.BlockSizeAsPowerOf2)
	writeFromTop := c.getProcessingWriteReq(address)

	doneRspToTop := mem.NewDoneRsp(now, c.ToTop, writeFromTop.Src(), writeFromTop.ID)
	c.upGoingBuffer = append(c.upGoingBuffer, doneRspToTop)
	c.removeProcessingWriteReq(writeFromTop)
	delete(c.processingReqs, cacheLineID)

	delete(c.pendingWriteReqs, writeBottom.ID)
	c.ToBottom.Retrieve(now)
	c.needTick = true
}

func (c *WriteAroundCache) parseReqFromTop(now akita.VTimeInSec) {
	req := c.ToTop.Peek()
	if req == nil {
		return
	}

	switch req := req.(type) {
	case *mem.ReadReq:
		c.parseReadReq(req, now)
	case *mem.WriteReq:
		c.parseWriteReq(req, now)
	default:
		log.Panicf("cannot process req of type %s", reflect.TypeOf(req))
	}
}

func (c *WriteAroundCache) parseReadReq(req *mem.ReadReq, now akita.VTimeInSec) {
	addr := req.Address

	if c.isWritingSameLine(addr) {
		c.traceMem(now, "parse-read-stall-writing-line",
			addr, req.GetByteSize(), nil)
		return
	}

	if c.isProcessingSameLine(addr) {
		c.addToProcessingList(req)
		c.ToTop.Retrieve(now)
		c.needTick = true
		return
	}

	if c.isDirectoryBusy() {
		c.traceMem(now, "parse-read-stall-directory-busy",
			addr, req.GetByteSize(), nil)
		return
	}

	c.addToProcessingList(req)
	c.sendReqToDirectoryStage(req)
	c.ToTop.Retrieve(now)
	c.traceMem(now, "parse-read-send-to-directory",
		addr, req.GetByteSize(), nil)
}

func (c *WriteAroundCache) parseWriteReq(req *mem.WriteReq, now akita.VTimeInSec) {
	addr := req.Address
	byteSize := req.GetByteSize()
	data := req.Data

	if c.isProcessingSameLine(addr) {
		c.traceMem(now, "parse-write-stall-writing-line",
			addr, byteSize, data)
		return
	}

	if c.isDirectoryBusy() {
		c.traceMem(now, "parse-write-stall-directory-busy",
			addr, byteSize, data)
		return
	}

	c.addToProcessingList(req)
	c.addToProcessingWriteList(req)
	c.sendReqToDirectoryStage(req)
	c.ToTop.Retrieve(now)
}

func (c *WriteAroundCache) runDirectoryStage(now akita.VTimeInSec) {
	if c.directoryLookupReq == nil {
		return
	}

	if c.directoryLookupCycleLeft <= 0 {
		switch req := c.directoryLookupReq.(type) {
		case *mem.ReadReq:
			c.directoryStageRead(now, req)
		case *mem.WriteReq:
			c.directoryStageWrite(now, req)
		default:
			log.Panicf("cannot process req %s", reflect.TypeOf(req))
		}
	} else {
		c.directoryLookupCycleLeft--
		c.needTick = true
	}
}

func (c *WriteAroundCache) directoryStageRead(now akita.VTimeInSec, req *mem.ReadReq) {
	addr := req.Address
	cacheLindID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	block := c.dir.Lookup(cacheLindID)

	if block != nil {
		if block.IsLocked {
			c.traceMem(now, "r-hit-stall-block-locked", addr, req.MemByteSize, nil)
			return
		}
		c.doReadHit(now, block, req)
	} else {
		c.readFromBottom(now, req)
	}
}

func (c *WriteAroundCache) directoryStageWrite(now akita.VTimeInSec, req *mem.WriteReq) {
	addr := req.Address
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)

	if len(c.writeBuffer) >= c.writeBufferCapacity {
		c.traceMem(now, "w-stall-write-buffer-full", addr, uint64(len(req.Data)), req.Data)
		return
	}

	block := c.dir.Lookup(cacheLineID)
	if block != nil {
		bankNum := bankID(block, c.dir.WayAssociativity(), c.NumBank)
		if c.bankBusy[bankNum].req != nil {
			c.traceMem(now, "w-hit-stall-bank-busy", addr, uint64(len(req.Data)), req.Data)
			return
		}

		if block.IsLocked {
			c.traceMem(now, "w-hit-stall-block-locked", addr, uint64(len(req.Data)), req.Data)
			return
		}

		c.traceMem(now, "w-hit", addr, uint64(len(req.Data)), req.Data)
		c.sendToBank(now, bankNum, req, block)
		block.IsLocked = true
		c.traceMem(now, "block-lock-write", block.Tag, c.blockSizeInByte(), nil)
	} else {
		c.traceMem(now, "w-miss", addr, uint64(len(req.Data)), req.Data)
	}

	lowModule := c.LowModuleFinder.Find(addr)
	writeBottom := mem.NewWriteReq(now, c.ToBottom, lowModule, addr)
	writeBottom.Data = req.Data
	c.writeBuffer = append(c.writeBuffer, writeBottom)
	c.pendingWriteReqs[writeBottom.ID] = writeBottom

	c.directoryLookupReq = nil
	c.needTick = true
}

func (c *WriteAroundCache) doReadHit(now akita.VTimeInSec, block *Block, req *mem.ReadReq) {
	addr := req.Address
	byteSize := req.MemByteSize
	bankNum := bankID(block, c.dir.WayAssociativity(), c.NumBank)
	if c.bankBusy[bankNum].req == nil {
		c.traceMem(now, "r-hit", addr, byteSize, nil)
		c.sendToBank(now, bankNum, req, block)
		c.directoryLookupReq = nil
		block.IsLocked = true
		c.traceMem(now, "block-lock-read-hit", block.Tag, c.blockSizeInByte(), nil)
		c.needTick = true
	} else {
		c.traceMem(now, "r-hit-stall-bank-conflict", addr, byteSize, nil)
	}
}

func (c *WriteAroundCache) sendToBank(now akita.VTimeInSec, bankNum int, req akita.Req, block *Block) {
	if c.bankBusy[bankNum].req != nil {
		log.Panic("bank is not available")
	}

	c.bankBusy[bankNum].cycleLeft = c.Latency
	c.bankBusy[bankNum].block = block
	c.bankBusy[bankNum].req = req

	time := c.Freq.NCyclesLater(c.Latency, now)
	evt := NewSetBankCycleLeftToZeroEvent(time, c, bankNum)
	c.engine.Schedule(evt)
}

func (c *WriteAroundCache) readFromBottom(
	now akita.VTimeInSec,
	req *mem.ReadReq,
) {
	addr := req.GetAddress()
	cacheLineID, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)

	mshrEntry := c.mshr.Query(cacheLineID)
	if mshrEntry != nil {
		mshrEntry.Requests = append(mshrEntry.Requests, req)
		c.traceMem(now, "r-miss", addr, req.MemByteSize, nil)
		c.traceMem(now, "insert-mshr-"+req.ID, req.Address, req.MemByteSize, nil)
		c.directoryLookupReq = nil
		c.needTick = true
		return
	}

	if c.mshr.IsFull() {
		c.traceMem(now, "r-miss-stall-mshr-full", addr, req.MemByteSize, nil)
		return
	}

	block := c.dir.Evict(cacheLineID)
	if block.IsLocked {
		c.traceMem(now, "r-miss-stall-block-locked", addr, req.MemByteSize, nil)
		return
	}
	block.IsValid = false
	block.IsLocked = true
	block.Tag = cacheLineID

	mshrEntry = c.mshr.Add(cacheLineID)
	mshrEntry.Requests = append(mshrEntry.Requests, req)
	mshrEntry.Block = block

	c.traceMem(now, "block-lock-read-from-bottom", block.Tag, c.blockSizeInByte(), nil)
	c.traceMem(now, "r-miss", addr, req.MemByteSize, nil)
	c.traceMem(now, "insert-mshr-"+req.ID, req.Address, req.MemByteSize, nil)

	lowModule := c.LowModuleFinder.Find(cacheLineID)
	readBottom := mem.NewReadReq(now, c.ToBottom, lowModule, cacheLineID, c.blockSizeInByte())
	c.readBuffer = append(c.readBuffer, readBottom)
	c.pendingReadReqs[readBottom.ID] = readBottom

	c.directoryLookupReq = nil
	c.needTick = true
}

func (c *WriteAroundCache) runBankStage(bankID int, now akita.VTimeInSec) {
	if c.bankBusy[bankID].req == nil {
		return
	}

	if c.bankBusy[bankID].cycleLeft == 0 {
		switch req := c.bankBusy[bankID].req.(type) {
		case *mem.ReadReq:
			c.completeLocalRead(bankID, now, req)
		case *mem.DataReadyRsp:
			c.completeWriteFromBottom(bankID, now, req)
		case *mem.WriteReq:
			c.completeLocalWrite(bankID, now, req)
		default:
			log.Panicf("cannot process request %s", reflect.TypeOf(req))
		}
	} else {
		c.bankBusy[bankID].cycleLeft--
		c.needTick = true
	}
}

func (c *WriteAroundCache) completeLocalRead(
	bankID int,
	now akita.VTimeInSec,
	req *mem.ReadReq,
) {
	if c.fulfillStageBusy {
		c.traceMem(now, "bank-stall-fulfill-stage-busy",
			req.Address, req.MemByteSize, nil)
		return
	}

	block := c.bankBusy[bankID].block
	data, _ := c.storage.Read(block.CacheAddress, c.blockSizeInByte())

	c.sendToFulfillStage(block.Tag, data)

	c.bankBusy[bankID].req = nil
	block.IsLocked = false
	c.traceMem(now, "block-unlock", block.Tag, c.blockSizeInByte(), nil)
	c.needTick = true

	c.traceMem(now, "read-local-complete",
		block.Tag, c.blockSizeInByte(), data)
}

func (c *WriteAroundCache) sendToFulfillStage(addr uint64, data []byte) {
	if c.fulfillStageBusy {
		log.Panic("fulfill stage not available")
	}

	c.fulfillStageBusy = true
	c.fulfillingAddr = addr
	c.fulfillingData = data
	c.needTick = true
}

func (c *WriteAroundCache) completeWriteFromBottom(
	bankID int,
	now akita.VTimeInSec,
	dataReady *mem.DataReadyRsp,
) {
	block := c.bankBusy[bankID].block
	if c.fulfillStageBusy {
		c.traceMem(now, "bank-stall-fulfill-stage-busy",
			block.Tag, uint64(len(dataReady.Data)), dataReady.Data)
		return
	}

	//c.combineProcessingWriteWithDataReady(now, mshrEntry)

	c.storage.Write(block.CacheAddress, dataReady.Data)
	block.IsValid = true
	block.IsLocked = false
	c.traceMem(now, "block-unlock", block.Tag, c.blockSizeInByte(), nil)
	c.bankBusy[bankID].req = nil
	c.sendToFulfillStage(block.Tag, dataReady.Data)

	c.traceMem(now, "write-from-bottom-complete",
		block.Tag, c.blockSizeInByte(), dataReady.Data)
	c.needTick = true
}

func (c *WriteAroundCache) combineProcessingWriteWithDataReady(
	now akita.VTimeInSec, entry *MSHREntry,
) {
	for _, req := range c.processingWriteReqs {
		address := req.Address
		cacheLineID, offset := GetCacheLineID(address, c.BlockSizeAsPowerOf2)
		block := entry.Block
		if block.Tag == cacheLineID {
			copy(entry.Data[offset:], req.Data)
			c.traceMem(now, "use write-buffer-data",
				block.Tag+offset, uint64(len(req.Data)), entry.Data)
		}
	}
}

func (c *WriteAroundCache) completeLocalWrite(
	bankID int,
	now akita.VTimeInSec,
	write *mem.WriteReq,
) {
	block := c.bankBusy[bankID].block
	_, offset := GetCacheLineID(write.Address, c.BlockSizeAsPowerOf2)
	c.storage.Write(block.CacheAddress+offset, write.Data)
	c.bankBusy[bankID].req = nil
	c.traceMem(now, "write-local-done", write.Address, uint64(len(write.Data)), write.Data)
	block.IsLocked = false
	c.traceMem(now, "block-unlock", block.Tag, c.blockSizeInByte(), nil)
	c.needTick = true
}

func (c *WriteAroundCache) runFulfillStage(now akita.VTimeInSec) {
	if !c.fulfillStageBusy {
		return
	}

	if len(c.processingReqs[c.fulfillingAddr]) == 0 {
		c.fulfillStageBusy = false
		delete(c.processingReqs, c.fulfillingAddr)
		c.traceMem(now, "fulfill-remove-processing-req-addr",
			c.fulfillingAddr, c.blockSizeInByte(), nil)
		return
	}

	if len(c.upGoingBuffer) >= c.upGoingBufferCapacity {
		c.traceMem(now, "fulfill-stall-up-going-buffer-full",
			c.fulfillingAddr, c.blockSizeInByte(), c.fulfillingData)
		return
	}

	switch req := c.processingReqs[c.fulfillingAddr][0].(type) {
	case *mem.ReadReq:
		byteSize := req.MemByteSize
		addr := req.Address
		_, offset := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
		dataReady := mem.NewDataReadyRsp(now, c.ToTop, req.Src(), req.ID)
		dataReady.Data = c.fulfillingData[offset : offset+byteSize]
		c.upGoingBuffer = append(c.upGoingBuffer, dataReady)
		c.processingReqs[c.fulfillingAddr] =
			c.processingReqs[c.fulfillingAddr][1:]
		c.traceMem(now, "fulfill-read-send-up", addr, byteSize, dataReady.Data)
	default:
		log.Panicf("cannot fulfill request of type %s", reflect.TypeOf(req))
	}

	c.needTick = true
}

func (c *WriteAroundCache) runSendStage(now akita.VTimeInSec) {
	if len(c.upGoingBuffer) > 0 {
		req := c.upGoingBuffer[0]
		req.SetSendTime(now)
		err := c.ToTop.Send(req)
		if err == nil {
			switch req := req.(type) {
			case *mem.DataReadyRsp:
				c.traceMem(now, "rsp-data-ready-"+req.RespondTo, 0, 0, req.Data)
			case *mem.DoneRsp:
				c.traceMem(now, "rsp-done-"+req.RespondTo, 0, 0, nil)
			}
			c.upGoingBuffer = c.upGoingBuffer[1:]
			c.needTick = true
		}
		return
	}

	if len(c.writeBuffer) > 0 {
		req := c.writeBuffer[0]
		req.SetSendTime(now)
		err := c.ToBottom.Send(req)
		if err == nil {
			c.writeBuffer = c.writeBuffer[1:]
			c.needTick = true
		}
		return
	}

	if len(c.readBuffer) > 0 {
		req := c.readBuffer[0]
		req.SetSendTime(now)
		err := c.ToBottom.Send(req)
		if err == nil {
			c.readBuffer = c.readBuffer[1:]
			c.needTick = true
		}

		return
	}
}

func (c *WriteAroundCache) blockSizeInByte() uint64 {
	return 1 << c.BlockSizeAsPowerOf2
}

func (c *WriteAroundCache) traceMem(
	now akita.VTimeInSec,
	what string,
	addr uint64, byteSize uint64,
	data []byte,
) {
	traceInfo := mem.TraceInfo{
		Where:    c.Name(),
		When:     now,
		What:     what,
		Address:  addr,
		ByteSize: byteSize,
		Data:     data,
	}

	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Item:   traceInfo,
	}

	c.InvokeHook(&ctx)
}

func (c *WriteAroundCache) Reset() {
	c.dir.Reset()
}

func (c *WriteAroundCache) SetNumBanks(n int) {
	c.NumBank = n
	c.bankBusy = make([]bankBusyInfo, n)
}

func (c *WriteAroundCache) isDirectoryBusy() bool {
	return c.directoryLookupReq != nil
}

func (c *WriteAroundCache) isWritingSameLine(addr uint64) bool {
	inputLine, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	for _, write := range c.processingWriteReqs {
		line, _ := GetCacheLineID(write.Address, c.BlockSizeAsPowerOf2)
		if line == inputLine {
			return true
		}
	}
	return false
}

func (c *WriteAroundCache) isProcessingSameLine(addr uint64) bool {
	inputLine, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	if c.processingReqs[inputLine] != nil {
		return true
	}
	return false
}

func (c *WriteAroundCache) addToProcessingList(req mem.AccessReq) {
	addr := req.GetAddress()
	line, _ := GetCacheLineID(addr, c.BlockSizeAsPowerOf2)
	c.processingReqs[line] = append(c.processingReqs[line], req)
}

func (c *WriteAroundCache) sendReqToDirectoryStage(req akita.Req) {
	if c.directoryLookupReq != nil {
		log.Panic("directory stage not available")
	}

	c.directoryLookupReq = req
	c.directoryLookupCycleLeft = c.DirectoryLatency
	c.needTick = true
}

func (c *WriteAroundCache) addToProcessingWriteList(req *mem.WriteReq) {
	c.processingWriteReqs = append(c.processingWriteReqs, req)
}

func (c *WriteAroundCache) getProcessingWriteReq(address uint64) *mem.WriteReq {
	for _, req := range c.processingWriteReqs {
		if req.Address == address {
			return req
		}
	}
	log.Panic("Cannot find incoming write request")
	return nil
}

func (c *WriteAroundCache) removeProcessingWriteReq(write *mem.WriteReq) {
	var i int
	var req *mem.WriteReq
	for i, req = range c.processingWriteReqs {
		if req == write {
			c.processingWriteReqs = append(c.processingWriteReqs[:i],
				c.processingWriteReqs[i+1:]...)
			return
		}
	}

	log.Panic("Cannot find incoming write request")
}

// NewWriteAroundCache creates a new write-through cache,
// injecting the dependency of the engine, the directory and the storage.
func NewWriteAroundCache(
	name string,
	engine akita.Engine,
	directory Directory,
	mshr MSHR,
	lowModuleFinder LowModuleFinder,
	storage *mem.Storage,
) *WriteAroundCache {
	cache := new(WriteAroundCache)
	cache.ComponentBase = akita.NewComponentBase(name)
	cache.ticker = akita.NewTicker(cache, engine, 1*akita.GHz)

	cache.ToTop = akita.NewLimitNumReqPort(cache, 1)
	cache.ToBottom = akita.NewLimitNumReqPort(cache, 1)

	cache.engine = engine
	cache.dir = directory
	cache.mshr = mshr
	cache.LowModuleFinder = lowModuleFinder
	cache.storage = storage

	cache.DirectoryLatency = 2
	cache.Latency = 10
	cache.Freq = 1 * akita.GHz
	cache.NumBank = 4
	cache.bankBusy = make([]bankBusyInfo, cache.NumBank)
	cache.BlockSizeAsPowerOf2 = 6
	cache.processingReqs = make(map[uint64][]mem.AccessReq)

	cache.readBuffer = make([]*mem.ReadReq, 0)
	cache.pendingReadReqs = make(map[string]*mem.ReadReq)
	cache.writeBuffer = make([]*mem.WriteReq, 0)
	cache.pendingWriteReqs = make(map[string]*mem.WriteReq)
	cache.writeBufferCapacity = 409600
	cache.upGoingBuffer = make([]akita.Req, 0)
	cache.upGoingBufferCapacity = 409600
	cache.processingWriteReqs = make([]*mem.WriteReq, 0)

	return cache
}
