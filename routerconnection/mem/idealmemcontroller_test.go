package mem

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	// . "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/akita/mock_akita"
)

var _ = Describe("IdealMemController", func() {

	var (
		mockCtrl      *gomock.Controller
		engine        *mock_akita.MockEngine
		memController *IdealMemController
		port          *mock_akita.MockPort
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())

		engine = mock_akita.NewMockEngine(mockCtrl)
		port = mock_akita.NewMockPort(mockCtrl)

		memController = NewIdealMemController("MemCtrl", engine, 1*MB)
		memController.Freq = 1000 * akita.MHz
		memController.Latency = 10
		memController.ToTop = port
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should process read request", func() {
		readReq := NewReadReq(10, nil, memController.ToTop, 0, 4)

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&readRespondEvent{}))

		memController.Handle(readReq)
	})

	It("should process write request", func() {
		writeReq := NewWriteReq(10, nil, memController.ToTop, 0)
		writeReq.Data = []byte{0, 1, 2, 3}

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&writeRespondEvent{}))

		memController.Handle(writeReq)

	})

	It("should handle read respond event", func() {
		data := []byte{1, 2, 3, 4}
		memController.Storage.Write(0, data)

		readReq := NewReadReq(10, nil, memController.ToTop, 0, 4)
		event := newReadRespondEvent(11, memController, readReq)

		port.EXPECT().Send(gomock.AssignableToTypeOf(&DataReadyRsp{}))

		memController.Handle(event)
	})

	It("should retry read if send DataReady failed", func() {
		data := []byte{1, 2, 3, 4}
		memController.Storage.Write(0, data)
		readReq := NewReadReq(10, nil, memController.ToTop, 0, 4)
		event := newReadRespondEvent(11, memController, readReq)

		port.EXPECT().
			Send(gomock.AssignableToTypeOf(&DataReadyRsp{})).
			Return(&akita.SendError{})

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&readRespondEvent{}))

		memController.Handle(event)
	})

	It("should handle write respond event", func() {
		data := []byte{1, 2, 3, 4}
		writeReq := NewWriteReq(10, nil, memController.ToTop, 0)
		writeReq.Data = data
		event := newWriteRespondEvent(11, memController, writeReq)

		port.EXPECT().Send(gomock.AssignableToTypeOf(&DoneRsp{}))

		memController.Handle(event)
	})

	It("should retry write respond event, if network busy", func() {
		data := []byte{1, 2, 3, 4}
		writeReq := NewWriteReq(10, nil, memController.ToTop, 0)
		writeReq.Data = data
		event := newWriteRespondEvent(11, memController, writeReq)

		port.EXPECT().
			Send(gomock.AssignableToTypeOf(&DoneRsp{})).
			Return(&akita.SendError{})
		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&writeRespondEvent{}))

		memController.Handle(event)
	})
})
