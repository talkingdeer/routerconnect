package main

import (
	"fmt"
	"math/rand"

	"time"

	"flag"

	"os"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/acceptancetests"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/mem/trace"
)

var seedFlag = flag.Int64("seed", 0, "Random Seed")
var numAccessFlag = flag.Int("num-access", 100000, "Number of accesses to generate")
var maxAddressFlag = flag.Uint64("max-address", 1048576, "Address range to use")
var traceFileFlag = flag.String("trace", "", "Trace file")
var traceWithStdoutFlag = flag.Bool("trace-stdout", false, "Trace with stdout")
var parallelFlag = flag.Bool("parallel", false, "Test with parallel engine")

func main() {
	flag.Parse()

	var seed int64
	if *seedFlag == 0 {
		seed = time.Now().UnixNano()
	} else {
		seed = *seedFlag
	}
	fmt.Fprintf(os.Stderr, "Seed %d\n", seed)
	rand.Seed(seed)

	var engine akita.Engine
	if *parallelFlag {
		engine = akita.NewParallelEngine()
	} else {
		engine = akita.NewSerialEngine()
	}
	//engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

	conn := akita.NewDirectConnection(engine)

	agent := acceptancetests.NewMemAccessAgent(engine)
	agent.MaxAddress = *maxAddressFlag
	agent.WriteLeft = *numAccessFlag
	agent.ReadLeft = *numAccessFlag

	builder := new(cache.Builder)
	builder.Engine = engine

	lowModuleFinderForL1 := new(cache.SingleLowModuleFinder)
	lowModuleFinderForL2 := new(cache.SingleLowModuleFinder)

	builder.LowModuleFinder = lowModuleFinderForL1
	l1Cache := builder.BuildWriteAroundCache("L1", 4, 16*mem.KB, 4)
	l1Cache.DirectoryLatency = 0
	l1Cache.Latency = 4
	l1Cache.SetNumBanks(4)

	builder.LowModuleFinder = lowModuleFinderForL2
	l2Cache := builder.BuildWriteAroundCache("L2", 16, 128*mem.MB, 4)
	l2Cache.DirectoryLatency = 2
	l2Cache.Latency = 20
	l2Cache.SetNumBanks(16)

	if *traceWithStdoutFlag {
		tracer := trace.NewTracer(os.Stdout)
		l1Cache.AcceptHook(tracer)
		l2Cache.AcceptHook(tracer)
	} else if *traceFileFlag != "" {
		traceFile, _ := os.Create(*traceFileFlag)
		tracer := trace.NewTracer(traceFile)
		l1Cache.AcceptHook(tracer)
		l2Cache.AcceptHook(tracer)
	}

	if *traceFileFlag != "" {
		traceFile, _ := os.Create(*traceFileFlag)
		tracer := trace.NewTracer(traceFile)
		l1Cache.AcceptHook(tracer)
		l2Cache.AcceptHook(tracer)
	}

	dram := mem.NewIdealMemController("DRAM", engine, 4*mem.GB)
	dram.Latency = 100

	lowModuleFinderForL1.LowModule = l2Cache.ToTop
	lowModuleFinderForL2.LowModule = dram.ToTop
	agent.LowModule = l1Cache.ToTop

	conn.PlugIn(agent.ToMem)
	conn.PlugIn(l1Cache.ToBottom)
	conn.PlugIn(l1Cache.ToTop)
	conn.PlugIn(l2Cache.ToBottom)
	conn.PlugIn(l2Cache.ToTop)
	conn.PlugIn(dram.ToTop)

	agent.TickLater(0)
	engine.Run()

	if len(agent.PendingWriteReq) > 0 || len(agent.PendingReadReq) > 0 {
		panic("Not all req returned")
	}
}
