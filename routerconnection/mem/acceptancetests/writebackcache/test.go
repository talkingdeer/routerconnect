package main

import (
	"fmt"
	"math/rand"

	"time"

	"flag"

	"os"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/acceptancetests"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/mem/trace"
)

var seedFlag = flag.Int64("seed", 0, "Random Seed")
var numAccessFlag = flag.Int("num-access", 100000, "Number of accesses to generate")
var maxAddressFlag = flag.Uint64("max-address", 1048576, "Address range to use")
var traceFileFlag = flag.String("trace", "", "Trace file")
var traceWithStdoutFlag = flag.Bool("trace-stdout", false, "Trace with stdout")
var parallelFlag = flag.Bool("parallel", false, "Test with parallel engine")

func main() {
	flag.Parse()

	var seed int64
	if *seedFlag == 0 {
		seed = time.Now().UnixNano()
	} else {
		seed = *seedFlag
	}
	fmt.Fprintf(os.Stderr, "Seed %d\n", seed)
	rand.Seed(seed)

	var engine akita.Engine
	if *parallelFlag {
		engine = akita.NewParallelEngine()
	} else {
		engine = akita.NewSerialEngine()
	}
	//engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

	conn := akita.NewDirectConnection(engine)

	agent := acceptancetests.NewMemAccessAgent(engine)
	agent.MaxAddress = *maxAddressFlag
	agent.WriteLeft = *numAccessFlag
	agent.ReadLeft = *numAccessFlag

	builder := new(cache.Builder)
	builder.Engine = engine
	lowModuleFinder := new(cache.SingleLowModuleFinder)
	builder.LowModuleFinder = lowModuleFinder
	writeBackCache := builder.BuildWriteBackCache("Cache", 4, 16*mem.KB, 4)
	writeBackCache.NumBank = 4

	if *traceWithStdoutFlag {
		tracer := trace.NewTracer(os.Stdout)
		writeBackCache.AcceptHook(tracer)
	} else if *traceFileFlag != "" {
		traceFile, _ := os.Create(*traceFileFlag)
		tracer := trace.NewTracer(traceFile)
		writeBackCache.AcceptHook(tracer)
	}

	dram := mem.NewIdealMemController("DRAM", engine, 4*mem.GB)
	lowModuleFinder.LowModule = dram.ToTop

	agent.LowModule = writeBackCache.ToTop

	conn.PlugIn(agent.ToMem)
	conn.PlugIn(writeBackCache.ToBottom)
	conn.PlugIn(writeBackCache.ToTop)
	conn.PlugIn(dram.ToTop)

	agent.TickLater(0)
	engine.Run()

	if len(agent.PendingWriteReq) > 0 || len(agent.PendingReadReq) > 0 {
		panic("Not all req returned")
	}
}
