package vm

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
)

type TLBSet struct {
	Blocks   []*Page
	LRUQueue []*Page
}

// A TLB is a cache that maintains some page information.
type TLB struct {
	*akita.TickingComponent

	ToTop    akita.Port
	ToBottom akita.Port
	ToCP     akita.Port

	LowModule akita.Port

	NumSets  uint64
	NumWays  uint64
	PageSize uint64 // A TLB can support different page size, but this field helps find set.

	Latency          int
	ShootdownLatency int

	Sets []*TLBSet

	translating                *TranslationReq
	inReadWriteStage           akita.Req
	inCPReqProcessingStage     akita.Req
	readWriteStageCycleLeft    int
	cpReqProcessStageCycleLeft int
	toSendToBottom             *TranslationReq
	toSendToTop                *TranslateReadyRsp
	toSendToCP                 *InvalidationCompleteRsp

	isBusy bool

	gpuID uint64
}

func (tlb *TLB) Reset() {
	tlb.Sets = make([]*TLBSet, tlb.NumSets)
	for i := 0; i < int(tlb.NumSets); i++ {
		set := &TLBSet{}
		tlb.Sets[i] = set
		set.Blocks = make([]*Page, tlb.NumWays)
		set.LRUQueue = make([]*Page, tlb.NumWays)
		for j := 0; j < int(tlb.NumWays); j++ {
			page := &Page{}
			set.Blocks[j] = page
			set.LRUQueue[j] = page
		}
	}
}

// Handle defines how a TLB handles events
func (tlb *TLB) Handle(e akita.Event) error {
	tlb.Lock()

	switch evt := e.(type) {
	case akita.TickEvent:
		tlb.handleTickEvent(evt)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}

	tlb.Unlock()
	return nil
}

func (tlb *TLB) handleTickEvent(tick akita.TickEvent) {
	now := tick.Time()
	tlb.NeedTick = false

	tlb.sendToTop(now)
	tlb.sendToBottom(now)
	tlb.readOrWrite(now)
	tlb.parseFromBottom(now)
	tlb.parseFromTop(now)
	tlb.parseFromCP(now)
	tlb.processRequestFromCP(now)
	tlb.sendToCP(now)

	if tlb.NeedTick {
		tlb.TickLater(now)
	}
}

func (tlb *TLB) sendToTop(now akita.VTimeInSec) {
	if tlb.toSendToTop == nil {
		return
	}

	tlb.toSendToTop.SetSendTime(now)
	sendErr := tlb.ToTop.Send(tlb.toSendToTop)
	if sendErr == nil {
		tlb.trace(now, "send-top")
		tlb.toSendToTop = nil
		tlb.NeedTick = true
	}
}

func (tlb *TLB) sendToBottom(now akita.VTimeInSec) {
	if tlb.toSendToBottom == nil {
		return
	}

	tlb.toSendToBottom.SetSendTime(now)
	sendErr := tlb.ToBottom.Send(tlb.toSendToBottom)
	if sendErr == nil {
		tlb.trace(now, "send-bottom")
		tlb.toSendToBottom = nil
		tlb.NeedTick = true
	}
}

func (tlb *TLB) sendToCP(now akita.VTimeInSec) {
	if tlb.toSendToCP == nil {
		return
	}

	tlb.toSendToCP.SetSendTime(now)
	sendErr := tlb.ToCP.Send(tlb.toSendToCP)
	if sendErr == nil {
		tlb.trace(now, "send-CP")
		tlb.toSendToCP = nil
		tlb.NeedTick = true
	}
}

func (tlb *TLB) readOrWrite(now akita.VTimeInSec) {
	if tlb.inReadWriteStage == nil {
		return
	}

	if tlb.readWriteStageCycleLeft <= 0 {
		switch req := tlb.inReadWriteStage.(type) {
		case *TranslationReq:
			tlb.doLocalRead(now, req)
			return
		case *TranslateReadyRsp:
			tlb.updateTLBEntry(now, req)
			return
		default:
			log.Panicf("cannot handle request of type %s", reflect.TypeOf(req))
		}
	}

	tlb.readWriteStageCycleLeft--
	tlb.NeedTick = true

}

func (tlb *TLB) processRequestFromCP(now akita.VTimeInSec) {
	if tlb.inCPReqProcessingStage == nil {
		return
	}

	//TODO: Do we need cycle stages?
	if tlb.cpReqProcessStageCycleLeft <= 0 {
		switch req := tlb.inCPReqProcessingStage.(type) {
		case *PTEInvalidationReq:
			tlb.doTLBShootdown(now, req)
			return
		default:
			log.Panicf("cannot handle request of type %s from CP", reflect.TypeOf(req))
		}
	}
	tlb.cpReqProcessStageCycleLeft--
	tlb.NeedTick = true
}

func (tlb *TLB) parseFromBottom(now akita.VTimeInSec) {
	if tlb.inReadWriteStage != nil {
		return
	}

	req := tlb.ToBottom.Retrieve(now)
	if req == nil {
		return
	}

	tlb.trace(now, "recv-rsp")

	tlb.inReadWriteStage = req
	tlb.readWriteStageCycleLeft = tlb.Latency
	tlb.NeedTick = true
}

func (tlb *TLB) parseFromCP(now akita.VTimeInSec) {
	//TODO: At this point there should not be any outstanding requests to the TLB. Assert

	req := tlb.ToCP.Retrieve(now)
	if req == nil {
		return
	}

	tlb.trace(now, "recv-shootdown-req")
	tlb.inCPReqProcessingStage = req
	tlb.cpReqProcessStageCycleLeft = tlb.ShootdownLatency

	tlb.NeedTick = true
}

func (tlb *TLB) parseFromTop(now akita.VTimeInSec) {
	if tlb.translating != nil {
		return
	}

	req := tlb.ToTop.Retrieve(now)
	if req == nil {
		return
	}

	tlb.trace(now, "recv-req")

	tlb.inReadWriteStage = req
	tlb.readWriteStageCycleLeft = tlb.Latency
	tlb.translating = req.(*TranslationReq)
	tlb.NeedTick = true
}

func (tlb *TLB) doTLBShootdown(now akita.VTimeInSec, req *PTEInvalidationReq) {
	vAddr := req.Vaddr
	pid := req.PID

	if tlb.toSendToCP != nil {
		return
	}
	tlb.invalidatePTEs(vAddr, pid)

	tlb.trace(now, "TLB Shootdown")

	respondToCP := NewInvalidationCompleteRsp(now, tlb.ToCP, req.Src(), req.ID)
	respondToCP.InvalidationDone = true
	tlb.toSendToCP = respondToCP
	tlb.NeedTick = true
	tlb.inCPReqProcessingStage = nil

}

func (tlb *TLB) invalidatePTEs(vAddr []uint64, pid PID) {
	for i := 0; i < len(vAddr); i++ {
		setID := tlb.GetSetID(vAddr[i])
		set := tlb.Sets[setID]
		for _, page := range set.Blocks {
			if page.PID == pid &&
				vAddr[i] >= page.VAddr &&
				vAddr[i] < page.VAddr+page.PageSize {
				page.Valid = false
			}
		}

	}

}

func (tlb *TLB) doLocalRead(now akita.VTimeInSec, req *TranslationReq) {
	page, set := tlb.FindPage(req.PID, req.VAddr)
	if page == nil {
		tlb.doTLBMiss(now, req)
		return
	}

	tlb.doTLBHit(now, req, page, set)
}

func (tlb *TLB) doTLBMiss(now akita.VTimeInSec, req *TranslationReq) {
	if tlb.toSendToBottom != nil {
		return
	}

	tlb.trace(now, "tlb-miss")

	reqToBottom := NewTranslateReq(now,
		tlb.ToBottom, tlb.LowModule, req.PID, req.VAddr, req.GPUID)
	tlb.toSendToBottom = reqToBottom
	tlb.NeedTick = true
	tlb.inReadWriteStage = nil
}

func (tlb *TLB) doTLBHit(
	now akita.VTimeInSec,
	req *TranslationReq,
	page *Page,
	set *TLBSet,
) {
	if tlb.toSendToTop != nil {
		return
	}

	tlb.trace(now, "tlb-hit")

	rsp := NewTranslateReadyRsp(now, tlb.ToTop, req.Src(), req.ID)
	rsp.Page = page
	tlb.toSendToTop = rsp
	tlb.NeedTick = true
	tlb.inReadWriteStage = nil
	tlb.translating = nil
	tlb.visitPage(page, set)
}

func (tlb *TLB) updateTLBEntry(
	now akita.VTimeInSec,
	rsp *TranslateReadyRsp,
) {
	if tlb.toSendToTop != nil {
		return
	}

	page := &Page{
		PID:      rsp.Page.PID,
		VAddr:    rsp.Page.VAddr,
		PAddr:    rsp.Page.PAddr,
		PageSize: rsp.Page.PageSize,
		Valid:    true,
		GPUID:    uint64(rsp.Page.GPUID),
	}

	_, set := tlb.FindPage(page.PID, page.VAddr)
	tlb.insertPage(page, set)

	tlb.trace(now, "write")

	rspToTop := NewTranslateReadyRsp(
		now,
		tlb.ToTop,
		tlb.translating.Src(),
		tlb.translating.GetID(),
	)
	rspToTop.Page = page
	tlb.inReadWriteStage = nil
	tlb.toSendToTop = rspToTop
	tlb.translating = nil
	tlb.NeedTick = true
}

func (tlb *TLB) trace(now akita.VTimeInSec, what string) {
	ctx := akita.HookCtx{
		Now:    now,
		Domain: tlb,
		Item:   what,
	}
	tlb.InvokeHook(&ctx)
}

func (tlb *TLB) insertPage(page *Page, set *TLBSet) {
	for i, p := range set.Blocks {
		if p == set.LRUQueue[tlb.NumWays-1] {
			set.Blocks[i] = page
			tlb.visitPage(page, set)
			return
		}
	}
	panic("last in LRU queue is not in set")
}

func (tlb *TLB) visitPage(page *Page, set *TLBSet) {
	newLRUQueue := make([]*Page, 0, tlb.NumWays)

	count := uint64(0)
	for _, p := range set.Blocks {
		if p != page {
			newLRUQueue = append(newLRUQueue, p)
			count++
		}
	}

	if count == tlb.NumWays {
		panic("page not found")
	}

	newLRUQueue = append(newLRUQueue, page)
	set.LRUQueue = newLRUQueue
}

func (tlb *TLB) FindPage(pID PID, vAddr uint64) (*Page, *TLBSet) {
	setID := tlb.GetSetID(vAddr)
	set := tlb.Sets[setID]
	for _, page := range set.Blocks {
		if page.PID == pID &&
			vAddr >= page.VAddr &&
			vAddr < page.VAddr+page.PageSize && page.Valid {
			return page, set
		}
	}
	return nil, set
}

func (tlb *TLB) GetSetID(vAddr uint64) uint64 {
	setID := uint64(vAddr / tlb.PageSize % tlb.NumSets)
	return setID
}

func (tlb *TLB) FullInvalidate(pID PID) error {

	for _, set := range tlb.Sets {
		for _, page := range set.Blocks {
			if page.PID == pID {
				page.Valid = false
			}
		}
	}

	return nil
}

func (tlb *TLB) PartialInvalidate(vAddr []uint64, pID PID) error {

	for i := 0; i < len(vAddr); i++ {
		setID := tlb.GetSetID(vAddr[i])
		set := tlb.Sets[setID]
		for _, page := range set.Blocks {
			if page.PID == pID &&
				vAddr[i] >= page.VAddr &&
				vAddr[i] < page.VAddr+page.PageSize {
				page.Valid = false
			}
		}

	}
	return nil
}

// NewTLB creates ane returns a new TLB component
func NewTLB(
	name string,
	engine akita.Engine,
) *TLB {
	tlb := new(TLB)
	tlb.TickingComponent =
		akita.NewTickingComponent(name, engine, 1*akita.GHz, tlb)

	tlb.Latency = 1
	tlb.ShootdownLatency = 1

	tlb.PageSize = 4096
	tlb.NumSets = 1024
	tlb.NumWays = 4

	tlb.Reset()

	tlb.isBusy = false

	tlb.ToTop = akita.NewLimitNumReqPort(tlb, 4)
	tlb.ToBottom = akita.NewLimitNumReqPort(tlb, 4)
	tlb.ToCP = akita.NewLimitNumReqPort(tlb, 1)

	return tlb

}
