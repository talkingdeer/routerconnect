// Package vm provides the models for address translations
package vm

import (
	"gitlab.com/akita/akita"
)

// A TraslationReq asks the receiver component to translate the request.
type TranslationReq struct {
	*akita.ReqBase

	VAddr uint64
	PID   PID

	GPUID uint64
}

// A TraslationReadyRsp is the respond for a TranslationReq. It carries the
// physical address.
type TranslateReadyRsp struct {
	*akita.ReqBase

	RespondTo string // The ID of the request it replies
	Page      *Page
}

//A TLBFullInvalidate asks the receiver TLB to invalidate all its entries

type PTEInvalidationReq struct {
	*akita.ReqBase
	PID   PID
	Vaddr []uint64
}

type InvalidationCompleteRsp struct {
	*akita.ReqBase

	RespondTo        string // The ID of the request it replies
	InvalidationDone bool
}

type TLBFullInvalidateReq struct {
	*akita.ReqBase
	PID PID
}

//A TLBPartialInvalidate asks the receiver TLB to invalidate the list of particular entries.
// The list can consist anyhere from 0 to N entries
type TLBPartialInvalidateReq struct {
	*akita.ReqBase

	VAddr []uint64
	PID   PID
}

func NewTranslateReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	pid PID,
	vAddr uint64,
	GPUID uint64,
) *TranslationReq {
	reqBase := akita.NewReqBase()
	req := new(TranslationReq)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.VAddr = vAddr
	req.PID = pid

	req.GPUID = GPUID

	return req
}

func NewTranslateReadyRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	respondTo string,
) *TranslateReadyRsp {
	reqBase := akita.NewReqBase()
	req := new(TranslateReadyRsp)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.RespondTo = respondTo

	return req
}

func NewInvalidationCompleteRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	respondTo string,
) *InvalidationCompleteRsp {
	reqBase := akita.NewReqBase()
	req := new(InvalidationCompleteRsp)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.RespondTo = respondTo

	return req
}

func NewFullTLBInvalidationReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	pid PID,
) *TLBFullInvalidateReq {
	reqBase := akita.NewReqBase()
	req := new(TLBFullInvalidateReq)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.PID = pid

	return req
}

func NewPartialTLBInvalidationReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	pid PID,
	vAddr []uint64,
) *TLBPartialInvalidateReq {
	reqBase := akita.NewReqBase()
	req := new(TLBPartialInvalidateReq)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.VAddr = vAddr
	req.PID = pid

	return req
}

func NewPTEInvalidationReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	pid PID,
	vAddr []uint64,
) *PTEInvalidationReq {
	reqBase := akita.NewReqBase()
	req := new(PTEInvalidationReq)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.Vaddr = vAddr
	req.PID = pid

	return req
}
