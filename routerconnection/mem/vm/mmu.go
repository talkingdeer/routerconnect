package vm

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
)

// PID defined the ID of a process. A process maintains its own virtual
// address space.
type PID uint8

//go:generate mockgen -destination mock_vm/mmu.go gitlab.com/akita/mem/vm MMU

// An MMU interface is the interface exposed to the driver and the emulator to
// allocate memory spaces and translate from virtual address to physical
// address.
type MMU interface {
	CreatePage(page *Page)
	RemovePage(pid PID, addr uint64)
	Translate(pid PID, vAddr uint64) (uint64, *Page)
	GetOrCreatePageTable(pid PID) PageTable
}

type inPipelineTranslationReqStatus struct {
	transaction *TranslationReq
	CycleLeft   int
}

// MMUImpl is the default mmu implementation. It is also an akita Component.
type MMUImpl struct {
	*akita.TickingComponent

	pageTableFactory PageTableFactory

	ToTop akita.Port
	ToCP  akita.Port

	Latency          int
	ShootdownLatency int

	isInvalidating      bool
	maxRequestsInFlight uint64
	numRequestsInFlight uint64
	PageTables          map[PID]PageTable

	inPipelineTranslation      []*inPipelineTranslationReqStatus
	postPipelineBuf            []*TranslationReq
	inCPReqProcessingStage     akita.Req
	readWriteStageCycleLeft    int
	cpReqProcessStageCycleLeft int
	toSendToTop                []*TranslateReadyRsp
	toSendToCP                 *InvalidationCompleteRsp
}

// CreatePage add a page to the page table.
func (mmu *MMUImpl) CreatePage(page *Page) {
	table := mmu.GetOrCreatePageTable(page.PID)
	table.InsertPage(page)
}

// RemovePage deletes the page contains the target VAddr
func (mmu *MMUImpl) RemovePage(pid PID, vAddr uint64) {
	table := mmu.GetOrCreatePageTable(pid)
	table.RemovePage(vAddr)
}

// Translate convert virtual address to physical address.
func (mmu *MMUImpl) Translate(pid PID, vAddr uint64) (uint64, *Page) {
	table := mmu.GetOrCreatePageTable(pid)
	page := table.FindPage(vAddr)
	if page == nil || page.Valid == false {
		return 0, nil
	}

	offset := vAddr % page.PageSize
	pAddr := page.PAddr + offset
	return pAddr, page
}

// Handle processes the event scheduled on the MMU component
func (mmu *MMUImpl) Handle(e akita.Event) error {
	mmu.Lock()

	switch evt := e.(type) {
	case akita.TickEvent:
		mmu.handleTickEvent(evt)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}

	mmu.Unlock()
	return nil
}

func (mmu *MMUImpl) handleTickEvent(evt akita.TickEvent) {
	now := evt.Time()
	mmu.NeedTick = false

	mmu.sendToTop(now)
	mmu.sendToCP(now)
	mmu.processPostPipelineBuf(now)
	mmu.processRequestFromCP(now)
	mmu.countDownPipeline(now)
	mmu.parseFromTop(now)
	mmu.parseFromCP(now)

	if mmu.NeedTick {
		mmu.TickLater(now)
	}
}

func (mmu *MMUImpl) trace(now akita.VTimeInSec, what string) {
	ctx := akita.HookCtx{
		Domain: mmu,
		Now:    now,
		Item:   what,
	}

	mmu.InvokeHook(&ctx)
}

func (mmu *MMUImpl) sendToTop(now akita.VTimeInSec) {

	newInToTop := make([]*TranslateReadyRsp, 0)

	if mmu.toSendToTop == nil {
		return
	}

	for i := 0; i < len(mmu.toSendToTop); i++ {
		sendPacket := mmu.toSendToTop[i]
		sendPacket.SetSendTime(now)
		sendErr := mmu.ToTop.Send(sendPacket)
		if sendErr == nil {
			mmu.trace(now, "send-Top")
			mmu.NeedTick = true
			mmu.numRequestsInFlight--
		} else {
			newInToTop = append(newInToTop, sendPacket)
		}

	}

	mmu.toSendToTop = newInToTop

}

func (mmu *MMUImpl) sendToCP(now akita.VTimeInSec) {
	if mmu.toSendToCP == nil {
		return
	}

	mmu.toSendToCP.SetSendTime(now)
	sendErr := mmu.ToCP.Send(mmu.toSendToCP)
	if sendErr == nil {
		mmu.trace(now, "send-CP")
		mmu.toSendToCP = nil
		mmu.NeedTick = true
	}
	mmu.isInvalidating = false
}

func (mmu *MMUImpl) processPostPipelineBuf(now akita.VTimeInSec) {
	if len(mmu.postPipelineBuf) == 0 {
		return
	}

	newInPostPipelineBuf := make([]*TranslationReq, 0)

	for i := 0; i < len(mmu.postPipelineBuf); i++ {
		req := mmu.postPipelineBuf[i]
		table := mmu.GetOrCreatePageTable(req.PID)
		page := table.FindPage(req.VAddr)

		if page.Valid == true {
			rsp := NewTranslateReadyRsp(
				now, mmu.ToTop, req.Src(),
				req.ID)
			rsp.Page = page

			rsp.SetSrc(mmu.ToTop)
			rsp.SetDst(req.Src())
			mmu.toSendToTop = append(mmu.toSendToTop, rsp)
			mmu.NeedTick = true
		} else {
			//log.Panicf("Page fault currently not supported")
			newInPostPipelineBuf = append(newInPostPipelineBuf, req)
		}
	}

	//TODO: Will probably need to change this after implementing page faults
	mmu.postPipelineBuf = newInPostPipelineBuf

}

func (mmu *MMUImpl) processRequestFromCP(now akita.VTimeInSec) {
	if mmu.inCPReqProcessingStage == nil {
		return
	}

	if mmu.cpReqProcessStageCycleLeft <= 0 {
		switch req := mmu.inCPReqProcessingStage.(type) {
		case *PTEInvalidationReq:
			mmu.doPTEInvalidation(now, req)
			return
		default:
			log.Panicf("cannot handle request of type %s from CP", reflect.TypeOf(req))
		}
	}
	mmu.cpReqProcessStageCycleLeft--
	mmu.NeedTick = true
}

func (mmu *MMUImpl) countDownPipeline(now akita.VTimeInSec) {
	newInPipeline := make([]*inPipelineTranslationReqStatus, 0)

	for _, inPipeline := range mmu.inPipelineTranslation {
		inPipeline.CycleLeft--
		mmu.NeedTick = true

		if inPipeline.CycleLeft > 0 {
			newInPipeline = append(newInPipeline, inPipeline)
		} else {
			mmu.postPipelineBuf = append(mmu.postPipelineBuf, inPipeline.transaction)
		}
	}

	mmu.inPipelineTranslation = newInPipeline

}

func (mmu *MMUImpl) doPTEInvalidation(now akita.VTimeInSec, req *PTEInvalidationReq) {
	mmu.isInvalidating = true
	mmu.InvalidatePages(req.Vaddr, req.PID)
	rsp := NewInvalidationCompleteRsp(
		now, mmu.ToCP, req.Src(),
		req.ID)

	rsp.SetSrc(mmu.ToCP)
	rsp.SetDst(req.Src())
	rsp.InvalidationDone = true
	mmu.toSendToCP = rsp
	mmu.NeedTick = true
	mmu.inCPReqProcessingStage = nil

}

func (mmu *MMUImpl) parseFromTop(now akita.VTimeInSec) {

	if mmu.numRequestsInFlight >= mmu.maxRequestsInFlight {
		return
	}

	req := mmu.ToTop.Retrieve(now)
	if req == nil {
		return
	}

	switch req := req.(type) {
	case *TranslationReq:
		mmu.addTranslationReqToPipeline(req)
	default:
		log.Panicf("MMU canot handle request of type %s", reflect.TypeOf(req))

	}

	mmu.numRequestsInFlight++
	mmu.NeedTick = true

}

func (mmu *MMUImpl) addTranslationReqToPipeline(req *TranslationReq) {
	translationInPipeline := new(inPipelineTranslationReqStatus)
	translationInPipeline.transaction = req
	translationInPipeline.CycleLeft = mmu.Latency
	mmu.inPipelineTranslation = append(mmu.inPipelineTranslation, translationInPipeline)
}

func (mmu *MMUImpl) parseFromCP(now akita.VTimeInSec) {

	if mmu.isInvalidating || mmu.numRequestsInFlight > 0 {
		return
	}

	req := mmu.ToCP.Retrieve(now)
	if req == nil {
		return
	}

	mmu.inCPReqProcessingStage = req
	mmu.cpReqProcessStageCycleLeft = mmu.ShootdownLatency
}

func (mmu *MMUImpl) InvalidatePages(vAddr []uint64, pid PID) {
	table := mmu.GetOrCreatePageTable(pid)

	for i := 0; i < len(vAddr); i++ {
		page := table.FindPage(vAddr[i])
		if page == nil {
			log.Panicf("Trying to invalidate a page that does not exist")

		}

		page.Valid = false
	}
}

func (mmu *MMUImpl) GetOrCreatePageTable(pid PID) PageTable {
	table, found := mmu.PageTables[pid]

	if !found {
		table = mmu.pageTableFactory.Build()
		mmu.PageTables[pid] = table
	}

	return table
}

// NewMMU returns an MMU component
func NewMMU(
	name string,
	engine akita.Engine,
	pageTableFactory PageTableFactory,
) *MMUImpl {
	mmu := new(MMUImpl)
	mmu.TickingComponent = akita.NewTickingComponent(
		name, engine, 1*akita.GHz, mmu)

	mmu.pageTableFactory = pageTableFactory

	mmu.ToTop = akita.NewLimitNumReqPort(mmu, 4)
	mmu.ToCP = akita.NewLimitNumReqPort(mmu, 1)

	mmu.isInvalidating = false
	mmu.PageTables = make(map[PID]PageTable)
	mmu.maxRequestsInFlight = 16
	mmu.numRequestsInFlight = 0

	return mmu
}
