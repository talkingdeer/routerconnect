package vm

import (
	"log"
	"sync"
)

// A Page is an entry in the page table, maintaining the information about how
// to translate a virtual address to a physical address
type Page struct {
	PID      PID
	PAddr    uint64
	VAddr    uint64
	PageSize uint64
	Valid    bool
	GPUID    uint64
}

// A PageTable holds the meta-data of the pages.
type PageTable interface {
	InsertPage(page *Page)
	RemovePage(vAddr uint64)
	FindPage(vAddr uint64) *Page
}

// PageTableImpl is the default implementation of a Page Table
type PageTableImpl struct {
	sync.Mutex
	entries []*Page
}

// InsertPage put a new page into the PageTable
func (pt *PageTableImpl) InsertPage(page *Page) {
	p := pt.FindPage(page.VAddr)
	if p != nil {
		log.Panic("Page already exists")
	}

	pt.Lock()
	pt.entries = append(pt.entries, page)
	pt.Unlock()
}

// RemovePage removes the entry in the page table that contains the target
// address.
func (pt *PageTableImpl) RemovePage(vAddr uint64) {
	newEntries := make([]*Page, 0, len(pt.entries)-1)

	pt.Lock()
	for _, entry := range pt.entries {
		if vAddr < entry.VAddr ||
			vAddr >= entry.VAddr+entry.PageSize {
			newEntries = append(newEntries, entry)
		}
	}
	pt.entries = newEntries
	pt.Unlock()
}

// FindPage returns the page that contains the given virtual address. This
// function returns nil if the page is not found.
func (pt *PageTableImpl) FindPage(vAddr uint64) *Page {
	pt.Lock()
	defer pt.Unlock()

	for _, entry := range pt.entries {
		if vAddr >= entry.VAddr &&
			vAddr < entry.VAddr+entry.PageSize {
			return entry
		}
	}
	return nil
}

// A PageTableFactor builds PageTables
type PageTableFactory interface {
	Build() PageTable
}

// DefaultPageTableFactory builds PageTableImpl
type DefaultPageTableFactory struct{}

func (f DefaultPageTableFactory) Build() PageTable {
	return &PageTableImpl{}
}
