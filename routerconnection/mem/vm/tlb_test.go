package vm

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/akita/mock_akita"
)

var _ = Describe("TLB", func() {

	var (
		mockCtrl     *gomock.Controller
		engine       *mock_akita.MockEngine
		tlb          *TLB
		toTop        *mock_akita.MockPort
		toBottom     *mock_akita.MockPort
		toCP         *mock_akita.MockPort
		topModule    *akita.LimitNumReqPort
		bottomModule *akita.LimitNumReqPort
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = mock_akita.NewMockEngine(mockCtrl)
		toTop = mock_akita.NewMockPort(mockCtrl)
		toBottom = mock_akita.NewMockPort(mockCtrl)
		toCP = mock_akita.NewMockPort(mockCtrl)

		tlb = NewTLB("tlb", engine)
		tlb.ToTop = toTop
		tlb.ToBottom = toBottom
		tlb.ToCP = toCP

		topModule = akita.NewLimitNumReqPort(nil, 4)
		bottomModule = akita.NewLimitNumReqPort(nil, 4)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should parse from top", func() {
		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		toTop.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(req)

		tlb.parseFromTop(11)

		Expect(tlb.NeedTick).To(BeTrue())
		Expect(tlb.inReadWriteStage).To(BeIdenticalTo(req))
		Expect(tlb.readWriteStageCycleLeft).To(Equal(tlb.Latency))
		Expect(tlb.translating).To(BeIdenticalTo(req))
	})

	It("should stall parse from top if the TLB is processing another request", func() {
		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.translating = req

		tlb.parseFromTop(11)

		Expect(tlb.NeedTick).To(BeFalse())
	})

	It("should parse from bottom", func() {
		req := NewTranslateReadyRsp(10, topModule, toTop, "ID")
		toBottom.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(req)

		tlb.parseFromBottom(11)

		Expect(tlb.NeedTick).To(BeTrue())
		Expect(tlb.inReadWriteStage).To(BeIdenticalTo(req))
		Expect(tlb.readWriteStageCycleLeft).To(Equal(tlb.Latency))
	})

	It("should stall parse from bottom if storage is occupied", func() {
		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.inReadWriteStage = req

		tlb.parseFromBottom(11)

		Expect(tlb.NeedTick).To(BeFalse())
	})

	It("should reduce read or write cycle", func() {
		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.inReadWriteStage = req
		tlb.readWriteStageCycleLeft = 2

		tlb.readOrWrite(11)

		Expect(tlb.readWriteStageCycleLeft).To(Equal(1))
		Expect(tlb.NeedTick).To(BeTrue())
	})

	It("should send req to bottom if miss", func() {
		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.inReadWriteStage = req
		tlb.readWriteStageCycleLeft = 0

		tlb.readOrWrite(11)

		Expect(tlb.toSendToBottom).NotTo(BeNil())
		Expect(tlb.inReadWriteStage).To(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
	})

	It("should stall if there is a pending req to bottom ", func() {
		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.inReadWriteStage = req
		tlb.readWriteStageCycleLeft = 0
		tlb.toSendToBottom = &TranslationReq{}

		tlb.readOrWrite(11)

		Expect(tlb.inReadWriteStage).NotTo(BeNil())
		Expect(tlb.NeedTick).To(BeFalse())
	})

	It("should send rsp to top if hit", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
		}
		tlb.Sets[1].Blocks[0] = page
		tlb.Sets[1].LRUQueue[0] = page

		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.translating = req
		tlb.inReadWriteStage = req
		tlb.readWriteStageCycleLeft = 0

		tlb.readOrWrite(11)

		Expect(tlb.toSendToTop).NotTo(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
		Expect(tlb.translating).To(BeNil())
		Expect(tlb.Sets[1].LRUQueue[tlb.NumWays-1]).To(BeIdenticalTo(page))
		Expect(tlb.toSendToTop.Page).To(BeEquivalentTo(page))
	})

	It("should stall is there is a pending send to top", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
		}
		tlb.Sets[1].Blocks[0] = page
		tlb.Sets[1].LRUQueue[0] = page

		tlb.toSendToTop = &TranslateReadyRsp{}

		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.translating = req
		tlb.inReadWriteStage = req
		tlb.readWriteStageCycleLeft = 0

		tlb.readOrWrite(11)

		Expect(tlb.NeedTick).To(BeFalse())
		Expect(tlb.translating).To(BeIdenticalTo(req))
		Expect(tlb.Sets[1].LRUQueue[tlb.NumWays-1]).NotTo(BeIdenticalTo(page))
	})

	It("should write to tlb storage", func() {
		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.translating = req

		page := &Page{
			PID:      1,
			PAddr:    0x0,
			VAddr:    0x1000,
			PageSize: 4096,
			Valid:    true,
			GPUID: 2,
		}
		rsp := NewTranslateReadyRsp(11, bottomModule, tlb.ToBottom, req.GetID())
		rsp.Page = page
		tlb.inReadWriteStage = rsp
		tlb.readWriteStageCycleLeft = 0

		tlb.readOrWrite(11)

		Expect(tlb.toSendToTop).NotTo(BeNil())
		Expect(tlb.inReadWriteStage).To(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
		Expect(tlb.translating).To(BeNil())
		Expect(tlb.Sets[1].LRUQueue[tlb.NumWays-1]).To(BeEquivalentTo(page))
		Expect(tlb.toSendToTop.Page).To(BeEquivalentTo(page))
	})

	It("should stall is there is penging request to send to top", func() {
		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.translating = req

		page := &Page{
			PID:      1,
			PAddr:    0x0,
			VAddr:    0x1000,
			PageSize: 4096,
			Valid:    true,
		}
		rsp := NewTranslateReadyRsp(11, bottomModule, tlb.ToBottom, req.GetID())
		rsp.Page = page
		tlb.inReadWriteStage = rsp
		tlb.readWriteStageCycleLeft = 0

		tlb.toSendToTop = &TranslateReadyRsp{}

		tlb.readOrWrite(11)

		Expect(tlb.NeedTick).To(BeFalse())
		Expect(tlb.translating).NotTo(BeNil())
		Expect(tlb.Sets[1].LRUQueue[tlb.NumWays-1]).NotTo(BeIdenticalTo(page))
	})

	It("should send to top", func() {
		rsp := NewTranslateReadyRsp(11, toTop, topModule, "")
		toTop.EXPECT().Send(rsp).Return(nil)

		tlb.toSendToTop = rsp

		tlb.sendToTop(11)

		Expect(tlb.toSendToTop).To(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
	})

	It("should send to bottom", func() {
		req := NewTranslateReq(11, toTop, topModule,
			1, 0x1000, 0)
		toBottom.EXPECT().Send(req).Return(nil)

		tlb.toSendToBottom = req

		tlb.sendToBottom(11)

		Expect(tlb.toSendToBottom).To(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
	})

	It("should parse from CP", func() {
		vAddr := make([]uint64, 1)
		vAddr[0] = 0x1000
		req := NewPTEInvalidationReq(10, nil, tlb.ToCP, 1, vAddr)
		toCP.EXPECT().Retrieve(akita.VTimeInSec(11)).Return(req)

		tlb.parseFromCP(11)

		Expect(tlb.NeedTick).To(BeTrue())
		Expect(tlb.inCPReqProcessingStage).To(BeIdenticalTo(req))
		Expect(tlb.cpReqProcessStageCycleLeft).To(Equal(tlb.ShootdownLatency))
	})

	It("should invalidate a page based on request from CP", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
		}
		tlb.Sets[1].Blocks[0] = page
		tlb.Sets[1].LRUQueue[0] = page

		vAddr := make([]uint64, 1)
		vAddr[0] = 0x1000

		req := NewPTEInvalidationReq(10, nil, tlb.ToCP, 1, vAddr)
		tlb.inCPReqProcessingStage = req
		tlb.cpReqProcessStageCycleLeft = 0

		tlb.processRequestFromCP(11)

		Expect(tlb.toSendToCP).NotTo(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
		Expect(tlb.Sets[1].Blocks[0].Valid).To((BeFalse()))
		Expect(tlb.toSendToCP.InvalidationDone).To(BeTrue())

	})

	It("should stall is there is a pending request being processed from CP", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
		}
		tlb.Sets[1].Blocks[0] = page
		tlb.Sets[1].LRUQueue[0] = page

		tlb.toSendToCP = &InvalidationCompleteRsp{}

		vAddr := make([]uint64, 1)
		vAddr[0] = 0x1000
		req := NewPTEInvalidationReq(10, nil, tlb.ToCP, 1, vAddr)
		tlb.inCPReqProcessingStage = req
		tlb.cpReqProcessStageCycleLeft = 0

		tlb.processRequestFromCP(11)

		Expect(tlb.NeedTick).To(BeFalse())
		Expect(tlb.toSendToCP.InvalidationDone).To(BeFalse())

	})

	It("should send to CP ", func() {
		rsp := NewInvalidationCompleteRsp(11, toCP, nil, "")
		toCP.EXPECT().Send(rsp).Return(nil)

		tlb.toSendToCP = rsp

		tlb.sendToCP(11)

		Expect(tlb.toSendToCP).To(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
	})

	It("should first have entries that hit. After invalidation these entries should be miss", func() {

		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
		}
		tlb.Sets[1].Blocks[0] = page
		tlb.Sets[1].LRUQueue[0] = page

		req := NewTranslateReq(10, topModule, toTop, 1, 0x1020, 0)
		tlb.translating = req
		tlb.inReadWriteStage = req
		tlb.readWriteStageCycleLeft = 0

		tlb.readOrWrite(11)

		Expect(tlb.toSendToTop).NotTo(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
		Expect(tlb.translating).To(BeNil())
		Expect(tlb.Sets[1].LRUQueue[tlb.NumWays-1]).To(BeIdenticalTo(page))
		Expect(tlb.toSendToTop.Page).To(BeIdenticalTo(page))

		vAddr := make([]uint64, 1)
		vAddr[0] = 0x1000

		cpReq := NewPTEInvalidationReq(12, nil, tlb.ToCP, 1, vAddr)
		tlb.inCPReqProcessingStage = cpReq
		tlb.cpReqProcessStageCycleLeft = 0

		tlb.processRequestFromCP(13)

		Expect(tlb.toSendToCP).NotTo(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())
		Expect(tlb.Sets[1].Blocks[0].Valid).To((BeFalse()))
		Expect(tlb.toSendToCP.InvalidationDone).To(BeTrue())

		req = NewTranslateReq(14, topModule, toTop, 1, 0x1020, 0)
		tlb.inReadWriteStage = req
		tlb.readWriteStageCycleLeft = 0

		tlb.readOrWrite(15)

		Expect(tlb.toSendToBottom).NotTo(BeNil())
		Expect(tlb.inReadWriteStage).To(BeNil())
		Expect(tlb.NeedTick).To(BeTrue())

	})

})

type mockComponent struct {
	*akita.ComponentBase
	ToMMU        akita.Port
	receivedReqs []akita.Req
}

func (c *mockComponent) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
	panic("implement me")

}

func (c *mockComponent) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	req := port.Retrieve(now)
	c.receivedReqs = append(c.receivedReqs, req)
}

func (mockComponent) Handle(e akita.Event) error {
	panic("implement me")
}

func newMockComponent() *mockComponent {
	componentBase := akita.NewComponentBase("mock")
	component := new(mockComponent)
	component.ComponentBase = componentBase
	component.ToMMU = akita.NewLimitNumReqPort(component, 4)
	return component
}

var _ = Describe("GPU TLB Only One Level Integration", func() {
	var (
		engine akita.Engine
		tlb    *TLB
		mmu    *MMUImpl
		conn   *akita.DirectConnection
		agent  *mockComponent
	)

	BeforeEach(func() {
		agent = newMockComponent()

		engine = akita.NewSerialEngine()
		tlb = NewTLB("tlb", engine)
		tlb.Freq = 1 * akita.GHz
		tlb.Latency = 10

		mmu = NewMMU("mmu", engine, DefaultPageTableFactory{})
		mmu.Freq = 1 * akita.GHz
		mmu.Latency = 20

		tlb.LowModule = mmu.ToTop

		conn = akita.NewDirectConnection(engine)
		conn.PlugIn(tlb.ToTop)
		conn.PlugIn(tlb.ToBottom)
		conn.PlugIn(mmu.ToTop)
		conn.PlugIn(agent.ToMMU)
	})

	It("should handle a translation request", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:   2,
		}
		mmu.CreatePage(page)

		vAddr := uint64(0x1030)
		pid := PID(1)

		translationReq := NewTranslateReq(
			10, agent.ToMMU, tlb.ToTop, pid, vAddr, 0)
		translationReq.SetRecvTime(10)

		tlb.ToTop.Recv(translationReq)

		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(1))
		rsp := agent.receivedReqs[0].(*TranslateReadyRsp)

		//log.Printf("PFN received is %d \n", dataReady.PhysicalFrameNumber)
		Expect(rsp.Page).To(BeEquivalentTo(page))

		vAddr = uint64(0x1020)
		translationReq = NewTranslateReq(
			10, agent.ToMMU,
			tlb.ToTop, pid, vAddr, 0)
		translationReq.SetRecvTime(10)

		tlb.ToTop.Recv(translationReq)
		engine.Run()

	})

})

var _ = Describe("GPU TLB L1 and L2 Integration", func() {
	var (
		engine akita.Engine
		tlbL1  *TLB
		tlbL2  *TLB
		mmu    *MMUImpl
		conn   *akita.DirectConnection
		agent  *mockComponent
	)

	BeforeEach(func() {
		agent = newMockComponent()

		engine = akita.NewSerialEngine()

		tlbL1 = NewTLB("tlbL1", engine)
		tlbL1.Freq = 1 * akita.GHz
		tlbL1.Latency = 10

		tlbL2 = NewTLB("tlbL2", engine)
		tlbL2.Freq = 1 * akita.GHz
		tlbL2.Latency = 20

		mmu = NewMMU("mmu", engine, DefaultPageTableFactory{})
		mmu.Freq = 1 * akita.GHz
		mmu.Latency = 30

		tlbL1.LowModule = tlbL2.ToTop
		tlbL2.LowModule = mmu.ToTop

		conn = akita.NewDirectConnection(engine)

		conn.PlugIn(tlbL1.ToTop)
		conn.PlugIn(tlbL1.ToBottom)
		conn.PlugIn(tlbL2.ToTop)
		conn.PlugIn(tlbL2.ToBottom)
		conn.PlugIn(mmu.ToTop)
		conn.PlugIn(agent.ToMMU)

	})

	It("should handle a translation request", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID: 2,
		}
		mmu.CreatePage(page)

		vAddr := uint64(0x1010)
		pid := PID(1)

		translationReq := NewTranslateReq(10,
			agent.ToMMU, tlbL1.ToTop, pid, vAddr, 0)
		translationReq.SetRecvTime(10)

		tlbL1.ToTop.Recv(translationReq)
		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(1))
		rsp := agent.receivedReqs[0].(*TranslateReadyRsp)
		Expect(rsp.Page).To(BeEquivalentTo(page))

		vAddr2 := uint64(0x1020)
		translationReq2 := NewTranslateReq(10,
			agent.ToMMU, tlbL1.ToTop, pid, vAddr2, 0)
		translationReq2.SetRecvTime(50)

		tlbL1.ToTop.Recv(translationReq2)
		engine.Run()

		Expect(agent.receivedReqs).To(HaveLen(2))
		rsp = agent.receivedReqs[1].(*TranslateReadyRsp)
		Expect(rsp.Page).To(BeEquivalentTo(page))
	})

})
