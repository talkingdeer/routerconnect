package vm

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/akita/mock_akita"
)

var _ = Describe("MMU", func() {

	var (
		mockCtrl *gomock.Controller
		engine   *mock_akita.MockEngine
		toTop    *mock_akita.MockPort
		toCP     *mock_akita.MockPort
		mmu      *MMUImpl
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = mock_akita.NewMockEngine(mockCtrl)
		toTop = mock_akita.NewMockPort(mockCtrl)
		toCP = mock_akita.NewMockPort(mockCtrl)

		mmu = NewMMU("mmu", engine, DefaultPageTableFactory{})
		mmu.Freq = 1000 * akita.MHz
		mmu.Latency = 100
		mmu.ToTop = toTop
		mmu.ToCP = toCP
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should create page", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		Expect(mmu.PageTables).To(HaveLen(1))
		Expect(mmu.PageTables[1].FindPage(0x1000)).To(BeIdenticalTo(page))
	})

	It("should translate", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		pAddr, retPage := mmu.Translate(1, 0x1020)

		Expect(pAddr).To(Equal(uint64(0x20)))
		Expect(retPage).To(BeIdenticalTo(page))
	})

	It("should not find non-existing page", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		_, retPage := mmu.Translate(1, 0x2000)

		Expect(retPage).To(BeNil())
	})

	It("should process translation request", func() {
		translationReq := NewTranslateReq(10, nil, mmu.ToTop, 1, 0x100000100, 0)
		toTop.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(translationReq)

		mmu.parseFromTop(10)

		Expect(mmu.numRequestsInFlight == 1)
	})

	It("should set MMU  to max in flight requests", func() {
		mmu.numRequestsInFlight = 15
		translationReq := NewTranslateReq(10, nil, mmu.ToTop, 1, 0x100000100, 0)
		toTop.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(translationReq)

		mmu.parseFromTop(10)

		Expect(mmu.numRequestsInFlight == 16)
	})
	It("should stall parse from top if MMU is servicing max requests", func() {
		mmu.numRequestsInFlight = 16

		mmu.parseFromTop(10)

		Expect(mmu.NeedTick).To(BeFalse())
	})
	It("should reduce translation cycles", func() {
		req := NewTranslateReq(10, nil, toTop, 1, 0x1020, 0)
		inPipeline := &inPipelineTranslationReqStatus{req, 10}
		mmu.inPipelineTranslation = append(mmu.inPipelineTranslation, inPipeline)
		mmu.countDownPipeline(11)
		Expect(inPipeline.CycleLeft).To(Equal(9))
		Expect(mmu.NeedTick).To(BeTrue())
	})

	It("should insert into post pipeline buf", func() {
		translationReq := NewTranslateReq(10, nil, nil, 1, 0x1024, 0)
		inPipeline := &inPipelineTranslationReqStatus{translationReq, 1}
		mmu.inPipelineTranslation = append(mmu.inPipelineTranslation, inPipeline)
		mmu.countDownPipeline(10)
		Expect(mmu.inPipelineTranslation).To(HaveLen(0))
		Expect(mmu.postPipelineBuf).To(HaveLen(1))

	})

	It("should send rsp to top if hit", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
		}
		mmu.CreatePage(page)

		translationReq := NewTranslateReq(10, nil, mmu.ToTop, 1, 0x1000, 0)
		mmu.postPipelineBuf = append(mmu.postPipelineBuf, translationReq)

		mmu.processPostPipelineBuf(11)

		Expect(mmu.toSendToTop).NotTo(BeNil())
		Expect(mmu.NeedTick).To(BeTrue())
		Expect(mmu.postPipelineBuf).To(HaveLen(0))

	})
	It("should invalidate a page. Subsequent translations should return invalid page", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		pAddr, retPage := mmu.Translate(1, 0x1020)

		Expect(pAddr).To(Equal(uint64(0x20)))
		Expect(retPage).To(BeIdenticalTo(page))

		vAddr := make([]uint64, 1)
		vAddr[0] = 0x1000

		mmu.InvalidatePages(vAddr, 1)

		pAddr, retPage = mmu.Translate(1, 0x1020)

		Expect(pAddr).To(Equal(uint64(0)))
		Expect(retPage).To((BeNil()))

	})

	It("should handle PTE invalidation event", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}

		mmu.CreatePage(page)
		vAddr := make([]uint64, 1)
		vAddr[0] = 0x1000

		pteInvalidationReq := NewPTEInvalidationReq(10, nil, mmu.ToCP, 1, vAddr)
		mmu.inCPReqProcessingStage = pteInvalidationReq
		mmu.cpReqProcessStageCycleLeft = 0

		mmu.processRequestFromCP(10)

		Expect(mmu.toSendToCP).NotTo(BeNil())
		Expect(mmu.NeedTick).To(BeTrue())
		Expect(mmu.inCPReqProcessingStage).To(BeNil())

	})

	It("should put a invalid page back to post pipeline buf", func() {
		page := &Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    false,
			GPUID:    1,
		}

		mmu.CreatePage(page)
		vAddr := make([]uint64, 1)
		vAddr[0] = 0x1000

		translationReq := NewTranslateReq(10, nil, mmu.ToTop, 1, 0x1000, 0)
		mmu.postPipelineBuf = append(mmu.postPipelineBuf, translationReq)

		mmu.processPostPipelineBuf(11)

		Expect(mmu.postPipelineBuf).To(HaveLen(1))

	})
})
