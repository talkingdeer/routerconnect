package mem

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
)

type readRespondEvent struct {
	*akita.EventBase

	req *ReadReq
}

func newReadRespondEvent(time akita.VTimeInSec, handler akita.Handler,
	req *ReadReq,
) *readRespondEvent {
	return &readRespondEvent{akita.NewEventBase(time, handler), req}
}

type writeRespondEvent struct {
	*akita.EventBase

	req *WriteReq
}

func newWriteRespondEvent(time akita.VTimeInSec, handler akita.Handler,
	req *WriteReq,
) *writeRespondEvent {
	return &writeRespondEvent{akita.NewEventBase(time, handler), req}
}

// An IdealMemController is an Akita component that allows read and write
//
// An IdealMemController always respond to the request in a fixed number of
// cycles. There is no limitation on the concurrency of this unit.
type IdealMemController struct {
	*akita.ComponentBase

	Storage *Storage

	Freq             akita.Freq
	Latency          int
	Engine           akita.Engine
	AddressConverter AddressConverter

	ToTop akita.Port
}

// NotifyPortFree of an IdealMemController does not do anything
func (c *IdealMemController) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
	// Do nothing
}

// NotifyRecv triggers the IdealMemController to retrieve requests from the
// port.
func (c *IdealMemController) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	req := port.Retrieve(now)
	akita.ProcessReqAsEvent(req, c.Engine, c.Freq)
}

// Handle defines how the IdealMemController handles event
func (c *IdealMemController) Handle(e akita.Event) error {
	switch e := e.(type) {
	case *ReadReq:
		return c.handleReadReq(e)
	case *WriteReq:
		return c.handleWriteReq(e)
	case *readRespondEvent:
		return c.handleReadRespondEvent(e)
	case *writeRespondEvent:
		return c.handleWriteRespondEvent(e)
	default:
		log.Panicf("cannot handle event of %s", reflect.TypeOf(e))
	}
	return nil
}

func (c *IdealMemController) handleReadReq(req *ReadReq) error {
	now := req.Time()
	c.traceMem(now, "r", req.Address, req.MemByteSize)
	timeToSchedule := c.Freq.NCyclesLater(c.Latency, req.RecvTime())
	respondEvent := newReadRespondEvent(timeToSchedule, c, req)
	c.Engine.Schedule(respondEvent)
	return nil
}

func (c *IdealMemController) handleWriteReq(req *WriteReq) error {
	now := req.Time()
	c.traceMem(now, "w", req.Address, uint64(len(req.Data)))
	timeToSchedule := c.Freq.NCyclesLater(c.Latency, req.RecvTime())
	respondEvent := newWriteRespondEvent(timeToSchedule, c, req)
	c.Engine.Schedule(respondEvent)
	return nil
}

func (c *IdealMemController) handleReadRespondEvent(e *readRespondEvent) error {
	now := e.Time()
	req := e.req

	addr := req.Address
	if c.AddressConverter != nil {
		addr = c.AddressConverter.ConvertExternalToInternal(addr)
	}

	data, err := c.Storage.Read(addr, req.MemByteSize)
	if err != nil {
		log.Panic(err)
	}

	rsp := NewDataReadyRsp(e.Time(), c.ToTop, req.Src(), req.ID)
	rsp.Data = data
	networkErr := c.ToTop.Send(rsp)
	if networkErr != nil {
		retry := newReadRespondEvent(c.Freq.NextTick(now), c, req)
		c.Engine.Schedule(retry)
		return nil
	}

	c.traceMem(now, "r-complete", req.Address, req.MemByteSize)
	c.traceMem(now, "send-top-data-ready", req.Address, req.MemByteSize)

	return nil
}

func (c *IdealMemController) handleWriteRespondEvent(e *writeRespondEvent) error {
	now := e.Time()
	req := e.req

	rsp := NewDoneRsp(e.Time(), c.ToTop, req.Src(), req.ID)
	networkErr := c.ToTop.Send(rsp)
	if networkErr != nil {
		retry := newWriteRespondEvent(c.Freq.NextTick(now), c, req)
		c.Engine.Schedule(retry)
		return nil
	}

	addr := req.Address
	if c.AddressConverter != nil {
		addr = c.AddressConverter.ConvertExternalToInternal(addr)
	}

	err := c.Storage.Write(addr, req.Data)
	if err != nil {
		log.Panic(err)
	}

	c.traceMem(now, "w-complete", req.Address, uint64(len(req.Data)))
	c.traceMem(now, "send-top-done", req.Address, uint64(len(req.Data)))

	return nil
}

func (c *IdealMemController) traceMem(
	now akita.VTimeInSec,
	what string,
	addr uint64, byteSize uint64,
) {
	traceInfo := TraceInfo{
		Where:    c.Name(),
		When:     now,
		What:     what,
		Address:  addr,
		ByteSize: byteSize,
	}

	ctx := akita.HookCtx{
		Domain: c,
		Item:   traceInfo,
		Now:    now,
	}
	c.InvokeHook(&ctx)
}

// NewIdealMemController creates a new IdealMemController
func NewIdealMemController(
	name string,
	engine akita.Engine,
	capacity uint64,
) *IdealMemController {
	c := new(IdealMemController)
	c.ComponentBase = akita.NewComponentBase(name)
	c.Engine = engine

	c.Freq = 1 * akita.GHz
	c.Latency = 100

	c.Storage = NewStorage(capacity)
	c.ToTop = akita.NewLimitNumReqPort(c, 1)
	return c
}
