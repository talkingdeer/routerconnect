package mem

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
)

// AccessReq abstracts read and write requests that are sent to the
// cache modules or memory controllers.
type AccessReq interface {
	akita.Req
	GetAddress() uint64
	GetByteSize() uint64
}

// A ReadReq is a request sent to a memory controller to fetch data
type ReadReq struct {
	*akita.ReqBase

	Address      uint64
	MemByteSize  uint64
	PID          vm.PID
	IsPhysical   bool
	IsLastInWave bool
}

func (r *ReadReq) ByteSize() int {
	return 12
}

func (r *ReadReq) GetByteSize() uint64 {
	return r.MemByteSize
}

// GetAddress returns the address that the request is accessing
func (r *ReadReq) GetAddress() uint64 {
	return r.Address
}

// A WriteReq is a request sent to a memory controller to fetch data
type WriteReq struct {
	*akita.ReqBase

	Address      uint64
	Data         []byte
	DirtyMask    []byte
	PID          vm.PID
	IsPhysical   bool
	IsLastInWave bool
}

func (r *WriteReq) ByteSize() int {
	return int(len(r.Data) + 12)
}

func (r *WriteReq) GetByteSize() uint64 {
	return uint64(len(r.Data))
}

// GetAddress returns the address that the request is accessing
func (r *WriteReq) GetAddress() uint64 {
	return r.Address
}

type MemRsp interface {
	akita.Req

	GetRespondTo() string
}

// A DataReadyRsp is the respond sent from the lower module to the higher
// module that carries the data loaded.
type DataReadyRsp struct {
	*akita.ReqBase

	RespondTo string // The ID of the request it replies
	Data      []byte
}

func (r *DataReadyRsp) GetRespondTo() string {
	return r.RespondTo
}

func (r *DataReadyRsp) ByteSize() int {
	return int(len(r.Data) + 12)
}

// A DoneRsp is a respond sent from the lower module to the higher module
// to mark a previous requests is completed successfully.
type DoneRsp struct {
	*akita.ReqBase

	RespondTo string
}

func (r *DoneRsp) GetRespondTo() string {
	return r.RespondTo
}

func (r *DoneRsp) ByteSize() int {
	return 12
}

// NewReadReq creates a ReadReq
func NewReadReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	address uint64,
	byteSize uint64,
) *ReadReq {
	reqBase := akita.NewReqBase()
	req := new(ReadReq)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.Address = address
	req.MemByteSize = byteSize

	return req
}

// NewWriteReq creates a new WriteReq
func NewWriteReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	address uint64,
) *WriteReq {
	reqBase := akita.NewReqBase()
	req := new(WriteReq)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.Address = address

	return req
}

// NewDataReadyRsp creates a new DataReadyRsp
func NewDataReadyRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	respondTo string,
) *DataReadyRsp {
	reqBase := akita.NewReqBase()
	req := new(DataReadyRsp)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.RespondTo = respondTo

	return req
}

// NewDoneRsp creates a new DoneRsp
func NewDoneRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	respondTo string,
) *DoneRsp {
	reqBase := akita.NewReqBase()
	req := new(DoneRsp)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.RespondTo = respondTo

	return req
}

type InvalidReq struct {
	*akita.ReqBase

	WriteBack bool
}

func NewInvalidReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
) *InvalidReq {
	req := new(InvalidReq)
	req.ReqBase = akita.NewReqBase()

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	return req
}

type InvalidDoneRsp struct {
	*akita.ReqBase

	RespondTo string
}

func NewInvalidDoneRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	respondTo string,
) *InvalidDoneRsp {
	req := new(InvalidDoneRsp)
	req.ReqBase = akita.NewReqBase()

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.RespondTo = respondTo

	return req
}
