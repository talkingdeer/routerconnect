module gitlab.com/akita/mem

require (
	github.com/golang/mock v1.2.0
	github.com/onsi/ginkgo v1.7.0
	github.com/onsi/gomega v1.4.3
	gitlab.com/akita/akita v1.3.0
	golang.org/x/net v0.0.0-20190225153610-fe579d43d832 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	golang.org/x/sys v0.0.0-20190225065934-cc5685c2db12 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
