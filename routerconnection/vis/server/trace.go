package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/mongodb/mongo-go-driver/bson"
)

func httpTrace(w http.ResponseWriter, r *http.Request) {
	// var err error

	taskType := r.FormValue("type")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	filter := bson.D{}
	if taskType != "" {
		filter = append(filter, bson.E{"type", taskType})
	}

	cursor, err := collection.Find(ctx, filter)
	dieOnErr(err)
	defer cursor.Close(ctx)

	tasks := make([]Task, 0)

	for cursor.Next(ctx) {
		task := Task{}
		err = cursor.Decode(&task)
		dieOnErr(err)

		tasks = append(tasks, task)
	}

	rsp, err := json.Marshal(tasks)
	dieOnErr(err)

	fmt.Println(rsp)
	_, err = w.Write([]byte(rsp))
	dieOnErr(err)
}
