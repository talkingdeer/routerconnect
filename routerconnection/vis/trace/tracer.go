package trace

import (
	"context"
	"log"
	"time"

	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/rs/xid"
	"go.mongodb.org/mongo-driver/bson"
)

// A Tracer can log tasks in a simulation
type Tracer interface {
	CreateTask(task *Task)
	UpdateTimestamp(taskID string, whichTime string, time float64)
	SaveTask(taskID string)
}

// A MongoDBTracer logs the tasks in a MongoDB database
type MongoDBTracer struct {
	client     *mongo.Client
	collection *mongo.Collection
	uri        string
	tasks      map[string]*Task
}

// NewMongoDBTracer returns a new MongoDBTracer
func NewMongoDBTracer() *MongoDBTracer {
	t := &MongoDBTracer{
		uri:   "mongodb://localhost:27017",
		tasks: make(map[string]*Task),
	}
	return t
}

// SetURI sets the server and the port to connect to
func (t *MongoDBTracer) SetURI(uri string) {
	t.uri = uri
}

// Init creates a new database for the trace
func (t *MongoDBTracer) Init() {
	var err error

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//t.client, err = mongo.Connect(ctx, options.Client().ApplyURI(t.uri))
	t.client, err = mongo.Connect(ctx, "hello")
	if err != nil {
		log.Panic(err)
	}

	dbName := xid.New().String()
	log.Printf("Trace collected in db %s\n", dbName)

	t.collection = t.client.Database(dbName).Collection("trace")

	t.createIndexes()
}

func (t *MongoDBTracer) createIndexes() {
	t.createIndex("ID", true)
	t.createIndex("ParentID", true)
	t.createIndex("Type", true)
	t.createIndex("InitiateTime", false)
	t.createIndex("StartTime", false)
	t.createIndex("CompleteTime", false)
	t.createIndex("ClearTime", false)
}

func (t *MongoDBTracer) createIndex(key string, useHash bool) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var value interface{}
	if useHash {
		value = "hashed"
	} else {
		value = 1
	}

	_, err := t.collection.Indexes().CreateOne(ctx,
		mongo.IndexModel{
			Keys: bson.D{bson.E{Key: key, Value: value}},
		},
	)
	if err != nil {
		log.Panic(err)
	}
}

// CreateTask create a record in the database
func (t *MongoDBTracer) CreateTask(task *Task) {
	t.tasks[task.ID] = task
}

// UpdateTimestamp writes a timestamp to a record that is already in the
// database
func (t *MongoDBTracer) UpdateTimestamp(
	taskID string,
	whichTime string,
	timestamp float64,
) {
	task, ok := t.tasks[taskID]
	if !ok {
		log.Panic("Updating a task time stamp that is not created.")
	}

	switch whichTime {
	case "InitiateTime":
		task.InitiateTime = timestamp
	case "StartTime":
		task.StartTime = timestamp
	case "CompleteTime":
		task.CompleteTime = timestamp
	case "ClearTime":
		task.ClearTime = timestamp
	default:
		log.Panicf("Uuknown task time %s", whichTime)
	}
}

// SaveTask stores the task into the database. After this, the task cannot be
// modified anymore.
func (t *MongoDBTracer) SaveTask(taskID string) {
	task := t.tasks[taskID]
	delete(t.tasks, taskID)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := t.collection.InsertOne(ctx, task)
	if err != nil {
		log.Panic(err)
	}
}
