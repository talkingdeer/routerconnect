import IPSChart from './ipschart'
import TaskChart from './taskchart'

class LandingPage {

    constructor() {
        this.commandView = null;
        this.ipsView = null;
        this.perGPUIPSView = null;
    }

    layout() {
        const container = document.getElementById('container');

        if (this.commandView === null) {
            this.commandView = document.createElement('div');
            this.commandView.setAttribute('id', 'command-view');
            this.commandView.appendChild(
                document.createElementNS("http://www.w3.org/2000/svg", "svg"));
            this.commandView.style.width = container.offsetWidth;
            this.commandView.style.height = 200;
        }

        if (this.ipsView === null) {
            this.ipsView = document.createElement('div');
            this.ipsView.setAttribute('id', 'ips-view');
            this.ipsView.appendChild(
                document.createElementNS("http://www.w3.org/2000/svg", "svg"));
            this.ipsView.style.width = container.offsetWidth;
            this.ipsView.style.height = 200;
        }

        if (this.perGPUIPSView === null) {
            this.perGPUIPSView = document.createElement('div');
            this.perGPUIPSView.setAttribute('id', 'per-cu-ips-view');
            this.perGPUIPSView.appendChild(
                document.createElementNS("http://www.w3.org/2000/svg", "svg"));
        }

        container.appendChild(this.commandView);
        container.appendChild(this.ipsView);
        container.appendChild(this.perGPUIPSView);
    }

    loadAndRender() {
        const container = document.getElementById('container');

        this.loadAndRenderIPSChart(container);
        this.loadAndRenderCommandChart(container);
    }

    loadAndRenderCommandChart(container) {
        const commandChart = new TaskChart();
        commandChart.canvas = this.commandView;
        commandChart.canvasWidth = container.offsetWidth;
        commandChart.canvasHeight = 200;

        fetch('/api/trace?type=Command')
            .then(rsp => rsp.json())
            .then(rsp => commandChart.updateData(rsp))
    }

    loadAndRenderIPSChart(container) {
        const ipsChart = new IPSChart();
        ipsChart.canvas = this.ipsView;
        ipsChart.canvasWidth = container.offsetWidth;
        ipsChart.canvasHeight = 200;

        fetch('/api/ips')
            .then(rsp => rsp.json())
            .then(rsp => ipsChart.updateData(rsp));
    }
}

export default LandingPage;