import LandingPage from "./landingpage";

class App {
    constructor() {
        this.view = "landing_page"

        this.LandingPage = new LandingPage()
    }

    start() {
        this.LandingPage.layout();
        this.LandingPage.loadAndRender();
    }
}

export default App;